package ads.team4.fp;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.client.methods.HttpPost;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class estimate {
	
	
	@RequestMapping(value = "estimatequick.htm", method = RequestMethod.POST)
	public ModelAndView quick(HttpServletRequest request, HttpServletResponse response,Locale locale, Model model) throws Exception {
		ModelAndView mv= new ModelAndView();
		
		
		String estimatetype = "Quick Estimate";
		String zipcode = request.getParameter("zipcode"); 
		String type = request.getParameter("htype");
		String room = request.getParameter("room");

		String bedroom = null  , bathroom = null;
		if(room.equals("studio")){
			bedroom = "1";  
			bathroom = "1";  
			}
		else if(room.equals("1b1b")){
			bedroom = "1";  
			bathroom = "1";  
			}
		else if(room.equals("2b1b")){
			bedroom = "2";  
			bathroom = "1";  
			}
		else if(room.equals("2b2b")){
			bedroom = "2";  
			bathroom = "2";  
			}
		else if(room.equals("3b1b")){
			bedroom = "3";  
			bathroom = "1"; 
			}
		else if(room.equals("3b2b")){
			bedroom = "3";  
			bathroom = "2";  
			}
		else if(room.equals("4b2b")){
			bedroom = "4";  
			bathroom = "2";  
			}

		
		//这里把所有的变量 加到一起变成azure的输入格式
		String userInput = "{\"Inputs\": {\"input1\": {\"ColumnNames\":" + " [\"zipcode\","
				+ " \"bedroom\"," + " \"bathroom\"," + " \"type\"],"
				+ "\"Values\": [";
		userInput+="[\""+ zipcode + "\",";
		userInput+="\""+ bedroom + "\","; 
		userInput+="\""+ bathroom  + "\",";   
		userInput+="\""+ type  + "\"]";  
		userInput+="]}},\"GlobalParameters\": {}}";

		
		//下面是链接azure
		String restUrl="https://ussouthcentral.services.azureml.net/workspaces/94a7bdddd924402681345905483aac86/services/78fbee130a844e838d910c974a4a18ea/execute?api-version=2.0&details=true";
        HttpPostReq httpPostReq=new HttpPostReq();
        HttpPost httpPost = httpPostReq.createConnectivity(restUrl);
        String result = httpPostReq.executeReq(userInput, httpPost);
        
        
		//输出最终结果
		mv.addObject("estimatedtype",estimatetype);
		mv.addObject("result",result);
		mv.setViewName("result");
		
		return mv;
	}

	
	
	
	
	@RequestMapping(value = "estimate.htm", method = RequestMethod.POST)
	public ModelAndView detail(HttpServletRequest request, HttpServletResponse response,Locale locale, Model model) throws Exception {
		ModelAndView mv= new ModelAndView();
		
		String zipcode = null, bedroom = null, bathroom = null;  
		String type = null, Parking = null,travel = null; 
		String Emergency_24 = null, GYM = null, ac = null;  
		String water_fee = null, Electronic_fee = null, heat_fee = null;  
		String dishwasher = null, microwave = null, laundry = null;  
			
		String estimatetype ="Detail Estimate";
		
		zipcode = request.getParameter("zipcode");  
		bedroom = request.getParameter("bedroom");  
		bathroom = request.getParameter("bathroom");  
		type = request.getParameter("type");  
		Parking = request.getParameter("parking");  
		Emergency_24 = request.getParameter("emergency");
		GYM = request.getParameter("gym");  
		ac = request.getParameter("ac");  
		water_fee = request.getParameter("water");  
		Electronic_fee = request.getParameter("electronic"); 
		heat_fee = request.getParameter("heat");  
		dishwasher = request.getParameter("dishwasher");  
		microwave = request.getParameter("microwave"); 
		laundry = request.getParameter("laundry"); 
		travel = request.getParameter("travel");
			
			
		//这里把所有的变量 加到一起变成azure的输入格式
		String userInput = "{\"Inputs\": {\"input1\": {\"ColumnNames\":" + " [\"zipcode\"," + " \"bedroom\","
				+ " \"bathroom\"," + " \"type\"," + " \"parking\"," + " \"24h-emergency/maintenance\","
				+ " \"GYM\"," + " \"AC\"," + " \"free-water\"," + " \"free-electricity\"," + " \"free-heat\","
				+ " \"dishwasher\"," + " \"microwave\"," + " \"washer-dryer\"," + " \"public-transportation\"],"
				+ "\"Values\": [";
		userInput+="[\""+ zipcode + "\",";
		userInput+="\""+ bedroom + "\","; 
		userInput+="\""+ bathroom  + "\",";  
		userInput+="\""+ type  + "\",";  
		userInput+="\""+ Parking  + "\",";  
		userInput+="\""+ Emergency_24  + "\",";  
		userInput+="\""+ GYM  + "\",";  
		userInput+="\""+ ac  + "\",";  
		userInput+="\""+ water_fee  + "\",";  
		userInput+="\""+ Electronic_fee  + "\",";  
		userInput+="\""+ heat_fee  + "\",";   
		userInput+="\""+ dishwasher  + "\",";  
		userInput+="\""+ microwave  + "\",";  
		userInput+="\""+ laundry  + "\",";   
		userInput+="\""+ travel  + "\"]";  
		userInput+="]}},\"GlobalParameters\": {}}";
		
		
		//下面是链接azure
		String restUrl="https://ussouthcentral.services.azureml.net/workspaces/94a7bdddd924402681345905483aac86/services/374ff728f7bb4bea96914b93c0ef6b28/execute?api-version=2.0&details=true";
        HttpPostReqDetail httpPostReqdetail=new HttpPostReqDetail();
        HttpPost httpPost = httpPostReqdetail.createConnectivitydetail(restUrl);
        String result = httpPostReqdetail.executeReqdetail(userInput, httpPost);
        
        
		//输出最终结果
		mv.addObject("estimatedtype",estimatetype);
		mv.addObject("result",result);
		mv.setViewName("result");
		
		return mv;
	}
	
	
}


