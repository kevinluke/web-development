<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.microsoft.azure.storage.*,com.microsoft.azure.storage.file.*"%>
<html>
<head>
<title>FinalProject</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="reptiles Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstarp-css -->
<link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstarp-css -->
<!-- css -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/style.css" type="text/css" media="all" />
<!--// css -->
<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>
  <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/MarkerCluster.css" />
  <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
  <script src="${pageContext.servletContext.contextPath}/resources/leaflet.markercluster.js"></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <style>
    #map{ height: 50% }
    .info {
			padding: 6px 8px;
			font: 14px/16px Arial, Helvetica, sans-serif;
			background: white;
			background: rgba(255,255,255,0.8);
			box-shadow: 0 0 15px rgba(0,0,0,0.2);
			border-radius: 5px;
		}
		.info h4 {
			margin: 0 0 5px;
			color: #777;
		}

		.legend {
			text-align: left;
			line-height: 18px;
			color: #555;
		}
		.legend i {
			width: 18px;
			height: 18px;
			float: left;
			margin-right: 8px;
			opacity: 0.7;
		}
  </style>
</head>
<body>
	<!-- banner -->
	<div class="banner top-banner">
		<div class="logo">
			<a href="index.html">TEAM FOUR</a>
		</div>
		<div class="top-nav">
			<span class="menu">MENU</span>
			<ul class="nav1">
				<li><a href="" class="active">Map</a></li>
				<li><a href="index.html">Home</a></li>
			</ul>
			<!-- script-for-menu -->
				<script>
					 $( "span.menu" ).click(function() {
					$( "ul.nav1" ).slideToggle( 300, function() {
					// Animation complete.
						});
						});
				</script>
			<!-- /script-for-menu -->
		</div>
	</div>
	<!-- //banner -->
	<!-- contact -->
	<div class="contact-top">
		<!-- container -->
		<div class="container">
			<div class="contact-info">
				<h3>Price and Details Map</h3>
				<p class="caption">you can easily check the heatmap of rent price in Boston and any house information in details</p>
			</div>
			<div id="map" class="map footer-middle">
				
			</div>
			<script>
var map = L.map('map').setView([42.35, -71.08], 13);
   
  // load a t;ile layer
L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png',
	    {
	      attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://www.zillow.com/research/data/#rental-data">Zillow</a>',
	      maxZoom: 17,
	      minZoom: 9
	    }).addTo(map);
function remap(){
	map.remove();
	map = L.map('map').setView([42.35, -71.08], 13);
	  L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png',
	    {
	      attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://mass.gov/mgis">MassGIS</a>',
	      maxZoom: 17,
	      minZoom: 9
	    }).addTo(map);
}
  function test(){
	  
	  map.remove();
	  // load a t;ile layer
	  map = L.map('map').setView([42.35, -71.08], 13);
	  L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png',
	    {
	      attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://mass.gov/mgis">MassGIS</a>',
	      maxZoom: 17,
	      minZoom: 9
	    }).addTo(map);
  // initialize the map
  
	  var folder = '<%=request.getContextPath()%>' ;
	  var txt = document.getElementById("txt").value;
	  var year = document.getElementById("year").value;
	  var bed = document.getElementById("bed").value;
	  txt+=".geojson";
	  year+=bed;
	  year+=".geojson";
	  <%!String storageConnectionString =
		    "DefaultEndpointsProtocol=http;" +
		    "AccountName=finalprojbostonrent;" +
		    "AccountKey=jmVu9SMLvCgz82JnmtO9BddKdyGeRAVPsFGzFTgvBQ4NOE/Pd2Rz3mZ+riGOG6eICvH6rtI1YDlNjsVr46zcSQ==";
%>
	  
	  var hoodData = "";
  $.getJSON(folder+"/resources/"+year,function(hoodData){
    L.geoJson( hoodData, {
      style: function(feature){
        var fillColor,
            density = feature.properties.density;
        if ( density > 3500 ) fillColor = "#005a32";
        else if ( density > 3000 ) fillColor = "#238b45";
        else if ( density > 2500 ) fillColor = "#41ab5d";
        else if ( density > 2000 ) fillColor = "#74c476";
        else if ( density > 1500 ) fillColor = "#a1d99b";
        else if ( density > 1000 ) fillColor = "#c7e9c0";
        else if ( density > 0 ) fillColor = "#e5f5e0";
        else fillColor = "#f7fcf5";  // no data
        return { color: "#999", weight: 1, fillColor: fillColor, fillOpacity: .6 };
      },
      onEachFeature: function( feature, layer ){
    	  if(feature.properties.density=="0"){
    		  if(feature.properties.Name=="Roslindale(02131)"){
    		  /* layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
    	        		"<form action=\"ViewDetails.htm\" method=\"post\"><input name=\"details\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input type=\"submit\" value=\"Details\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 */				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/6d366d3a-4f61-4b30-abcf-a9fc313a79a0/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
    		  }
 			else if(feature.properties.Name=="Jamaica Plain(02130)"){
			layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/0922bb52-9ad2-4497-8ac2-43a50230da26/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 			}
 			else if(feature.properties.Name=="Mission Hill(02120)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/1bac3c88-5d11-4f81-acc4-3dc00f18f6a5/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Longwood Medical Area(02446)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Kenmore(02116)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/95b21f76-2840-45d5-ad32-7f89c7da4e86/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Leather District(02111)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Chinatown(02111)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/31deaa5a-7207-4e18-af93-17cd71855b04/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="North End(02109)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/e88bfe1d-931e-402a-a5a3-53d45e38ab7f/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Roxbury(02119)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/95b46ff4-cbdd-47f4-a91e-6a42ee9bb19d/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="South End(02118)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/8ae5d21a-31fa-4486-b77e-24a298c882da/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Back Bay(02116)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/16429a3f-2039-400f-a983-1284f665ab93/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="East Boston(02128)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/98f82edd-1de0-4318-8da5-4bfcc3c23998/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Charlestown(02129)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/e04733a9-ecca-4e0c-b729-677850fa82dc/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="West End(02114)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/2876825b-b9de-49af-90c8-1e25689d8804/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Beacon Hill(02108"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/fd979518-bd21-4a05-bf52-d38f8c5b366b/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Downtown(02108)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/fc00ddc1-fe79-4938-886c-ee9fb32052fb/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Fenway(02115)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/458f0b66-8ed0-4bb6-b1ba-e8d98acb2d01/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Brighton(02135)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/ee7c29c7-028d-486b-bf51-8b8840c6fbaa/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="West Roxbury(02132)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/95b46ff4-cbdd-47f4-a91e-6a42ee9bb19d/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Hyde Park(02136)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Mattapan(02126)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Dorchester(02125)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/35d24055-d034-40d3-95e6-d840154323b6/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="South Boston Waterfront(02127)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/ff321183-b7b7-4091-9c34-640756c126cf/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="South Boston(02127)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/ff321183-b7b7-4091-9c34-640756c126cf/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Allston(02134)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/0eedfc94-4fa8-4b52-b673-33701875a920/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else{
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
 		 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 		 	 		
 			}
    		  
    	  }
    	  else{
/*         layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
        		"<form action=\"ViewDetails.htm\" method=\"post\"><input name=\"details\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input type=\"submit\" value=\"Details\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
    	  */
    	  if(feature.properties.Name=="Roslindale(02131)"){
    		  /* layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
    	        		"<form action=\"ViewDetails.htm\" method=\"post\"><input name=\"details\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input type=\"submit\" value=\"Details\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\"1\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 */				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/6d366d3a-4f61-4b30-abcf-a9fc313a79a0/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
    		  }
 			else if(feature.properties.Name=="Jamaica Plain(02130)"){
			layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/0922bb52-9ad2-4497-8ac2-43a50230da26/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 			}
 			else if(feature.properties.Name=="Mission Hill(02120)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/1bac3c88-5d11-4f81-acc4-3dc00f18f6a5/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Longwood Medical Area(02446)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Kenmore(02116)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/95b21f76-2840-45d5-ad32-7f89c7da4e86/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Leather District(02111)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Chinatown(02111)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/31deaa5a-7207-4e18-af93-17cd71855b04/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="North End(02109)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/e88bfe1d-931e-402a-a5a3-53d45e38ab7f/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Roxbury(02119)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/95b46ff4-cbdd-47f4-a91e-6a42ee9bb19d/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="South End(02118)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/8ae5d21a-31fa-4486-b77e-24a298c882da/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Back Bay(02116)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/16429a3f-2039-400f-a983-1284f665ab93/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="East Boston(02128)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/98f82edd-1de0-4318-8da5-4bfcc3c23998/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Charlestown(02129)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/e04733a9-ecca-4e0c-b729-677850fa82dc/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="West End(02114)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/2876825b-b9de-49af-90c8-1e25689d8804/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Beacon Hill(02108"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/fd979518-bd21-4a05-bf52-d38f8c5b366b/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Downtown(02108)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/fc00ddc1-fe79-4938-886c-ee9fb32052fb/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Fenway(02115)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/458f0b66-8ed0-4bb6-b1ba-e8d98acb2d01/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Brighton(02135)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/ee7c29c7-028d-486b-bf51-8b8840c6fbaa/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="West Roxbury(02132)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/95b46ff4-cbdd-47f4-a91e-6a42ee9bb19d/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Hyde Park(02136)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Mattapan(02126)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Dorchester(02125)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/35d24055-d034-40d3-95e6-d840154323b6/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="South Boston Waterfront(02127)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/ff321183-b7b7-4091-9c34-640756c126cf/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="South Boston(02127)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/ff321183-b7b7-4091-9c34-640756c126cf/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else if(feature.properties.Name=="Allston(02134)"){
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'https://app.powerbi.com/groups/me/reports/0eedfc94-4fa8-4b52-b673-33701875a920/ReportSection\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 	 			}
 			else{
 				layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
 		 	     		"<form><input type=\"button\" value=\"Details\"/ onclick=\"window.open(\'http://www.baidu.com\')\"></form><form action=\"Estimate.htm\" method=\"post\"><input name=\"details2\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input name=\"bedroom\" type=\"hidden\" value=\""+bed+"\"><input type=\"submit\" value=\"Estimate\"></form></div>");
 		 	 		
 			}
      }
    }}).addTo(map);
  });
  <%!String fmr321 = ""; %>
  <%!String fmr331 = ""; %>
  <%!String fmr341 = ""; %>
  <%!String fmr351 = ""; %>
  <%!String fmr361 = ""; %>
  <%!String fmr371 = ""; %>
  <%!String fmr381 = ""; %>
  <%!String fmr391 = ""; %>
  <%!String fmr3101 = ""; %>
  <%!String fmr3111 = ""; %>
  <%!String fmr3112 = ""; %>
  <%!String fmr3121 = ""; %>
  <%!String fmr511 = ""; %>
  <%!String fmr521 = ""; %>
  <%!String fmr531 = ""; %>
  <%!String fmr541 = ""; %>
  <%!String fmr551 = ""; %>
  <%!String fmr561 = ""; %>
  <%!String fmr571 = ""; %>
  <%!String fmr581 = ""; %>
  <%!String fmr591 = ""; %>
  <%!String fmr5101 = ""; %>
  <%!String fmr5111 = ""; %>
  <%!String fmr5121 = ""; %>
  <%!String fmr611 = ""; %>
  <%!String fmr621 = ""; %>
  <%!String fmr631 = ""; %>
  <%!String fmr641 = ""; %>
  <%!String fmr651 = ""; %>
  <%!String fmr661 = ""; %>
  <%!String fmr671 = ""; %>
  <%!String fmr411 = ""; %>
  <%!String fmr421 = ""; %>
  <%!String fmr431 = ""; %>
  <%!String fmr441 = ""; %>
  <%!String fmr451 = ""; %>
  <%!String fmr461 = ""; %>
  <%!String fmr471 = ""; %>
  <%!String fmr481 = ""; %>
  <%!String fmr491 = ""; %>
  <%!String fmr4101 = ""; %>
  <%!String fmr4111 = ""; %>
  <%!String fmr4121 = ""; %>
  <% 
		// Use the CloudStorageAccount object to connect to your storage account
		try {
		    CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

		    
		CloudFileClient fileClient = storageAccount.createCloudFileClient();
		
		// Get a reference to the file share
		CloudFileShare share = fileClient.getShareReference("asd");
		//Get a reference to the root directory for the share.
		CloudFileDirectory sampleDir = share.getRootDirectoryReference();

		//Get a reference to the directory that contains the file


		//Get a reference to the file you want to download
		CloudFile mr321 = sampleDir.getFileReference("2013-02-18.geojson");
		CloudFile mr331 = sampleDir.getFileReference("2013-03-18.geojson");
		CloudFile mr341 = sampleDir.getFileReference("2013-04-18.geojson");
		CloudFile mr351 = sampleDir.getFileReference("2013-05-18.geojson");
		CloudFile mr361 = sampleDir.getFileReference("2013-06-18.geojson");
		CloudFile mr371 = sampleDir.getFileReference("2013-07-18.geojson");
		CloudFile mr381 = sampleDir.getFileReference("2013-08-18.geojson");
		CloudFile mr391 = sampleDir.getFileReference("2013-09-18.geojson");
		CloudFile mr3101 = sampleDir.getFileReference("2013-10-18.geojson");
		CloudFile mr3111 = sampleDir.getFileReference("2013-11-18.geojson");
		CloudFile mr3121 = sampleDir.getFileReference("2013-12-18.geojson");
		CloudFile mr3112 = sampleDir.getFileReference("2013-11-21.geojson");
		CloudFile mr511 = sampleDir.getFileReference("2015-01-18.geojson");
		CloudFile mr521 = sampleDir.getFileReference("2015-02-18.geojson");
		CloudFile mr531 = sampleDir.getFileReference("2015-03-18.geojson");
		CloudFile mr541 = sampleDir.getFileReference("2015-04-18.geojson");
		CloudFile mr551 = sampleDir.getFileReference("2015-05-18.geojson");
		CloudFile mr561 = sampleDir.getFileReference("2015-06-18.geojson");
		CloudFile mr571 = sampleDir.getFileReference("2015-07-18.geojson");
		CloudFile mr581 = sampleDir.getFileReference("2015-08-18.geojson");
		CloudFile mr591 = sampleDir.getFileReference("2015-09-18.geojson");
		CloudFile mr5101 = sampleDir.getFileReference("2015-10-18.geojson");
		CloudFile mr5111 = sampleDir.getFileReference("2015-11-18.geojson");
		CloudFile mr5121 = sampleDir.getFileReference("2015-12-18.geojson");
		CloudFile mr611 = sampleDir.getFileReference("2016-01-18.geojson");
		CloudFile mr621 = sampleDir.getFileReference("2016-02-18.geojson");
		CloudFile mr631 = sampleDir.getFileReference("2016-03-18.geojson");
		CloudFile mr641 = sampleDir.getFileReference("2016-04-18.geojson");
		CloudFile mr651 = sampleDir.getFileReference("2016-05-18.geojson");
		CloudFile mr661 = sampleDir.getFileReference("2016-06-18.geojson");
		CloudFile mr671 = sampleDir.getFileReference("2016-07-18.geojson");
		CloudFile mr411 = sampleDir.getFileReference("2014-01-18.geojson");
		CloudFile mr421 = sampleDir.getFileReference("2014-02-18.geojson");
		CloudFile mr431 = sampleDir.getFileReference("2014-03-18.geojson");
		CloudFile mr441 = sampleDir.getFileReference("2014-04-18.geojson");
		CloudFile mr451 = sampleDir.getFileReference("2014-05-18.geojson");
		CloudFile mr461 = sampleDir.getFileReference("2014-06-18.geojson");
		CloudFile mr471 = sampleDir.getFileReference("2014-07-18.geojson");
		CloudFile mr481 = sampleDir.getFileReference("2014-08-18.geojson");
		CloudFile mr491 = sampleDir.getFileReference("2014-09-18.geojson");
		CloudFile mr4101 = sampleDir.getFileReference("2014-10-18.geojson");
		CloudFile mr4111 = sampleDir.getFileReference("2014-11-18.geojson");
		CloudFile mr4121 = sampleDir.getFileReference("2014-12-18.geojson");
		fmr321 = mr321.downloadText();
		fmr331 = mr331.downloadText();
		fmr341 = mr341.downloadText();
		fmr351 = mr351.downloadText();
		fmr361 = mr361.downloadText();
		fmr371 = mr371.downloadText();
		fmr381 = mr381.downloadText();
		fmr391 = mr391.downloadText();
		fmr3101 = mr3101.downloadText();
		fmr3111 = mr3111.downloadText();
		fmr3121 = mr3121.downloadText();
		fmr3112 = mr3112.downloadText();
		fmr511 = mr511.downloadText();
		fmr521 = mr521.downloadText();
		fmr531 = mr531.downloadText();
		fmr541 = mr541.downloadText();
		fmr551 = mr551.downloadText();
		fmr561 = mr561.downloadText();
		fmr571 = mr571.downloadText();
		fmr581 = mr581.downloadText();
		fmr591 = mr591.downloadText();
		fmr5101 = mr5101.downloadText();
		fmr5111 = mr5111.downloadText();
		fmr5121 = mr5121.downloadText();
		fmr611 = mr611.downloadText();
		fmr621 = mr621.downloadText();
		fmr631 = mr631.downloadText();
		fmr641 = mr641.downloadText();
		fmr651 = mr651.downloadText();
		fmr661 = mr661.downloadText();
		fmr671 = mr671.downloadText();
		fmr411 = mr411.downloadText();
		fmr421 = mr421.downloadText();
		fmr431 = mr431.downloadText();
		fmr441 = mr441.downloadText();
		fmr451 = mr451.downloadText();
		fmr461 = mr461.downloadText();
		fmr471 = mr471.downloadText();
		fmr481 = mr481.downloadText();
		fmr491 = mr491.downloadText();
		fmr4101 = mr4101.downloadText();
		fmr4111 = mr4111.downloadText();
		fmr4121 = mr4121.downloadText();


		//Write the contents of the file to the console.

	} catch (Exception invalidKey) {
	    // Handle the exception
		System.out.println("12");
	}// Create the file storage client.%>
	var data = "";
	if(txt=="2013-02-18.geojson"){

		data = JSON.parse('<%=fmr321%>');	
	}
	else if(txt=="2013-03-18.geojson"){
		data = JSON.parse('<%=fmr331%>');
	}
	else if(txt=="2013-04-18.geojson"){
		data = JSON.parse('<%=fmr341%>');
	}
	else if(txt=="2013-05-18.geojson"){
		data = JSON.parse('<%=fmr351%>');
	}
	else if(txt=="2013-06-18.geojson"){
		data = JSON.parse('<%=fmr361%>');
	}
	else if(txt=="2013-07-18.geojson"){
		data = JSON.parse('<%=fmr371%>');
	}
	else if(txt=="2013-08-18.geojson"){
		data = JSON.parse('<%=fmr381%>');
	}
	else if(txt=="2013-09-18.geojson"){
		data = JSON.parse('<%=fmr391%>');
	}
	else if(txt=="2013-10-18.geojson"){
		data = JSON.parse('<%=fmr3101%>');
	}
	else if(txt=="2013-11-18.geojson"){
		data = JSON.parse('<%=fmr3111%>');
	}
	else if(txt=="2013-12-18.geojson"){
		data = JSON.parse('<%=fmr3121%>');
	}
	else if(txt=="2013-11-21.geojson"){
		data = JSON.parse('<%=fmr3111%>');
	}
	else if(txt=="2015-01-18.geojson"){
		data = JSON.parse('<%=fmr511%>');
	}
	else if(txt=="2015-02-18.geojson"){
		data = JSON.parse('<%=fmr521%>');
	}
	else if(txt=="2015-03-18.geojson"){
		data = JSON.parse('<%=fmr531%>');
	}
	else if(txt=="2015-04-18.geojson"){
		data = JSON.parse('<%=fmr541%>');
	}
	else if(txt=="2015-05-18.geojson"){
		data = JSON.parse('<%=fmr551%>');
	}
	else if(txt=="2015-06-18.geojson"){
		data = JSON.parse('<%=fmr561%>');
	}
	else if(txt=="2015-07-18.geojson"){
		data = JSON.parse('<%=fmr571%>');
	}
	else if(txt=="2015-08-18.geojson"){
		data = JSON.parse('<%=fmr581%>');
	}
	else if(txt=="2015-09-18.geojson"){
		data = JSON.parse('<%=fmr591%>');
	}
	else if(txt=="2015-10-18.geojson"){
		data = JSON.parse('<%=fmr5101%>');
	}
	else if(txt=="2015-11-18.geojson"){
		data = JSON.parse('<%=fmr5111%>');
	}
	else if(txt=="2015-12-18.geojson"){
		data = JSON.parse('<%=fmr5121%>');
	}
	else if(txt=="2016-01-18.geojson"){
		data = JSON.parse('<%=fmr611%>');
	}
	else if(txt=="2016-02-18.geojson"){
		data = JSON.parse('<%=fmr621%>');
	}
	else if(txt=="2016-03-18.geojson"){
		data = JSON.parse('<%=fmr631%>');
	}
	else if(txt=="2016-04-18.geojson"){
		data = JSON.parse('<%=fmr641%>');
	}
	else if(txt=="2016-05-18.geojson"){
		data = JSON.parse('<%=fmr651%>');
	}
	else if(txt=="2016-06-18.geojson"){
		data = JSON.parse('<%=fmr661%>');
	}
	else if(txt=="2016-07-18.geojson"){
		data = JSON.parse('<%=fmr671%>');
	}
	else if(txt=="2014-01-18.geojson"){
		data = JSON.parse('<%=fmr411%>');
	}
	else if(txt=="2014-02-18.geojson"){
		data = JSON.parse('<%=fmr421%>');
	}
	else if(txt=="2014-03-18.geojson"){
		data = JSON.parse('<%=fmr431%>');
	}
	else if(txt=="2014-04-18.geojson"){
		data = JSON.parse('<%=fmr441%>');
	}
	else if(txt=="2014-05-18.geojson"){
		data = JSON.parse('<%=fmr451%>');
	}
	else if(txt=="2014-06-18.geojson"){
		data = JSON.parse('<%=fmr461%>');
	}
	else if(txt=="2014-07-18.geojson"){
		data = JSON.parse('<%=fmr471%>');
	}
	else if(txt=="2014-08-18.geojson"){
		data = JSON.parse('<%=fmr481%>');
	}
	else if(txt=="2014-09-18.geojson"){
		data = JSON.parse('<%=fmr491%>');
	}
	else if(txt=="2014-10-18.geojson"){
		data = JSON.parse('<%=fmr4101%>');
	}
	else if(txt=="2014-11-18.geojson"){
		data = JSON.parse('<%=fmr4111%>');
	}
	else{
		data = JSON.parse('<%=fmr4121%>');
	}


    var ratIcon = L.icon({
      iconUrl: 'http://humanrights.gradeeight.org/homelessnessca/Home.png',
      iconSize: [60,50]
    });
    var rodents = L.geoJson(data,{
  
      pointToLayer: function(feature,latlng){
        var marker = L.marker(latlng,{icon: ratIcon});
        
        marker.bindPopup('Bedroom: '+feature.properties.Bedroom + '<br/>' + 'Price: '+feature.properties.Price);
        return marker;
      }
    });
    var clusters = L.markerClusterGroup();
    clusters.addLayer(rodents);
    map.addLayer(clusters);
 
	var info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h4>Rent Price</h4>' ;
	};

	info.addTo(map);
	function getColor(d) {
		return d > 3500   ? '#005a32' :
				d > 3000 ? '#238b45' :
		       d > 2500  ? '#41ab5d' :
		       d > 2000  ? '#74c476' :
		       d > 1500  ? '#a1d99b' :
		       d > 1000   ? '#c7e9c0' :
		       d > 0   ? '#e5f5e0' :
		                  '#f7fcf5';
	}
  var legend = L.control({position: 'bottomright'});

  legend.onAdd = function (map) {

      var div = L.DomUtil.create('div', 'info legend'),
          grades = [0, 1000, 1500,2000,2500,3000,3500],
          labels = [];

      // loop through our density intervals and generate a label with a colored square for each interval
      for (var i = 0; i < grades.length; i++) {
          div.innerHTML +=
              '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
              grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
      }

      return div;
  };

  legend.addTo(map);
 }
 
  </script>
			<div class="mail-grids">
				<div class="col-md-6 mail-grid-left wow fadeInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
					<h3>Intro</h3>
					<h5>put some introductions here <span>and here </span></h5>
					<h4>Team members</h4>
					<p>Jianxing Lu
						<span>Qiaomin Ling</span>
						Wenjin Cao
					</p>
					<h4>Get In Touch</h4>
					<p>Telephone: +1 234 567 9871
						<span>FAX: +1 234 567 9871</span>
						E-mail: <a href="mailto:info@example.com">mail@example.com</a>
					</p>
				</div>
				<div class="col-md-6 contact-form">
				<h3>Option</h3>
					<form>
					<h5>Select month to check every rent info</h5><br/>
						<select id="txt" name="txt">
		  <option value ="2013-02-18" >2013-02-18</option>
		  <option value ="2013-03-18">2013-03-18</option>
		  <option value ="2013-04-18">2013-04-18</option>
		  <option value ="2013-05-18">2013-05-18</option>
		  <option value ="2013-06-18">2013-06-18</option>
		  <option value ="2013-07-18">2013-07-18</option>
		  <option value ="2013-08-18">2013-08-18</option>
		  <option value ="2013-09-18">2013-09-18</option>
		  <option value ="2013-10-18">2013-10-18</option>
		  <option value ="2013-11-18">2013-11-18</option>
		  <option value ="2013-11-21">2013-11-21</option>
		  <option value ="2013-12-18">2013-12-18</option>
		  <option value ="2014-01-18">2014-01-18</option>
		  <option value ="2014-02-18">2014-02-18</option>
		  <option value ="2014-03-18">2014-03-18</option>
		  <option value ="2014-04-18">2014-04-18</option>
		  <option value ="2014-05-18">2014-05-18</option>
		  <option value ="2014-06-18">2014-06-18</option>
		  <option value ="2014-07-18">2014-07-18</option>
		  <option value ="2014-08-18">2014-08-18</option>
		  <option value ="2014-09-18">2014-09-18</option>
		  <option value ="2014-10-18">2014-10-18</option>
		  <option value ="2014-11-18">2014-11-18</option>
		  <option value ="2014-12-18">2014-12-18</option>
		  <option value ="2015-01-18">2015-01-18</option>
		  <option value ="2015-02-18">2015-02-18</option>
		  <option value ="2015-03-18">2015-03-18</option>
		  <option value ="2015-04-18">2015-04-18</option>
		  <option value ="2015-05-18">2015-05-18</option>
		  <option value ="2015-06-18">2015-06-18</option>
		  <option value ="2015-07-18">2015-07-18</option>
		  <option value ="2015-08-18">2015-08-18</option>
		  <option value ="2015-09-18">2015-09-18</option>
		  <option value ="2015-10-18">2015-10-18</option>
		  <option value ="2015-11-18">2015-11-18</option>
		  <option value ="2015-12-18">2015-12-18</option>
		  <option value ="2016-01-18">2016-01-18</option>
		  <option value ="2016-02-18">2016-02-18</option>
		  <option value ="2016-03-18">2016-03-18</option>
		  <option value ="2016-04-18">2016-04-18</option>
		  <option value ="2016-05-18">2016-05-18</option>
		  <option value ="2016-06-18">2016-06-18</option>
		  <option value ="2016-07-18">2016-07-18</option>
		</select><br>
		<h5>Select year to check heat map</h5><br/>
		<select id="year" name="year">
		  <option value ="2013" >2013</option>
		  <option value ="2014" >2014</option>
		  <option value ="2015" >2015</option>
		  <option value ="2016" >2016</option>
		  <option value ="2016o" >2016(Ourselves)</option>
		</select>
		<h5>Select number of bedroom for heatmap</h5><br/>
		<select id="bed" name="bed">
		  <option value ="0" >Studio</option>
			<option value ="1" >1 Bedroom</option>
			<option value ="2" >2 Bedroom</option>
			<option value ="3" >3 Bedroom</option>
			<option value ="4" >4 Bedroom</option>
			<option value ="5" >5 Bedroom</option>
		</select>
		<input type="button" value="View in map" onclick="test()"/>
		<input type="button" value="Refresh map" onclick="remap()"/>
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<!-- //container -->
	</div>

	<!-- //contact -->
	<!-- footer -->
	<div class="footer">
		<!-- container -->
		<div class="container">
			<div class="footer-top">
				<div class="footer-logo">
					<a href="index.html">TEAM FOUR</a>
				</div>
				<div class="footer-icons">
					<ul>
						<li><a href="#" class="facebook"> </a></li>
						<li><a href="#" class="facebook twitter"> </a></li>
						<li><a href="#" class="facebook chrome"> </a></li>
						<li><a href="#" class="facebook dribbble"> </a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<!-- //container -->
		
	</div>
	<!-- //footer -->
</body>	
</html>