
<%-- 	${requestScope.zipcode }${requestScope.name }    --%>

	
	
	
	
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css"
	  href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link href="resources/css/carousel.css" 
      rel="stylesheet">
<link rel="stylesheet" 
      href="css/searchBar.css">
<link rel="stylesheet" 
      href="skeleton/base.css">
<link rel="stylesheet" 
      href="skeleton/skeleton.css">
<link rel="stylesheet" 
      href="skeleton/layout.css">
<link rel="stylesheet" 
      href="font-awesome/css/font-awesome.css">
<link rel="stylesheet" 
      href="smoothie-website.css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600"
	  rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css"
	  rel="stylesheet">
<link rel="stylesheet" 
      type="text/css" 
      media="screen"
      href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">

<script type="text/javascript"
        src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript"
	    src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script async src="http://c.cnzz.com/core.php"></script>
<script type="text/javascript"
	src="https://raw.githubusercontent.com/joewalnes/smoothie/master/smoothie.js"></script>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/bootstrap/css/bootstrap.css" />"
	rel="stylesheet" type="text/css" />
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link href="resources/css/carousel.css" rel="stylesheet">

<title>Team4 Final Project</title>


</head>

<body>
	<br />
	<br />
	<div>

		<div id="body_head">

			<!-- Carousel ================================================== -->
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" 
					    data-slide-to="0" class="active"></li>
					    
					<li data-target="#myCarousel" 
					    data-slide-to="1"></li>
					    
					<li data-target="#myCarousel" 
					    data-slide-to="2"></li>
					    
					<li data-target="#myCarousel" 
					    data-slide-to="3"></li>
				</ol>
				
				<div class="carousel-inner" 
				     role="listbox">

					<div class="item active">
						<img class="first-slide"
							src="${pageContext.servletContext.contextPath}/resources/image/l5.jpeg"
							alt="First slide" height=40% width=100%>
						<div class="container">

							<div class="carousel-caption">

								<h1>Team Four Final Project</h1>
								<p></p>
								<p></p>
							</div>
						</div>
					</div>


					<div class="item">
						<img class="second-slide"
							src="${pageContext.servletContext.contextPath}/resources/image/l4.jpg"
							alt="Second slide" height=40% width=100%>
						<div class="container">
							<div class="carousel-caption">
								<h1>Wenjin Cao, Jianxing Lu, Qiaomin Ling</h1>
								<p></p>
								<p></p>
								<p></p>

							</div>
						</div>
					</div>


					<div class="item">
						<img class="third-slide"
							src="${pageContext.servletContext.contextPath}/resources/image/l7.jpg"
							alt="Third slide" height=40% width=100%>
						<div class="container">
							<div class="carousel-caption">
								<!-- <h1>Quick Estimate</h1> -->
								<input class="btn  btn-outline-secondary  btn-lg" type="button"
									value="quick estimate" id="btn" onclick="quick();" /><br />
								<p></p>
								<p></p>
								<p></p>
							</div>
						</div>
					</div>


					<div class="item">
						<img class="fourth-slide"
							src="${pageContext.servletContext.contextPath}/resources/image/l3.jpg"
							alt="Fourth slide" height=40% width=100%>
						<div class="container">
							<div class="carousel-caption">
								<!-- <h1>Detail Estimate</h1> -->
								<input class="btn  btn-outline-secondary  btn-lg" type="button"
									value="detail estimate" id="btn" onclick="detail();" /> <br />
								<p></p>
								<p></p>
								<p></p>

							</div>
						</div>
					</div>

				</div>
				<a class="left carousel-control" href="#myCarousel" role="button"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#myCarousel" role="button"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			<!-- /.carousel ================================================================-->

		</div>


	</div>

	<div id="input"></div>

	<div id="image"></div>





	<script type="text/javascript">
		function Open() {

			var largeImage = document.getElementById('image');
			largeImage.style.display = 'block';
			largeImage.style.width = 200 + "px";
			largeImage.style.height = 200 + "px";
			var url = largeImage.getAttribute('src');
			window
					.open(
							src = "${pageContext.servletContext.contextPath}/resources/image/zip.jpg",
							'Image',
							'width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');

		}


		function dcheck() {
			/* var zip = document.getElementById("zipcode").value;
			var bed = document.getElementById("bedroom").value; */
			var bath = document.getElementById("bathroom").value;

			/* if (!(bed.match("1") || bed.match("2") || bed.match("3")
					|| bed.match("4") || bed.match("5"))) {
				alert("Please enter valid bed room number! Valid bedroom number will be 1-5!");
			} else  */if (!(bath.match("1") || bath.match("1.5")
					|| bath.match("2") || bath.match("2.5") || bath.match("3"))) {
				alert("Please enter valid bath room number! Valid bathroom number will be 1, 1.5, 2, 2.5, 3!");
			}
			else {
				document.forms["detailcheck"].submit();

			}
		}

		function quick() {

			var htmlstr = '<center><div><input class="btn  btn-primary"  type="button" value="Open ZipCode Map" id="btn" onclick="Open();"  />  <br/> <br/>  <br/>';

			htmlstr += '<font size="5" color="8A2BE2">Quick Estimate</font>';

			htmlstr += '<br/>';
			htmlstr += '<br/>';

			htmlstr += '   <form action="estimatequick.htm" method="POST" name="quickcheck">   ';
			htmlstr += '<table class="table table-bordered">';
			
			
			/* -------------- ZIPCODE  --------------*/
			/* 由上个界面传递参数 */
			htmlstr += '<tr class="danger" >';
			htmlstr += '<td align="right"><font size="3">Zipcode:&nbsp </font></td>';
			htmlstr += '<td> ${requestScope.zipcode } </td>';
			htmlstr += '<input type="hidden" name="zipcode" id="zipcode" value=${requestScope.zipcode }   />';
			htmlstr += '</tr>';

			/* -------------- HOUSING CATEGORY  --------------*/

			htmlstr += '<tr class="success" >';
			htmlstr += '<td align="right" ><font size="3">Housing Category: &nbsp</font></td>';
			htmlstr += '<td><input type="radio" name="htype" id="htype" value="2" checked="checked"/>';
			htmlstr += '<font size="3">Apartment </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="warning" >';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="htype" id="htype" value="1"/>';
			htmlstr += '<font size="3">House</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="success">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="htype" id="htype" value="3"/>';
			htmlstr += '<font size="3">Condos</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="warning" >';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="htype" id="htype" value="4"/>';
			htmlstr += '<font size="3">Townhouse</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			/* -------------- ROOM TYPE  --------------*/

			htmlstr += '<tr  class="info" >';
			htmlstr += '<td align="right" ><font size="3">Room Type: &nbsp</font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="room" id="room" value="studio" checked="checked"/>';
			htmlstr += '<font size="3">Studio </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr   class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="1b1b" />';
			htmlstr += '<font size="3">One Bed One Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="info" >';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="2b1b" />';
			htmlstr += '<font size="3">Two Bed One Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr   class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="2b2b" />';
			htmlstr += '<font size="3">Two Bed Two Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="3b1b" />';
			htmlstr += '<font size="3">Three Bed One Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="3b2b" />';
			htmlstr += '<font size="3">Three Bed Two Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="4b2b" />';
			htmlstr += '<font size="3">Four Bed Two Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '</table>';

			htmlstr += '<br/>';
			htmlstr += '<br/>';
			htmlstr += '<td><input  class="btn  btn-primary" type="submit"   value="Quick Estimate" /></td>';

			htmlstr += '</form>    </div> <center/> ';

			document.getElementById("input").innerHTML = htmlstr;

		}

		
		
		
		function detail() {

			var htmlstr = '<center><div><input class="btn  btn-primary"  type="button" value="Open ZipCode Map" id="btn" onclick="Open();"  />  <br/> <br/>  <br/>';

			htmlstr += '<font size="5" color="8A2BE2">Detail Estimate</font>';
			htmlstr += '<br/>';
			htmlstr += '<br/>';
			htmlstr += '<form action="estimate.htm" method = "POST" name="detailcheck"> ';
			
			htmlstr += '<table  class="table table-bordered">';
			
			
			/* -------------- ZIPCODE  --------------*/
			/* 由上个界面传递参数 */
			htmlstr += '<tr  class="active">';
			htmlstr += '<td align="right"><font size="3">Zipcode: &nbsp  </font></td>';
			htmlstr += '<td> ${requestScope.zipcode } ';
			htmlstr += '<input type="hidden" name="zipcode" id="zipcode" value=${requestScope.zipcode } />   </td> ';
			htmlstr += '</tr>';
			
			/* --------------  BEDROOM   --------------*/  
			/* 由上个界面传递参数 */
			htmlstr += '<tr  class="success">';
			htmlstr += '<td align="right"><font size="3">Bedroom: &nbsp </font></td>';
			htmlstr += '<td> ${requestScope.name ';
			htmlstr += '<input type="hidden" name="bedroom" id="bedroom" value=${requestScope.name />   </td> ';
			
			htmlstr += '</tr>';
			
			/* --------------  BATHROOM   --------------*/
			htmlstr += '<tr  class="warning">';
			htmlstr += '<td align="right"><font size="3">Bathroom: &nbsp  </font></td>';
			htmlstr += '<td><input type="text" name="bathroom" id="bathroom"/></td>';
			htmlstr += '</tr>';
			
			/* --------------  TYPE   --------------*/
			htmlstr += '<tr  class="info">';
			htmlstr += '<td align="right"><font size="3">Type:&nbsp   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="type" id="type" value="1" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp House </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="type" id="type" value="2" />';
			htmlstr += '<font size="3"> &nbsp Apartment</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="type" id="type" value="3" />';
			htmlstr += '<font size="3"> &nbsp Condos</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="type" id="type" value="4" />';
			htmlstr += '<font size="3"> &nbsp Townhouse</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  PARKING   --------------*/
			htmlstr += '<tr  class="warning">';
			htmlstr += '<td align="right"><font size="3">Parking:&nbsp   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="parking" id="parking" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No Parking </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="warning">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="parking" id="parking" value="1" />';
			htmlstr += '<font size="3"> &nbsp Indoor Parking</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="warning">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="parking" id="parking" value="2" />';
			htmlstr += '<font size="3"> &nbsp On Street Parking</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  24-EMERGENCY   --------------*/
			htmlstr += '<tr  class="active">';
			htmlstr += '<td align="right"><font size="3">24-Hour Concierge: &nbsp  </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="emergency" id="emergency" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp  &nbsp </font>';
			htmlstr += '<input type="radio" name="emergency" id="emergency" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  GYM   --------------*/
			htmlstr += '<tr  class="success">';
			htmlstr += '<td align="right"><font size="3">GYM: &nbsp </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="gym" id="gym" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp &nbsp </font>';
			htmlstr += '<input type="radio" name="gym" id="gym" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  AC   --------------*/
			htmlstr += '<tr class="warning">';
			htmlstr += '<td align="right"><font size="3">AC: &nbsp</font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="ac" id="ac" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp &nbsp </font>';
			htmlstr += '<input type="radio" name="ac" id="ac" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  WATER-FEE   --------------*/
			htmlstr += '<tr class="danger">';
			htmlstr += '<td align="right"><font size="3">Water-Fee Included: &nbsp  </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="water" id="water" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp &nbsp </font>';
			htmlstr += '<input type="radio" name="water" id="water" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  ELECTRONIC-FEE   --------------*/
			htmlstr += '<tr class="info">';
			htmlstr += '<td align="right"><font size="3">Electronic-Fee Included:&nbsp   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="electronic" id="electronic" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp &nbsp </font>';
			htmlstr += '<input type="radio" name="electronic" id="electronic" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  HEAT-FEE   --------------*/
			htmlstr += '<tr  class="active">';
			htmlstr += '<td align="right"><font size="3">Heat-Fee Included:&nbsp  </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="heat" id="heat" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp &nbsp </font>';
			htmlstr += '<input type="radio" name="heat" id="heat" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  DISHWASHER   --------------*/
			htmlstr += '<tr  class="success">';
			htmlstr += '<td align="right"><font size="3">Dishwasher Included:&nbsp   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="dishwasher" id="dishwasher" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp &nbsp </font>';
			htmlstr += '<input type="radio" name="dishwasher" id="dishwasher" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  MICROWAVE   --------------*/
			htmlstr += '<tr  class="warning">';
			htmlstr += '<td align="right"><font size="3">Microwave Included:&nbsp   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="microwave" id="microwave" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No &nbsp &nbsp </font>';
			htmlstr += '<input type="radio" name="microwave" id="microwave" value="1" />';
			htmlstr += '<font size="3"> &nbsp Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  LAUNDRY   --------------*/
			htmlstr += '<tr class="active">';
			htmlstr += '<td align="right"><font size="3">Laundry:&nbsp   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="laundry" id="laundry" value="0" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp No</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			htmlstr += '<tr class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="laundry" id="laundry" value="1" />';
			htmlstr += '<font size="3"> &nbsp In Unit</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="laundry" id="laundry" value="2" />';
			htmlstr += '<font size="3"> &nbsp In Building</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  TRAVEL   --------------*/
			htmlstr += '<tr class="success">';
			htmlstr += '<td align="right"><font size="3">Walk To Public Transportation: &nbsp </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="travel" id="travel" value="1" checked="checked"/>';
			htmlstr += '<font size="3"> &nbsp Within 10 Mins</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="success">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="travel" id="travel" value="2" />';
			htmlstr += '<font size="3"> &nbsp 10-20 Mins</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="success">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="travel" id="travel" value="3" />';
			htmlstr += '<font size="3"> &nbsp More Than 20 Mins</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '</table>';
			htmlstr += '<br/>';
			htmlstr += '<br/>';
			
			htmlstr += '<input class="btn  btn-primary"  type="button"   value="Detail Estimate"  onclick="dcheck()" />';
			htmlstr += '</form>  </div> <center/>';

			document.getElementById("input").innerHTML = htmlstr;
		}
		
	</script>


</body>
</html>




	