package strong.app;



import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.app.jianxing.exception.AdException;
import com.app.jianxing.model.Taxi;



/**
 *
 * @author lu
 */
public class TaxiDAO extends DAO {

    public TaxiDAO() {
    }

    public Taxi get(String taxiname, String taxipwd)
            throws AdException {
        try {
            begin();
            Query q = getSession().createQuery("from Taxi where taxiname = :taxiname and taxipwd = :taxipwd");
            q.setString("taxiname", taxiname);
            q.setString("taxipwd", taxipwd);
            Taxi taxi = (Taxi) q.uniqueResult();
            commit();
            return taxi;
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not get taxi " + taxiname, e);
        }
    }

//    public Taxi create(String taxiname, String password, String firstName, String lastName)
//            throws AdException {
//        try {
//            begin();
//            System.out.println("inside DAO");
//
//            Taxi taxi=new Taxi();
//            
//            taxi.setFname(firstName);
//            taxi.setLname(lastName);
//            
//            taxi.setTaxiname(taxiname);
//            taxi.setTaxipwd(password);
//            
//            getSession().save(taxi);
//            
//            commit();
//            return taxi;
//        } catch (HibernateException e) {
//            rollback();
//            //throw new AdException("Could not create taxi " + taxiname, e);
//            throw new AdException("Exception while creating taxi: " + e.getMessage());
//        }
//    }
//
//    public void delete(Taxi taxi)
//            throws AdException {
//        try {
//            begin();
//            getSession().delete(taxi);
//            commit();
//        } catch (HibernateException e) {
//            rollback();
//            throw new AdException("Could not delete taxi " + taxi.getTaxiname(), e);
//        }
//    }
}
