package com.app.jianxing.model;
// Generated 2016-4-5 16:47:36 by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Station generated by hbm2java
 */
@Entity
@Table(name = "station", catalog = "final")
public class Station implements java.io.Serializable {

	private Integer stationId;
	private String stationName;
	private Set<Vehicle> vehicles = new HashSet<Vehicle>(0);

	public Station() {
	}

	public Station(String stationName) {
		this.stationName = stationName;
	}

	public Station(String stationName, Set<Vehicle> vehicles) {
		this.stationName = stationName;
		this.vehicles = vehicles;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "station_id", unique = true, nullable = false)
	public Integer getStationId() {
		return this.stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	@Column(name = "station_name", nullable = false, length = 45)
	public String getStationName() {
		return this.stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "vehicle_has_station", catalog = "final", joinColumns = {
			@JoinColumn(name = "station_station_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "vehicle_vehicle_id", nullable = false, updatable = false) })
	public Set<Vehicle> getVehicles() {
		return this.vehicles;
	}

	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

}
