<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Visual Admin Dashboard - Preferences</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
        <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>        <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/bootstrap-filestyle.min.js"/>"></script>  <!-- http://markusslima.github.io/bootstrap-filestyle/ -->
    <script type="text/javascript" src="<c:url value="resources/js/templatemo-script.js"/>"></script>  
    <link href="<c:url value='http://fonts.useso.com/css?family=Open+Sans:400,300,400italic,700'/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="resources/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/css/templatemo-style.css"/>" rel="stylesheet">

	<script>
   function confirmop(){
	   document.forms["AddExpire"].submit();
   }
</script>
<script>
                function check(){
                	var id = document.getElementById("cardid").value;
                	var user = document.getElementById("username").value;
                	if(id ==0||isNaN(id)){
                		document.getElementById("cardlabel").innerHTML="Are U Crazy?? ";
                		document.getElementById("cardlabel").style.color="red";
                	}
                	else{
                		var xmlhttp;
                        if (window.XMLHttpRequest){
                            xmlhttp=new XMLHttpRequest();
                        }
                        else{
                            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange=function(){
                            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                            	alert(xmlhttp.responseText);
                            	if(xmlhttp.responseText=="false"){
                            		document.getElementById("cardlabel").innerHTML="Card ID: (Your Balance: ${requestScope.user.userBalance })";
                            		document.getElementById("cardlabel").style.color="green";
                            	}
                            	else if(xmlhttp.responseText=="ture"){
                            		document.getElementById("cardlabel").innerHTML="U don't have this card :(";
                            		document.getElementById("cardlabel").style.color="red";
                            	}
                            	else{
                            		document.getElementById("cardlabel").innerHTML="What's that??";
                            		document.getElementById("cardlabel").style.color="red";
                            	}
                            }
                          }
                        xmlhttp.open("GET","checkcard.htm?cardid="+id+"&user="+user,true);
                        xmlhttp.send();
                	}
                	
                }
                </script>
  </head>
  <body>
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>Hi Smart ${requestScope.user.userFname} :)</h1>
        </header>
        <div class="profile-photo-container">
          <img src="<c:url value="resources/images/${Math.floor(Math.random()*10+1) }.jpg" />" alt="Profile Photo" class="img-responsive">
          <div class="profile-photo-overlay"></div>
        </div>
        <!-- Search box -->
<%--         <form class="templatemo-search-form" role="search">
          <div class="input-group">
              <button type="submit" class="fa fa-search"></button>
              <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
          </div>
        </form> --%>
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
          </div>
        <nav class="templatemo-left-nav">
          <ul>
<!--             <li><a href="index.html"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-bar-chart fa-fw"></i>Charts</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-database fa-fw"></i>Data Visualization</a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker fa-fw"></i>Maps</a></li>
            <li><a href="manage-users.html"><i class="fa fa-users fa-fw"></i>Manage Users</a></li> -->
            <li><a href=><i class="fa fa-sliders fa-fw"></i>Let's Go!!</a></li>
            <li><a href="havearest.htm?user=${requestScope.user }" class="active"><i class="fa fa-users fa-fw"></i>Have a Rest</a></li>
            <li><a href="index"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>
        </nav>
      </div>
      <!-- Main content -->
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a  class="active" >Add Card</a></li>
                <li><a>Add Balance</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="templatemo-content-container">
          <div class="templatemo-content-widget white-bg">
            <h2 class="margin-bottom-10">Card Details</h2>
            <p> All are my treasure, GET AWAY FROM THEM :D</p>
            <div class="col-1">
              <div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">
                <i class="fa fa-times"></i>
                
                <div class="panel-heading templatemo-position-relative"><h2 class="text-uppercase">Stations Information</h2></div>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <td>Car Id</td>
                        <td>Expire Date</td>

                      </tr>
                    </thead>
                    <tbody>
               <c:forEach var="card" items="${requestScope.cards}">
                      <tr>
                        <td>${card.cardId }</td>
                        <td>${card.cardExpire }</td>

                      </tr>
                    </c:forEach>       
                    </tbody>
                  </table>    
                </div>                          
              </div>
              <div class="form-group text-right">
              <button type="button" class="templatemo-blue-button" onclick="location='http://localhost:8080/security/addcard.htm?user='+'${requestScope.user}'">Add</button>
                     </div>
            </div> 
            <form name="AddExpire" action="addexpire.htm" class="templatemo-login-form" method="post" enctype="multipart/form-data">
              
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label id="cardlabel" for=""cardid"">Card ID (Your Balance: ${requestScope.user.userBalance })</label>
                    <input name="cardid" type="text" class="form-control" id="cardid" onkeyup="check()"/>    
                               
                </div>
                
                <div class="col-lg-6 col-md-6 form-group">
                  <div>
                    <label class="control-label templatemo-block">Operations</label> 
                    <div class="templatemo-block margin-bottom-5">
                      <input type="radio" name="operations" id="r1" value="one" checked>
                      <label for="r1" class="font-weight-400"><span></span>1 Day Pass $7</label>
                    </div>
                    <div class="templatemo-block margin-bottom-5">
                      <input type="radio" name="operations" id="r2" value="seven">
                      <label for="r2" class="font-weight-400"><span></span>Weekly Pass $19</label>
                    </div>
                    <div class="templatemo-block margin-bottom-5">
                      <input type="radio" name="operations" id="r3" value="thirty">
                      <label for="r3" class="font-weight-400"><span></span>Monthly Pass $75</label>
                    </div>                    
                  </div>                  
                </div> 
                </div>
                <input type="hidden" value="${requestScope.user}" name="balance" id="username"/>
                <div class="form-group text-right">
              <button type="button" class="templatemo-blue-button" onclick="confirmop()">Confirm</button>
                <button type="button" class="templatemo-white-button" onclick="location='http://localhost:8080/security/addbalance.htm?user=${requestScope.user}'">Add Balance</button>
                </div>
                
            </form>
              
          </div>
         
        </div>
        <footer class="text-right">
            <p>Copyright &copy; Jianxing Lu
            | Final Project</p>
          </footer>
      </div>
    </div>

    <!-- JS -->
      <!-- Templatemo Script -->
  </body>
</html>
