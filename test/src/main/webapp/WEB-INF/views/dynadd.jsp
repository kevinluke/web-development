<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="col-lg-6 col-md-6 form-group"> 
                  <label class="control-label templatemo-block">Station List</label>                 
                  <select id="stations" multiple class="templatemo-multi-select form-control" style="overflow-y: scroll;">
                    <c:forEach var="station" items="${requestScope.stationList}">
                    	<option value="${station.stationName}">${station.stationName}</option>
                    </c:forEach>
                                
                  </select>
                </div>
                
                <div class="form-group text-right"> 
                <button type="button" class="templatemo-blue-button" onclick="addStation('${requestScope.vehicle.vehicleName}')">Confirm</button>
                <button type="button" class="templatemo-white-button" onclick="location='http://localhost:8080/security/createstation.htm'">Create Station</button>
                </div>