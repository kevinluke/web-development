package com.app.jianxing.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.app.jianxing.dao.OrderDAO;
import com.app.jianxing.model.Order;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	@Qualifier("orderHibernateDAOImpl")
	private OrderDAO orderDAO;
	
	@Override
	public void addOrder(Order order) {
		// TODO Auto-generated method stub
		orderDAO.addOrder(order);
	}

	@Override
	public void updateOrder(Order order) {
		// TODO Auto-generated method stub
		orderDAO.updateOrder(order);
	}

	@Override
	public Collection<Order> listOrder() {
		// TODO Auto-generated method stub
		return orderDAO.listOrder();
	}

	@Override
	public Order getOrder(String ordername) {
		// TODO Auto-generated method stub
		return orderDAO.getOrder(ordername);
	}

	@Override
	public void deleteOrder(int orderid) {
		// TODO Auto-generated method stub
		orderDAO.deleteOrder(orderid);
	}


	@Override
	public Collection<Order> SearchOrder(int orderid) {
		// TODO Auto-generated method stub
		return orderDAO.SearchOrder(orderid);
	}

	@Override
	public Order getOrderbyID(int id) {
		// TODO Auto-generated method stub
		return orderDAO.getOrderbyID(id);
	}

	@Override
	public List<Object> getAllByUser(int userid) {
		// TODO Auto-generated method stub
		return orderDAO.getAllByUser(userid);
	}

	@Override
	public List<Object> getAllByTaxi(int taxiid) {
		// TODO Auto-generated method stub
		return orderDAO.getAllByTaxi(taxiid);
	}

	@Override
	public List<Order> getAll() {
		// TODO Auto-generated method stub
		return orderDAO.getAll();
	}

}
