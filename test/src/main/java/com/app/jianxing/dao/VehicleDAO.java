package com.app.jianxing.dao;

import java.util.Collection;
import java.util.List;

import com.app.jianxing.model.Station;
import com.app.jianxing.model.Vehicle;

public interface VehicleDAO {
	public void addVehicle(Vehicle vehicle);
	public void updateVehicle(Vehicle vehicle);
	public Collection<Vehicle> listVehicle();
	public Vehicle checkVehicle(String vehiclename, String vehiclepwd);
	public Vehicle getVehicle(String vehiclename);
	public void deleteVehicle(String vehiclename);
	public List<Vehicle> getAll();
	public Collection<Vehicle> SearchVehicle(String vehiclename);
}
