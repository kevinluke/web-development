package com.app.jianxing.dao;

import java.util.Collection;
import java.util.List;

import com.app.jianxing.model.Station;

public interface StationDAO {
	public void addStation(Station station);
	public void updateStation(Station station);
	public Collection<Station> listStation();
	public Station checkStation(String stationname, String stationpwd);
	public Station getStation(String stationname);
	public void deleteStation(int stationid);
	public List<Station> getAll();
	public Collection<Station> SearchStation(String stationname);
	public Station getStationbyID(int id);
}
