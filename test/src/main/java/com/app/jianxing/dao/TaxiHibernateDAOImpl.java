package com.app.jianxing.dao;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.jianxing.model.Taxi;
import com.app.jianxing.model.User;

@Repository
public class TaxiHibernateDAOImpl implements TaxiDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addTaxi(Taxi taxi) {
		sessionFactory.getCurrentSession().saveOrUpdate(taxi);
		sessionFactory.getCurrentSession().flush();		
		
	}

	@Override
	public void updateTaxi(Taxi taxi) {
		sessionFactory.getCurrentSession().saveOrUpdate(taxi);
		sessionFactory.getCurrentSession().flush();
		
	}

	@Override
	public Collection<Taxi> listTaxi() {
		return sessionFactory.getCurrentSession().createQuery("from Taxi").list();
	}

	@Override
	public Taxi checkTaxi(String taxiname, String taxipwd) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Taxi "+"where taxiname =:taxiname and taxipwd =:taxipwd");
		query.setParameter("taxiname", taxiname);
		query.setParameter("taxipwd", taxipwd);
		Taxi taxi = (Taxi) query.uniqueResult();
		return taxi;
	}

	@Override
	public Taxi getTaxi(int taxiid) {
		return (Taxi) sessionFactory.getCurrentSession().get(Taxi.class, taxiid);
	}

	@Override
	public void deleteTaxi(int taxiid) {
		Taxi taxi = getTaxi(taxiid);
		if (taxi != null) {
			sessionFactory.getCurrentSession().delete(taxi);
			sessionFactory.getCurrentSession().flush();
		}
		
	}

	@Override
	public Collection<Taxi> SearchTaxi(String name) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Taxi where taxiname =:taxiname");
		query.setParameter("taxiname", name);
		Collection<Taxi> result = query.list();
		return result;
	}

	@Override
	public Taxi getTaxiByName(String taxiname) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Taxi "+"where taxiname =:taxiname");
		query.setParameter("taxiname", taxiname);
		Taxi user = (Taxi) query.uniqueResult();
		return user;
	}

}
