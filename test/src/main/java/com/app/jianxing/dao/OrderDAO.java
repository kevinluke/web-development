package com.app.jianxing.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.app.jianxing.model.Order;

public interface OrderDAO {
	public void addOrder(Order order);
	public void updateOrder(Order order);
	public Collection<Order> listOrder();
	public Order getOrder(String ordername);
	public void deleteOrder(int orderid);
	public List<Object> getAllByUser(int userid);
	public List<Order> getAll();
	public List<Object> getAllByTaxi(int taxiid);
	public Collection<Order> SearchOrder(int orderId);
	public Order getOrderbyID(int id);
}
