package com.app.jianxing.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.jianxing.service.VehicleService;

@Controller
@RequestMapping("/adminpage.htm")
public class AdminRedirect {
	@Autowired
	private VehicleService vehicleService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(method = RequestMethod.GET)
	public String AdminRedirect(Model model){
		System.out.print(vehicleService.getAll().size());
		model.addAttribute("vehicleList", vehicleService.getAll());
		return "adminpage";
	}
}
