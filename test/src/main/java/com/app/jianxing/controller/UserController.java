package com.app.jianxing.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.app.jianxing.model.Card;
import com.app.jianxing.model.Order;
import com.app.jianxing.model.Station;
import com.app.jianxing.model.User;
import com.app.jianxing.model.Vehicle;
import com.app.jianxing.service.CardService;
import com.app.jianxing.service.OrderService;
import com.app.jianxing.service.StationService;
import com.app.jianxing.service.UserService;
import com.app.jianxing.service.VehicleService;

@Controller
public class UserController {

	@Autowired
    private StationService stationService;
	@Autowired
    private VehicleService vehicleService;
	@Autowired
    private UserService userService;
	@Autowired
    private OrderService orderService;
	@Autowired
    private CardService cardService;
	
	@RequestMapping(value = "searchPath.htm", method=RequestMethod.GET)
	@ResponseBody
	public String SearchPath(String from, String to){
		Station fs = stationService.getStation(from);
		Station ts = stationService.getStation(to);

		Set<Vehicle> vfs = fs.getVehicles();
		Set<Vehicle> vts = ts.getVehicles();
		
		for(Vehicle vf : vfs){
			for(Vehicle vt : vts){
				if(vf.equals(vt)){
					return "1:"+vf.toString();
				}
			}
		}
		
		for(Vehicle vf : vfs){
			for(Vehicle vt : vts){
				System.out.println(vf.getVehicleName()+vt.getVehicleName());
				String result = FindExchange(vf,vt);
				System.out.println(result);
				if(!result.equals("")){
					return result;
				}
			}
		}
		
		
		
		return "taxi";
	}
	
	public String FindExchange(Vehicle from, Vehicle to){
		Set<Station> sf = from.getStations();
		Set<Station> st = to.getStations();
		
		for(Station f : sf){
			for(Station t : st){
				if(f.equals(t)){
					return "2:"+from.toString()+":"+f.toString()+":"+to.toString();
				}
			}
		}
		
		for(Station f : sf){
			for(Station t : st){
				String result = FindExchangeTwo(f,t);
				if(!result.equals("")){
					return "3:"+from.toString()+":"+result+":"+to.toString();
				}
				
			}
		}
		return "";
	}
	
	public String FindExchangeTwo(Station from, Station to){
		Set<Vehicle> vf = from.getVehicles();
		Set<Vehicle> vt = to.getVehicles();
		System.out.println(vf.size()+" "+vt.size());
		for(Vehicle f : vf){
			for(Vehicle t : vt){
				if(f.equals(t)){
					return from.toString()+":"+f.toString()+":"+to.toString();
				}
			}
		}
		return "";
	}
	
	@RequestMapping(value = "getstatus.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        String username = hsr.getParameter("user");
		ModelAndView mv = new ModelAndView();
		User user = userService.getUserByName(username);
		List<Object> ss = orderService.getAllByUser(user.getUserId());
        List<Order> orders = new ArrayList<>();
        System.out.println(ss.size());
        for(int i = 0 ; i < ss.size(); i ++){
        	Object [] obj = (Object [])ss.get(i);
        	Order d = (Order)obj[0];
        	orders.add(d);
        }
        mv.addObject("orderList", orders);
		System.out.println(username);
		mv.addObject("user", userService.getUserByName(username));
		mv.setViewName("userstatus");
		return mv;
	}
	
	@RequestMapping(value = "checkcard.htm", method=RequestMethod.GET)
	@ResponseBody
	public String CheckCard(String cardid, String user){
		int id = Integer.parseInt(cardid);
		System.out.println(user);
		User userob = userService.getUserByName(user);
		for(Card card :userob.getCards()){
			if(card.getCardId().equals(id)){
				return "false";
			}
		}
		
		
		Collection<Card> orders = cardService.listCard(id);
		
		if(orders.isEmpty()){
			return "empty";
		}

		return "true";
	}
	
	@RequestMapping(value="confirmorder.htm", method=RequestMethod.POST)
	public ModelAndView ConfirmOrder(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String username = hsr.getParameter("user");
			int orderid = Integer.parseInt(hsr.getParameter("orderid"));
			Order order = orderService.getOrderbyID(orderid);
			String cost = order.getOrderPayment();
			order.setOrderStatus("Confirm");
			orderService.updateOrder(order);
			User user = userService.getUserByName(username);
			List<Object> ss = orderService.getAllByUser(user.getUserId());
	        List<Order> orders = new ArrayList<>();
	        System.out.println(ss.size());
	        for(int i = 0 ; i < ss.size(); i ++){
	        	Object [] obj = (Object [])ss.get(i);
	        	Order d = (Order)obj[0];
	        	orders.add(d);
	        }
	        String balance = user.getUserBalance();
	        if(Float.parseFloat(balance)-Float.parseFloat(cost)<0){
	        	List<Object> ss1 = cardService.SearchCard(user.getUserName());
	            List<Card> cards = new ArrayList<>();
	            System.out.println(ss1.size());
	            for(int i = 0 ; i < ss1.size(); i ++){
	            	Object [] obj = (Object [])ss1.get(i);
	            	Card d = (Card)obj[0];
	            	cards.add(d);
	            }
	    		System.out.println(cards.size());
	    		mv.addObject("cards", cards);
        		mv.addObject("user", user);
	        	mv.setViewName("havearest");
	        }
	        else{
	        	user.setUserBalance(String.valueOf(Float.parseFloat(balance)-Float.parseFloat(cost)));
		        userService.updateUser(user);
	        	mv.addObject("orderList", orders);
				mv.addObject("user", userService.getUserByName(username));
				mv.setViewName("userstatus");
	        }
	        
		
		return mv;
	}
	
	
	@RequestMapping(value = "statuscheck.htm", method=RequestMethod.GET)
	@ResponseBody
	public String CheckStatus(String orderid){
		int id = Integer.parseInt(orderid);
		Collection<Order> orders = orderService.SearchOrder(id);
		
		if(orders.isEmpty()){
			return "empty";
		}
		else{
			Order order = orderService.getOrderbyID(id);
			System.out.println(order.getOrderStatus());
			if(order.getOrderStatus().equals("Accepted")){
				return "true";
			}
			else{
				return "false";
			}
		}
	}
	
	@RequestMapping(value = "getPath.htm", method=RequestMethod.POST)
	public ModelAndView GetPath(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String from = hsr.getParameter("from");
        String to = hsr.getParameter("to");
        String username = hsr.getParameter("user");
        User user = userService.getUserByName(username);
        Order order = new Order();
        order.setOrderPayment("User Balance");
        order.setOrderStatus("Waiting");
        order.setUser(user);
        orderService.addOrder(order);
        mv.addObject("user", user);
        List<Object> ss = orderService.getAllByUser(user.getUserId());
        List<Order> orders = new ArrayList<>();
        System.out.println(ss.size());
        for(int i = 0 ; i < ss.size(); i ++){
        	Object [] obj = (Object [])ss.get(i);
        	Order d = (Order)obj[0];
        	orders.add(d);
        }
        mv.addObject("orderList", orders);
        
   
        
        
        
		mv.setViewName("userstatus");
		return mv;
	}
	
	@RequestMapping(value = "addexpire.htm", method=RequestMethod.POST)
	public ModelAndView AddExpire(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		int cardid = Integer.parseInt(hsr.getParameter("cardid"));
        String op = hsr.getParameter("operations");
        Card card = cardService.getCard(cardid);
        String username = hsr.getParameter("balance");
        User user = userService.getUserByName(username);
        float balance = Float.parseFloat(user.getUserBalance());
        if(op.equals("one")){
        	if(balance>7){
        		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
                Date date = null;
                try{
                    date = sdf.parse(card.getCardExpire());
                }
                catch (ParseException e){
                    e.printStackTrace(); 
                }
                Calendar cl = Calendar.getInstance();  
                cl.setTime(date);
                cl.add(Calendar.DATE, +1);
                date = cl.getTime();
                card.setCardExpire((sdf.format(date)));
                user.setUserBalance(String.valueOf(Float.parseFloat(user.getUserBalance())-7));
                userService.updateUser(user);
                cardService.updateCard(card);
                List<Object> ss = cardService.SearchCard(user.getUserName());
                List<Card> cards = new ArrayList<>();
                System.out.println(ss.size());
                for(int i = 0 ; i < ss.size(); i ++){
                	Object [] obj = (Object [])ss.get(i);
                	Card d = (Card)obj[0];
                	cards.add(d);
                }
        		System.out.println(cards.size());
        		mv.addObject("cards", cards);
        		mv.addObject("user", user);
                mv.setViewName("havearest");
        	}
        	else{
        		mv.addObject("user", user);
        		mv.setViewName("addbalance");
        	}
        }
        else if(op.equals("seven")){
        	if(balance>19){
        		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
                Date date = null;
                try{
                    date = sdf.parse(card.getCardExpire());
                }
                catch (ParseException e){
                    e.printStackTrace(); 
                }
                Calendar cl = Calendar.getInstance();  
                cl.setTime(date);
                cl.add(Calendar.DATE, +7);
                date = cl.getTime();
                card.setCardExpire((sdf.format(date)));
                user.setUserBalance(String.valueOf(Float.parseFloat(user.getUserBalance())-19));
                userService.updateUser(user);
                cardService.updateCard(card);
                List<Object> ss = cardService.SearchCard(user.getUserName());
                List<Card> cards = new ArrayList<>();
                System.out.println(ss.size());
                for(int i = 0 ; i < ss.size(); i ++){
                	Object [] obj = (Object [])ss.get(i);
                	Card d = (Card)obj[0];
                	cards.add(d);
                }
        		System.out.println(cards.size());
        		mv.addObject("cards", cards);
        		mv.addObject("user", user);
                mv.setViewName("havearest");
        	}
        	else{
        		mv.addObject("user", user);
        		mv.setViewName("addbalance");
        	}
        }
        else{
        	if(balance>75){
        		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
                Date date = null;
                try{
                    date = sdf.parse(card.getCardExpire());
                }
                catch (ParseException e){
                    e.printStackTrace(); 
                }
                Calendar cl = Calendar.getInstance();  
                cl.setTime(date);
                cl.add(Calendar.MONTH, +1);
                date = cl.getTime();
                card.setCardExpire((sdf.format(date)));
                user.setUserBalance(String.valueOf(Float.parseFloat(user.getUserBalance())-75));
                userService.updateUser(user);
                cardService.updateCard(card);
                List<Object> ss = cardService.SearchCard(user.getUserName());
                List<Card> cards = new ArrayList<>();
                System.out.println(ss.size());
                for(int i = 0 ; i < ss.size(); i ++){
                	Object [] obj = (Object [])ss.get(i);
                	Card d = (Card)obj[0];
                	cards.add(d);
                }
        		System.out.println(cards.size());
        		mv.addObject("cards", cards);
        		mv.addObject("user", user);
                mv.setViewName("havearest");
        	}
        	else{
        		mv.addObject("user", user);
        		mv.setViewName("addbalance");
        	}
        }
        
		return mv;
	}

	@RequestMapping(value = "addbalance.htm", method=RequestMethod.GET)
	public ModelAndView AddBalance(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String username = hsr.getParameter("user");
		User user = userService.getUserByName(username);
		mv.addObject("user", user);
		mv.setViewName("addbalance");
		
		return mv;
	}
	
	@RequestMapping(value = "confirmbalance.htm", method=RequestMethod.POST)
	public ModelAndView ConfirmBalance(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String username = hsr.getParameter("user");
		float balance = Float.parseFloat(hsr.getParameter("ab"));
		System.out.println(username);
		User user = userService.getUserByName(username);
		user.setUserBalance(String.valueOf(Float.parseFloat(user.getUserBalance())+balance));
		userService.updateUser(user);
		mv.addObject("user", user);
		mv.setViewName("addbalance");
		
		return mv;
	}
	
	@RequestMapping(value = "addcard.htm", method=RequestMethod.GET)
	public ModelAndView AddCard(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String username = hsr.getParameter("user");
		User user = userService.getUserByName(username);
		Card card = new Card();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
        Date date = new Date();
        String datestr = sdf.format(date);
        card.setCardExpire(datestr);
        card.setUser(user);
        user.getCards().add(card);
        cardService.addCard(card);
		userService.updateUser(user);
		List<Object> ss = cardService.SearchCard(user.getUserName());
        List<Card> cards = new ArrayList<>();
        System.out.println(ss.size());
        for(int i = 0 ; i < ss.size(); i ++){
        	Object [] obj = (Object [])ss.get(i);
        	Card d = (Card)obj[0];
        	cards.add(d);
        }
		System.out.println(cards.size());
		mv.addObject("cards", cards);
		mv.addObject("user", user);
		mv.setViewName("havearest");
		
		return mv;
	}
	
	@RequestMapping(value = "checkout.htm", method=RequestMethod.POST)
	public ModelAndView CheckOut(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		float price = Float.parseFloat(hsr.getParameter("price"));
        String pay = hsr.getParameter("pay");
        String username = hsr.getParameter("username");
        User user = userService.getUserByName(username);
        String[] str = pay.split(" ");
        if(str.length>1){
        	 Calendar cl = Calendar.getInstance();
             Date date = new Date();
             Date d = null;
             cl.setTime(date);
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
             try{
                 d = sdf.parse(str[2]);
             }
             catch(ParseException e){
                 e.printStackTrace();
             }
             if (cl.before(d)){
            	 List<Object> ss = cardService.SearchCard(user.getUserName());
                 List<Card> cards = new ArrayList<>();
                 System.out.println(ss.size());
                 for(int i = 0 ; i < ss.size(); i ++){
                 	Object [] obj = (Object [])ss.get(i);
                 	Card card = (Card)obj[0];
                 	cards.add(card);
                 }
         		System.out.println(cards.size());
         		mv.addObject("cards", cards);
            	 mv.addObject("user", user);
               	mv.setViewName("havearest");
             }
             else{
            	 mv.addObject("user", user);
              	mv.setViewName("addsuccess");
             }
        }
        else{
        	float balance = Float.parseFloat(pay); 
        	user.setUserBalance(String.valueOf(balance-price));
        	userService.updateUser(user);
        	mv.addObject("user", user);
        	mv.setViewName("addsuccess");
        }
        
        System.out.println(price+pay+username);

		return mv;
	}
	
	
	@RequestMapping(value = "havearest.htm", method=RequestMethod.GET)
	public ModelAndView HaveARest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String username = hsr.getParameter("user");
		System.out.println(username);
		User user = userService.getUserByName(username);
		List<Object> ss = cardService.SearchCard(user.getUserName());
        List<Card> cards = new ArrayList<>();
        System.out.println(ss.size());
        for(int i = 0 ; i < ss.size(); i ++){
        	Object [] obj = (Object [])ss.get(i);
        	Card d = (Card)obj[0];
        	cards.add(d);
        }
		System.out.println(cards.size());
		mv.addObject("cards", cards);
		mv.addObject("user", user);
		mv.setViewName("havearest");
		
		return mv;
	}
}


