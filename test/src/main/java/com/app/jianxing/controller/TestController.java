package com.app.jianxing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {

	@RequestMapping(value="test.htm", method=RequestMethod.GET)
	public ModelAndView ajsdf(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("test");
		return mv;
	}
}
