package com.app.jianxing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.jianxing.model.Station;

@Controller
@RequestMapping("/createstation.htm")
public class AddRedirect {
	@RequestMapping(method = RequestMethod.GET)
	public String AddRedirect(@ModelAttribute("station") Station station, Model model){
		return "createstation";
	}
}
