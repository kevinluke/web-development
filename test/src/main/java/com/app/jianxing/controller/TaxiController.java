package com.app.jianxing.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.app.jianxing.model.Order;
import com.app.jianxing.model.Taxi;
import com.app.jianxing.service.OrderService;
import com.app.jianxing.service.TaxiService;

@Controller
public class TaxiController {

	
	@Autowired
    private OrderService orderService;
	@Autowired
    private TaxiService taxiService;
	
	@RequestMapping(value = "checkorder.htm", method=RequestMethod.GET)
	@ResponseBody
	public String CheckOrder(String orderid){
		int id = Integer.parseInt(orderid);
		Collection<Order> orders = orderService.SearchOrder(id);
		
		if(orders.isEmpty()){
			return "empty";
		}
		else{
			Order order = orderService.getOrderbyID(id);
			System.out.println(order.getTaxi().getTaxiname());
			if(order.getTaxi()==null){
				return "true";
			}
			else{
				return "false";
			}
		}
	}
	
	@RequestMapping(value = "checkstatus.htm", method=RequestMethod.GET)
	@ResponseBody
	public String CheckStatus(String orderid){
		int id = Integer.parseInt(orderid);
		Collection<Order> orders = orderService.SearchOrder(id);
		
		if(orders.isEmpty()){
			return "empty";
		}
		else{
			Order order = orderService.getOrderbyID(id);
			System.out.println(order.getTaxi().getTaxiname());
			if(order.getOrderStatus().equals("Confirm")){
				return "true";
			}
			else{
				return "false";
			}
		}
	}
	
	@RequestMapping(value="acceptorder.htm", method=RequestMethod.POST)
	public ModelAndView AcceptOrder(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String taxiname = hsr.getParameter("taxi");
		Taxi taxi = taxiService.getTaxiByName(taxiname);
		String method = hsr.getParameter("method");
		if(method.equals("accept")){
			System.out.println(taxiname);
			int orderid = Integer.parseInt(hsr.getParameter("orderid"));
			String cost = hsr.getParameter("cost");
			Order order = orderService.getOrderbyID(orderid);
			System.out.println(cost);
			order.setTaxi(taxi);
			order.setOrderStatus("Accepted");
			order.setOrderPayment(cost);
			orderService.updateOrder(order);
			mv.addObject("orderList", orderService.getAll());
            mv.addObject("taxi", taxi);
			mv.setViewName("driverpage");
		}
		else{
			int orderid = Integer.parseInt(hsr.getParameter("orderid"));
			Order order = orderService.getOrderbyID(orderid);
			order.setOrderStatus("Completed LOL");
			taxi.setIncome(taxi.getIncome()+Float.parseFloat(order.getOrderPayment()));
			taxiService.updateTaxi(taxi);
			orderService.updateOrder(order);
			mv.addObject("orderList", orderService.getAll());
            mv.addObject("taxi", taxi);
			mv.setViewName("driverpage");
		}
		return mv;
	}
	
	@RequestMapping(value="myorder.htm", method=RequestMethod.GET)
	public ModelAndView MyOrder(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String taxiname = hsr.getParameter("taxi");
		System.out.println(taxiname);
		Taxi taxi = taxiService.getTaxiByName(taxiname);
		List<Object> ss = orderService.getAllByTaxi(taxi.getTaxiid());
        List<Order> orders = new ArrayList<>();
        System.out.println(ss.size());
        for(int i = 0 ; i < ss.size(); i ++){
        	Object [] obj = (Object [])ss.get(i);
        	Order d = (Order)obj[0];
        	orders.add(d);
        }
        mv.addObject("orderList", orders);
        mv.addObject("taxi", taxi);
		mv.setViewName("myorder");
		return mv;
	}
	
	@RequestMapping(value="income.htm", method=RequestMethod.GET)
	public ModelAndView Income(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String taxiname = hsr.getParameter("taxi");
		
		System.out.println(taxiname);
		Taxi taxi = taxiService.getTaxiByName(taxiname);
		List<Object> ss = orderService.getAllByTaxi(taxi.getTaxiid());
        List<Order> orders = new ArrayList<>();
        System.out.println(ss.size());
        for(int i = 0 ; i < ss.size(); i ++){
        	Object [] obj = (Object [])ss.get(i);
        	Order d = (Order)obj[0];
        	System.out.println(d.getOrderStatus());
        	if(d.getOrderStatus().equals("Completed LOL")){
        		orders.add(d);
        	}
        	
        }
        System.out.println(orders.size());
        mv.addObject("orderList", orders);
        mv.addObject("taxi", taxi);
		mv.setViewName("income");
		return mv;
	}
}
