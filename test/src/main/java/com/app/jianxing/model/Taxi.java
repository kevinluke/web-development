package com.app.jianxing.model;
// Generated 2016-4-24 10:20:07 by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Taxi generated by hbm2java
 */
@Entity
@Table(name = "taxi", catalog = "myfinal")
public class Taxi implements java.io.Serializable {

	private Integer taxiid;
	private String taxiname;
	private String taxipwd;
	private float income;
	private Set<Order> orders = new HashSet<Order>(0);

	public Taxi() {
	}

	public Taxi(String taxiname, String taxipwd, float income) {
		this.taxiname = taxiname;
		this.taxipwd = taxipwd;
		this.income = income;
	}

	public Taxi(String taxiname, String taxipwd, float income, Set<Order> orders) {
		this.taxiname = taxiname;
		this.taxipwd = taxipwd;
		this.income = income;
		this.orders = orders;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "taxiid", unique = true, nullable = false)
	public Integer getTaxiid() {
		return this.taxiid;
	}

	public void setTaxiid(Integer taxiid) {
		this.taxiid = taxiid;
	}

	@Column(name = "taxiname", nullable = false, length = 45)
	public String getTaxiname() {
		return this.taxiname;
	}

	public void setTaxiname(String taxiname) {
		this.taxiname = taxiname;
	}

	@Column(name = "taxipwd", nullable = false, length = 45)
	public String getTaxipwd() {
		return this.taxipwd;
	}

	public void setTaxipwd(String taxipwd) {
		this.taxipwd = taxipwd;
	}

	@Column(name = "income", nullable = false, precision = 12, scale = 0)
	public float getIncome() {
		return this.income;
	}

	public void setIncome(float income) {
		this.income = income;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "taxi")
	public Set<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}

	@Override
	public String toString(){
		return this.taxiname;
	}
}
