package strong.app;


import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.app.jianxing.exception.AdException;
import com.app.jianxing.model.User;


/**
 *
 * @author lu
 */
public class UserDAO extends DAO {

    public UserDAO() {
    }

    public User get(String username, String userpwd)
            throws AdException {
        try {
            begin();
            Query q = getSession().createQuery("from User where username = :username and userpwd = :userpwd");
            q.setString("username", username);
            q.setString("userpwd", userpwd);
            User user = (User) q.uniqueResult();
            commit();
            return user;
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not get user " + username, e);
        }
    }

    public User create(String username, String password, String firstName, String lastName)
            throws AdException {
        try {
            begin();
            System.out.println("inside DAO");

            User user=new User();
            
            user.setUserFname(firstName);
            user.setUserLname(lastName);
            
            user.setUserName(username);
            user.setUserPwd(password);
            
            getSession().save(user);
            
            commit();
            return user;
        } catch (HibernateException e) {
            rollback();
            //throw new AdException("Could not create user " + username, e);
            throw new AdException("Exception while creating user: " + e.getMessage());
        }
    }

    public void delete(User user)
            throws AdException {
        try {
            begin();
            getSession().delete(user);
            commit();
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not delete user " + user.getUserName(), e);
        }
    }
}
