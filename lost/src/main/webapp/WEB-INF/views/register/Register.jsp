<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Create Student</title>
<link rel="stylesheet" href="<c:url value="/resources/blueprint/screen.css" />" type="text/css" media="screen, projection">
<link rel="stylesheet" href="<c:url value="/resources/blueprint/print.css" />" type="text/css" media="print">
<!--[if lt IE 8]>
	<link rel="stylesheet" href="<c:url value="/resources/blueprint/ie.css" />" type="text/css" media="screen, projection">
<![endif]-->
<script src="<c:url value="/resources/jquery-2.1.3/jquery.js" />"></script>
<script src="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.js" />"></script>
<link rel="stylesheet" href="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.css" />" type="text/css">

<body>
<div class="container">
	<div id="messagesContainer">
	</div>
	<h1>
		Create Register
	</h1>
	<div class="span-12 last">	
		<form:form modelAttribute="register" action="addregister" method="post" enctype="multipart/form-data">
		  	<fieldset>		
				<legend>Register Fields</legend>
				<p>
					<form:label	for="registerId" path="registerId" cssErrorClass="error">registerId:</form:label><br/>
					<form:input path="registerId" /> <form:errors path="registerId" />			
				</p>
				
				<p>
					<form:label	for="registerName" path="registerName" cssErrorClass="error">registerName:</form:label><br/>
					<form:input path="registerName" /> <form:errors path="registerName" />			
				</p>
				<p>	
					<form:label for="registerPassword" path="registerPassword" cssErrorClass="error">registerPassword:</form:label><br/>
					<form:input path="registerPassword" /> <form:errors path="registerPassword" />
				</p>
				<p>	
					<form:label for="registerPhone" path="registerPhone" cssErrorClass="error">RegisterPhone:</form:label><br/>
					<form:input path="registerPhone" /> <form:errors path="registerPhone" />
				</p>
				<p>	
					<form:label for="registerIpadress" path="registerIpadress" cssErrorClass="error">registerIpadress:</form:label><br/>
					<form:input path="registerIpadress" /> <form:errors path="registerIpadress" />
				</p>
				<p>	
					<form:label for="registerEmail" path="registerEmail" cssErrorClass="error">registerEmail:</form:label><br/>
					<form:input path="registerEmail" /> <form:errors path="registerEmail" />
				</p>
				
					<input type="submit" />
				</p>
			</fieldset>
		</form:form>
		
	<div>
			<a href="registers">Register List</a> <a href="findregister">Search Register</a>
		</div>
		<h2>Registers</h2>
		
	<c:if test="${!empty registerList}">
			<table>
				<c:forEach items="${registerList}" var="register">
					<tr>
					
						<td>${register.registerId}</td>
						<td>${register.registerName}</td>
						<td>${register.registerPhone}</td>
						<td>${register.registerEmail}</td>
				
						<td><a href="students">for tests</a></td>
						
					</tr>
				</c:forEach>
			</table>
		</c:if>	
		
		
		
	</div>	
</div>
</body>
</html>