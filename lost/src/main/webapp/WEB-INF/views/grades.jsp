<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Create Assignment Grade</title>
<link rel="stylesheet" href="<c:url value="/resources/blueprint/screen.css" />" type="text/css" media="screen, projection">
<link rel="stylesheet" href="<c:url value="/resources/blueprint/print.css" />" type="text/css" media="print">
<!--[if lt IE 8]>
	<link rel="stylesheet" href="<c:url value="/resources/blueprint/ie.css" />" type="text/css" media="screen, projection">
<![endif]-->
<script src="<c:url value="/resources/jquery-2.1.3/jquery.js" />"></script>
<script src="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.js" />"></script>
<link rel="stylesheet" href="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.css" />" type="text/css">
<script type="text/javascript">
	// http://api.jquery.com/
	// http://jqueryui.com/
	$(document).ready(function() {
		$("select").selectmenu();
		$("#pointsScored").spinner();
		$("input[type=submit], a, button").button();
	});
</script>
<body>
<div class="container">
	<h1>
		Create Assignment Grade
	</h1>
	<div class="span-12 last">	
		<form:form modelAttribute="assignmentGrade" action="grade" method="post">
		  	<fieldset>		
				<legend>Assignment Grade Fields</legend>
				<p>
					Student Name: ${student.firstName}&nbsp;${assignmentGrade.student.lastName}
				</p>
				<p>
					<form:label	for="name" path="assignment.name">Assignment Name:</form:label><br/>
					<form:select items="${assignmentList}"	itemLabel="name" itemValue="id" path="assignment.id" />
				</p>
				<p>
					<form:label for="pointsScored" path="pointsScored" cssErrorClass="error">Points Scored:</form:label><br/>
					<form:input path="pointsScored" id="pointsScored" /> <form:errors path="pointsScored" />
				</p>
				<p>	
					<input type="submit" />
				</p>
			</fieldset>
		</form:form>
		
		<ul>
			<a href="../../students">Student List</a> <a href="../../assignments">Assignment List</a>
		</ul>
		<h2>Assignment Grades for ${assignmentGrade.student.firstName}&nbsp;${assignmentGrade.student.lastName}</h2>
		<c:if test="${!empty assignmentGradeList}">
			<ul style="list-style-type:none">
				<c:forEach items="${assignmentGradeList}" var="assignmentGrade">
					<li class="box">
						<div>Name:</div>
						<div>${assignmentGrade.assignment.name}</div>
						<div>Points Scored:</div> 
						<div>${assignmentGrade.pointsScored}</div>
					</li>
				</c:forEach>
			</ul>
		</c:if>
	</div>	
</div>
</body>
</html>