<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Create Assignment</title>
<link rel="stylesheet" href="<c:url value="/resources/blueprint/screen.css" />" type="text/css" media="screen, projection">
<link rel="stylesheet" href="<c:url value="/resources/blueprint/print.css" />" type="text/css" media="print">
<!--[if lt IE 8]>
	<link rel="stylesheet" href="<c:url value="/resources/blueprint/ie.css" />" type="text/css" media="screen, projection">
<![endif]-->
<script src="<c:url value="/resources/jquery-2.1.3/jquery.js" />"></script>
<script src="<c:url value="/resources/ckeditor/ckeditor.js" />"></script>
<script src="<c:url value="/resources/ckeditor/adapters/jquery.js" />"></script>
<script src="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.js" />"></script>
<link rel="stylesheet" href="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.css" />" type="text/css">
<script type="text/javascript">
	// http://api.jquery.com/
	// http://jqueryui.com/
	$(document).ready(function() {
		$("textarea").ckeditor();
		$("#datePicker").datepicker();
		$("#pointsPossible").spinner();
		$("input[type=submit], a, button").button();
	});
</script>
<body>
<div class="container">
	<h1>
		Create Assignment
	</h1>
	<div class="span-24 last">	
		<form:form modelAttribute="assignment" action="assignments" method="post">
		  	<fieldset>		
				<legend>Assignment Fields</legend>
				<p>
					<form:label	for="name" path="name" cssErrorClass="error">Name:</form:label><br/>
					<form:input path="name" /> <form:errors path="name" />			
				</p>
				<p>	
					<form:label for="instructions" path="instructions" cssErrorClass="error">Instructions:</form:label><br/>
					<form:textarea path="instructions" /> <form:errors path="instructions" />
				</p>
				<p>
					<form:label for="dueDate" path="dueDate" cssErrorClass="error">Due Date:</form:label><br/>
					<form:input path="dueDate" id="datePicker" /> <form:errors path="dueDate" />
				</p>
				<p>
					<form:label for="pointsPossible" path="pointsPossible" cssErrorClass="error">Points Possible:</form:label><br/>
					<form:input path="pointsPossible" id="pointsPossible" /> <form:errors path="pointsPossible" />
				</p>
				<p>	
					<input type="submit" />
				</p>
			</fieldset>
		</form:form>
		
		<ul>
			<a href="students">Student List</a> <a href="assignments">Assignment List</a>
		</ul>
		<h2>Assignments</h2>
		<c:if test="${!empty assignmentList}">
			<ul style="list-style-type:none">
				<c:forEach items="${assignmentList}" var="assignment">
					<li class="box">
						<div>Name:</div>
						<div>${assignment.name}</div>
						<div>Instructions:</div> 
						<div>${assignment.instructions}</div>
						<div>Due Date:</div> 
						<div>${assignment.dueDate}</div>
						<div>Points Possible:</div> 
						<div>${assignment.pointsPossible}</div>
					</li>
				</c:forEach>
			</ul>
		</c:if>
	</div>	
</div>
</body>
</html>