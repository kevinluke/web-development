package com.app.High;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.hibnate.Student;
import com.app.Service.*;

@Controller
@RequestMapping("/students")
public class StudentController {
	@Autowired
	private StudentService studentService;
	//private ItemService itemService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String addStudent(@Valid Student student, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("studentList", studentService.listStudents());
			return "students";
		}
		studentService.addStudent(student);//加入对象，加入东西
		return "redirect:/students";
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String listStudents(Model model) {
		model.addAttribute(new Student());//拿东西
		model.addAttribute("studentList", studentService.listStudents());
		
		return "students";
	}
	
	@RequestMapping(value="/{studentId}/photo", method=RequestMethod.GET)
	public ResponseEntity<byte[]> viewPhoto(@PathVariable Long studentId, HttpServletResponse response) throws IOException {
		Student student = studentService.getStudent(studentId);
		byte[] photoBytes = student.getPhotoBytes();
		if (photoBytes != null) {
			HttpHeaders headers = new HttpHeaders();
			if (student.getPhotoContentType().equals("image/jpeg")){
				headers.setContentType(MediaType.IMAGE_JPEG);
			}
			else {
				headers.setContentType(MediaType.IMAGE_PNG);
			}
			
			return new ResponseEntity<byte[]>(photoBytes, headers, HttpStatus.OK);
		}
		return null;
		
	}
	
	@RequestMapping(value="/{studentId}", method=RequestMethod.DELETE)
	public @ResponseBody String deleteStudent(@PathVariable Long studentId) {
		studentService.deleteStudent(studentId);
		
		return "";
	}
}
