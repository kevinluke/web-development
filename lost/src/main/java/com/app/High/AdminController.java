package com.app.High;



import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.hibnate.Admin;
import com.app.Service.*;

@Controller
@RequestMapping("/Admins")
public class AdminController {
	@Autowired
	private AdminService AdminService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String addAdmin(@Valid Admin Admin, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("AdminList", AdminService.listAdmins());
			return "Admins";
		}
		AdminService.addAdmin(Admin);
		return "redirect:/Admins";
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String listAdmins(Model model) {
		model.addAttribute(new Admin());
		model.addAttribute("AdminList", AdminService.listAdmins());
		return "Admins";
	}
	/*
	@RequestMapping(value="/{AdminId}/photo", method=RequestMethod.GET)
	public @ResponseBody String viewPhoto(@PathVariable Long AdminId, HttpServletResponse response) throws IOException {
		Admin Admin = AdminService.getAdmin(AdminId);
		byte[] photoBytes = Admin.getPhotoBytes();
		if (photoBytes != null) {
			int photoLength = photoBytes.length;
			try (ServletOutputStream sos = response.getOutputStream()) {
				response.setContentType(Admin.getPhotoContentType());
				response.setContentLength(photoLength);
				response.setHeader("Content-Disposition", "inline; filename=\"" + Admin.getPhotoFilename() + "\"");
				
				sos.write(photoBytes);
				sos.flush();
			}
		}
		return "";
	}
	*/
	@RequestMapping(value="/{AdminId}", method=RequestMethod.DELETE)
	public @ResponseBody String deleteAdmin(@PathVariable Long AdminId) {
		AdminService.deleteAdmin(AdminId);
		
		return "";
	}
}