package com.app.High;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.hibnate.Register;

import com.app.Service.*;

@Controller

public class RegisterController {
	@Autowired
	private RegisterService registerService;
	
	@RequestMapping(value="/addregister",method=RequestMethod.POST)
	public String addRegister(@Valid Register register, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("registerList", registerService.listRegisters());
			return "register/Register";
		}
		registerService.addRegister(register);//加入对象，加入东西
		return "redirect:/registers";
	}
	
	
	
	@RequestMapping(value = "/registers",method=RequestMethod.GET)
	public String listRegisters(Model model) {
		model.addAttribute(new Register());//拿东西
		model.addAttribute("registerList", registerService.listRegisters());
		
		
		return "register/Register";
	}
	
	@RequestMapping(value = "/findregister", method = RequestMethod.GET)
    public String Findregister(Model model) {
	
	
	return "register/searchRegister";
}
	
	
	
	@RequestMapping(value="/searchRegister", method = RequestMethod.POST)
	public String searchRegister(String registerId, Model model)
	{  
		
	//	model.addAttribute("Registers", registerService.SearchRegisters(registerId));
		
		
		System.out.println("PPPPPPPP");
		int ID=Integer.parseInt(registerId);
		System.out.println(registerId+"  correct");
		
		Register register = registerService.getRegister(ID);
		System.out.println("check");
		System.out.println(register.getRegisterEmail());
		
		System.out.println(register.getRegisterName()+"  "+register.getRegisterEmail());
	    ArrayList<Register> list =new ArrayList<Register>();
		list.add(register);
		
		model.addAttribute("Registers", list);
		model.addAttribute("Register",new Register());
		
		return  "register/searchResult";
		
	}
	
	@RequestMapping(value = "/findregister0", method = RequestMethod.GET)
    public String Findregister0(Model model) {
	
	
	return "register/searchRegister0";
}
	
	@RequestMapping(value="/searchRegister0", method = RequestMethod.POST)
	public String searchRegister0(String name, Model model)
	{  
		
	  
		System.out.print(name);
		model.addAttribute("Registers", registerService.SearchRegisters(name));
		model.addAttribute("Register",new Register());
		
		return  "register/searchResult0";
		
	}	
	
	
	/*
	@RequestMapping(value="/{registerId}/photo", method=RequestMethod.GET)
	public @ResponseBody String viewPhoto(@PathVariable Long registerId, HttpServletResponse response) throws IOException {
		Register register = registerService.getRegister(registerId);
		byte[] photoBytes = register.getPhotoBytes();
		if (photoBytes != null) {
			int photoLength = photoBytes.length;
			try (ServletOutputStream sos = response.getOutputStream()) {
				response.setContentType(register.getPhotoContentType());
				response.setContentLength(photoLength);
				response.setHeader("Content-Disposition", "inline; filename=\"" + register.getPhotoFilename() + "\"");
				
				sos.write(photoBytes);
				sos.flush();
			}
		}
		return "";
	}
	
	
	*/
	/*
	
	@RequestMapping(value="/{registerId}", method=RequestMethod.DELETE)
	public @ResponseBody String deleteRegister(@PathVariable Long registerId) {
		registerService.deleteRegister(registerId);
		
		return "";
	}
	*/
}
