package com.app.Service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.StudentDAO;
import com.app.hibnate.Student;

@Service
public class StudentServiceImpl implements StudentService {
	
	@Autowired
	@Qualifier("studentHibernateDAOImpl")
	private StudentDAO studentDAO;
	
	@Override
	@Transactional
	public void addStudent(Student student) {
		studentDAO.addStudent(student);
	}

	@Override
	@Transactional
	public Collection<Student> listStudents() {
		return studentDAO.listStudents();
	}

	@Override
	@Transactional
	public Student getStudent(Long id) {
		return studentDAO.getStudent(id);
	}

	@Override
	@Transactional
	public void deleteStudent(Long id) {
		studentDAO.deleteStudent(id);
	}
}
