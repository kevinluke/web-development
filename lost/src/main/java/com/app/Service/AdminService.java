package com.app.Service;

import java.util.Collection;

import com.app.hibnate.Admin;

public interface AdminService {
	public void addAdmin(Admin admin);
	public Collection<Admin> listAdmins();
	public Admin getAdmin(Long id);
	public void deleteAdmin(Long id);
}
