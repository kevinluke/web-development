package com.app.Service;

import java.util.Collection;

import com.app.hibnate.Register;

public interface RegisterService {
	public void addRegister(Register register);
	public void updateRegister(Register register);
	public Collection<Register> listRegisters();
	public Register getRegister(int registerId);
	public void deleteRegister(int registerId);
	public Collection<Register> SearchRegisters(String Name) ;
}
