package com.app.Service;

import java.util.Collection;

import com.app.hibnate.Student;

public interface StudentService {
	public void addStudent(Student student);
	public Collection<Student> listStudents();
	public Student getStudent(Long id);
	public void deleteStudent(Long id);
}
