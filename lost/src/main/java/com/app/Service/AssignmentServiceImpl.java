package com.app.Service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.AssignmentDAO;
import com.app.hibnate.Assignment;

@Service
public class AssignmentServiceImpl implements AssignmentService {
	
	@Autowired
	private AssignmentDAO assignmentDAO;
	
	@Override
	@Transactional
	public void addAssignment(Assignment assignment) {
		assignmentDAO.addAssignment(assignment);
	}

	@Override
	@Transactional
	public Collection<Assignment> listAssignments() {
		return assignmentDAO.listAssignments();
	}

	@Override
	@Transactional
	public Assignment getAssignment(Long id) {
		return assignmentDAO.getAssignment(id);
	}
}
