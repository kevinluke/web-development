package com.app.Service;

import java.util.Collection;

import com.app.hibnate.Assignment;

public interface AssignmentService {
	public void addAssignment(Assignment assignment);
	public Collection<Assignment> listAssignments();
	public Assignment getAssignment(Long id);
}
