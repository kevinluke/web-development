package com.app.dao;

import java.util.Collection;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.hibnate.Student;

@Repository	
public class StudentHibernateDAOImpl implements StudentDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addStudent(Student student) {
		sessionFactory.getCurrentSession().saveOrUpdate(student);
		sessionFactory.getCurrentSession().flush();
	}

	@Override
	public Collection<Student> listStudents()
	{
		return sessionFactory.getCurrentSession().createQuery("from Student").list();
	}

	@Override
	public Student getStudent(Long id) 
	{
		return (Student) sessionFactory.getCurrentSession().get(Student.class, id);
	}

	@Override
	public void deleteStudent(Long id) {
		Student student = getStudent(id);
		if (student != null) {
			sessionFactory.getCurrentSession().delete(student);
			sessionFactory.getCurrentSession().flush();
		}
	}
}
