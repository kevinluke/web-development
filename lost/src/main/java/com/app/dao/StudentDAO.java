package com.app.dao;

import java.util.Collection;

import com.app.hibnate.Student;

public interface StudentDAO {
	public void addStudent(Student student);
	public Collection<Student> listStudents();
	public Student getStudent(Long id);
	public void deleteStudent(Long id);
}
