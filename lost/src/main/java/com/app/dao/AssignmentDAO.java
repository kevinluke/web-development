package com.app.dao;

import java.util.Collection;

import com.app.hibnate.Assignment;

public interface AssignmentDAO {
	public void addAssignment(Assignment assignment);
	public Collection<Assignment> listAssignments();
	public Assignment getAssignment(Long id);
}
