package com.app.dao;

import java.util.Collection;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.hibnate.Assignment;

@Repository	
public class AssignmentHibernateDAOImpl implements AssignmentDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addAssignment(Assignment assignment) {
		sessionFactory.getCurrentSession().save(assignment);
	}

	@Override
	public Collection<Assignment> listAssignments() {
		return sessionFactory.getCurrentSession().createQuery("from Assignment").list();
	}

	@Override
	public Assignment getAssignment(Long id) {
		return (Assignment) sessionFactory.getCurrentSession().load(Assignment.class, id);
	}
}
