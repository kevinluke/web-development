package com.app.hibnate;
// Generated 2016-3-26 22:29:34 by Hibernate Tools 4.3.1.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Item generated by hbm2java
 */
@Entity
@Table(name = "item", catalog = "mywyc")
public class Item implements java.io.Serializable {

	private int itemId;
	private Category category;
	private Recommendation recommendation;
	private Register register;
	private String itemName;
	private String itemImageName;
	private String itemImageFiletye;
	private byte[] itemData;
	private Integer itemLike;
	private Set<Step> steps = new HashSet<Step>(0);

	public Item() {
	}

	public Item(int itemId) {
		this.itemId = itemId;
	}

	public Item(int itemId, Category category, Recommendation recommendation, Register register, String itemName,
			String itemImageName, String itemImageFiletye, byte[] itemData, Integer itemLike, Set<Step> steps) {
		this.itemId = itemId;
		this.category = category;
		this.recommendation = recommendation;
		this.register = register;
		this.itemName = itemName;
		this.itemImageName = itemImageName;
		this.itemImageFiletye = itemImageFiletye;
		this.itemData = itemData;
		this.itemLike = itemLike;
		this.steps = steps;
	}

	@Id

	@Column(name = "ItemId", unique = true, nullable = false)
	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CategoryId")
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RecommondationId")
	public Recommendation getRecommendation() {
		return this.recommendation;
	}

	public void setRecommendation(Recommendation recommendation) {
		this.recommendation = recommendation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RegisterId")
	public Register getRegister() {
		return this.register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}

	@Column(name = "ItemName", length = 45)
	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@Column(name = "ItemImageName")
	public String getItemImageName() {
		return this.itemImageName;
	}

	public void setItemImageName(String itemImageName) {
		this.itemImageName = itemImageName;
	}

	@Column(name = "ItemImageFiletye")
	public String getItemImageFiletye() {
		return this.itemImageFiletye;
	}

	public void setItemImageFiletye(String itemImageFiletye) {
		this.itemImageFiletye = itemImageFiletye;
	}

	@Column(name = "ItemData")
	public byte[] getItemData() {
		return this.itemData;
	}

	public void setItemData(byte[] itemData) {
		this.itemData = itemData;
	}

	@Column(name = "ItemLike")
	public Integer getItemLike() {
		return this.itemLike;
	}

	public void setItemLike(Integer itemLike) {
		this.itemLike = itemLike;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
	public Set<Step> getSteps() {
		return this.steps;
	}

	public void setSteps(Set<Step> steps) {
		this.steps = steps;
	}

}
