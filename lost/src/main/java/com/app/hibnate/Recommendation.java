package com.app.hibnate;
// Generated 2016-3-26 22:29:34 by Hibernate Tools 4.3.1.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Recommendation generated by hbm2java
 */
@Entity
@Table(name = "recommendation", catalog = "mywyc")
public class Recommendation implements java.io.Serializable {

	private int recommondationId;
	private Admin admin;
	private String recommondationDate;
	private String recommendationcol;
	private Set<Item> items = new HashSet<Item>(0);

	public Recommendation() {
	}

	public Recommendation(int recommondationId) {
		this.recommondationId = recommondationId;
	}

	public Recommendation(int recommondationId, Admin admin, String recommondationDate, String recommendationcol,
			Set<Item> items) {
		this.recommondationId = recommondationId;
		this.admin = admin;
		this.recommondationDate = recommondationDate;
		this.recommendationcol = recommendationcol;
		this.items = items;
	}

	@Id

	@Column(name = "RecommondationId", unique = true, nullable = false)
	public int getRecommondationId() {
		return this.recommondationId;
	}

	public void setRecommondationId(int recommondationId) {
		this.recommondationId = recommondationId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AdminId")
	public Admin getAdmin() {
		return this.admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	@Column(name = "RecommondationDate", length = 50)
	public String getRecommondationDate() {
		return this.recommondationDate;
	}

	public void setRecommondationDate(String recommondationDate) {
		this.recommondationDate = recommondationDate;
	}

	@Column(name = "Recommendationcol", length = 45)
	public String getRecommendationcol() {
		return this.recommendationcol;
	}

	public void setRecommendationcol(String recommendationcol) {
		this.recommendationcol = recommendationcol;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "recommendation")
	public Set<Item> getItems() {
		return this.items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

}
