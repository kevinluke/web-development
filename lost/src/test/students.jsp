<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Create Student</title>
<link rel="stylesheet" href="<c:url value="/resources/blueprint/screen.css" />" type="text/css" media="screen, projection">
<link rel="stylesheet" href="<c:url value="/resources/blueprint/print.css" />" type="text/css" media="print">
<!--[if lt IE 8]>
	<link rel="stylesheet" href="<c:url value="/resources/blueprint/ie.css" />" type="text/css" media="screen, projection">
<![endif]-->
<script src="<c:url value="/resources/jquery-2.1.3/jquery.js" />"></script>
<script src="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.js" />"></script>
<link rel="stylesheet" href="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.css" />" type="text/css">
<script type="text/javascript">
	// http://api.jquery.com/
	// http://jqueryui.com/
	$(document).ready(function() {
		$("input[type=submit], a, button").button();
	});
	function deleteStudent(e) {
		/*     http://www.w3schools.com/ajax/default.asp
		var xmlhttp;
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
		  	xmlhttp=new XMLHttpRequest();
		}
		else { 
			// code for IE6, IE5
		  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				location.reload(true);
		}
		xmlhttp.open("DELETE", "students/" + e.target.id, true);
		xmlhttp.send();
		*/
		$.ajax({		// http://www.w3schools.com/jquery/jquery_ajax_intro.asp
			url: "students/" + e.target.id,
			method: "DELETE",
			success: function(result) {
				$("#messagesContainer").prepend("<div class='success'>Student successfully deleted</div>");
				setTimeout(function() {
					location.reload(true);
				}, 10000);
			}
		});
	}
</script>
<body>
<div class="container">
	<div id="messagesContainer">
	</div>
	<h1>
		Create Student
	</h1>
	<div class="span-12 last">	
		<form:form modelAttribute="student" action="students" method="post" enctype="multipart/form-data">
		  	<fieldset>		
				<legend>Student Fields</legend>
				<p>
					<form:label	for="firstName" path="firstName" cssErrorClass="error">First Name:</form:label><br/>
					<form:input path="firstName" /> <form:errors path="firstName" />			
				</p>
				<p>	
					<form:label for="lastName" path="lastName" cssErrorClass="error">Last Name:</form:label><br/>
					<form:input path="lastName" /> <form:errors path="lastName" />
				</p>
				<p>
					<form:label for="photo" path="photo" cssErrorClass="error">Student Photo:</form:label><br/>
					<form:input path="photo" type="file" /> <form:errors path="photo" />
				<p>	
					<input type="submit" />
				</p>
			</fieldset>
		</form:form>
		
		<div>
			<a href="students">Student List</a> <a href="assignments">Assignment List</a>
		</div>
		<h2>Students</h2>
		<c:if test="${!empty studentList}">
			<table>
				<c:forEach items="${studentList}" var="student">
					<tr>
						<td>
							<img src="students/${student.id}/photo" height="100" width="100" />
						</td>
						<td>${student.firstName}</td>
						<td>${student.lastName}</td>
						<td><a href="#" id="${student.id}" onclick="deleteStudent(event)">Delete</a></td>
						<td><a href="students/${student.id}/grades">View Grades</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</div>	
</div>
</body>
</html>