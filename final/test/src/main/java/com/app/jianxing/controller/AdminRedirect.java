package com.app.jianxing.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/adminpage.htm")
public class AdminRedirect {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(method = RequestMethod.GET)
	public String AdminRedirect(Model model){
		return "adminpage";
	}
}
