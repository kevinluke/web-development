package com.app.jianxing.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.jianxing.model.User;
import com.app.jianxing.service.UserService;

@Controller
public class UserRegisterController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/uregister.htm", method=RequestMethod.POST)
	public String addUser(@Valid User user, BindingResult bindingResult, Model model){
		if (bindingResult.hasErrors()) {
			model.addAttribute("userList", userService.listUser());
			return "uregister";
		}
//		model.addAttribute(new User());
		userService.addUser(user);
		return "index";
	}
	@RequestMapping(value = "/registers.htm",method=RequestMethod.GET)
	public String listRegisters(Model model) {
		model.addAttribute(new User());//�ö���
		
		
		return "uregister";
	}
	
	@RequestMapping(value = "checkUser.do", method=RequestMethod.GET)
	@ResponseBody
	public String checkUser(HttpServletRequest hsr, HttpServletResponse hsr1){
		String name = hsr.getParameter("username");
		Collection<User> result = userService.SearchUser(name);
		if(result.isEmpty()){
			return "true";
		}
		else{
			return "false";
		}
	}
}
