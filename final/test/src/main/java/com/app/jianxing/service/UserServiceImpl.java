package com.app.jianxing.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.jianxing.dao.UserDAO;
import com.app.jianxing.model.User;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	@Qualifier("userHibernateDAOImpl")
	private UserDAO userDAO;
	
	@Override
	@Transactional
	public void addUser(User user) {
		userDAO.addUser(user);
		
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		userDAO.updateUser(user);
		
	}

	@Override
	@Transactional
	public Collection<User> listUser() {
		return userDAO.listUser();
	}

	@Override
	@Transactional
	public User checkUser(String username, String userpwd) {
		
		return userDAO.checkUser(username, userpwd);
	}

	@Override
	@Transactional
	public User getUser(int userid) {
		return userDAO.getUser(userid);
	}

	@Override
	@Transactional
	public void deleteUser(int userid) {
		userDAO.deleteUser(userid);
		
	}

	@Override
	@Transactional
	public Collection<User> SearchUser(String name) {
		return userDAO.SearchUser(name);
	}

}
