package com.app.jianxing.dao;

import java.util.Collection;

import com.app.jianxing.model.Taxi;

public interface TaxiDAO {
	public void addTaxi(Taxi taxi);
	public void updateTaxi(Taxi taxi);
	public Collection<Taxi> listTaxi();
	public Taxi checkTaxi(String taxiname, String taxipwd);
	public Taxi getTaxi(int taxiid);
	public void deleteTaxi(int taxiid);
	public Collection<Taxi> SearchTaxi(String name);
}
