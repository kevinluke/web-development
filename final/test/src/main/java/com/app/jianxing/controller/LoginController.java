package com.app.jianxing.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.jianxing.model.Taxi;
import com.app.jianxing.model.User;
import com.app.jianxing.service.TaxiService;
import com.app.jianxing.service.UserService;



@Controller
@RequestMapping("/login.htm")
public class LoginController{
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	@Autowired
    private UserService userService;
	@Autowired
    private TaxiService taxiService;


    @RequestMapping(method=RequestMethod.POST)
    public ModelAndView handleRequest(@ModelAttribute("taxi") Taxi taxi, @ModelAttribute("user") User user, HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        ModelAndView mv = new ModelAndView();
        String value = hsr.getParameter("action");
        if(value.equals("user")){
            String username = hsr.getParameter("username");
            String userpwd = hsr.getParameter("userpwd");
            int errorcode=1;
            user = userService.checkUser(username, userpwd);
            if(user != null){
                mv.addObject("user", user);
                mv.setViewName("userpage");
            }
            else{
            	mv.addObject("usererror", errorcode);
            	mv.setViewName("index");
            }
        }
        else if(value.equals("driver")){
            String taxiname = hsr.getParameter("taxiname");
            String taxipwd = hsr.getParameter("taxipwd");
            taxi = taxiService.checkTaxi(taxiname, taxipwd);
            int errorcode=2;
            if(taxi != null){
                mv.addObject("taxi", taxi);
                mv.setViewName("driverpage");
            }
            else{
            	mv.addObject("usererror", errorcode);
            	mv.setViewName("index");
            }
        }
        else if(value.equals("type")){
            String type = hsr.getParameter("type");
            if(type.equals("User")){
                mv.setViewName("uregister");
            }
            else{
                mv.setViewName("dregister");
            }
        }
        else{
            mv.setViewName("error");
        }
        
        return mv;
    }
    
}	
