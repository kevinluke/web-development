package com.app.jianxing.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.jianxing.model.Taxi;
import com.app.jianxing.model.User;
import com.app.jianxing.service.TaxiService;

@Controller
public class TaxiRegisterController {

	@Autowired
	private TaxiService taxiService;
	
	@RequestMapping(value="/dregister.htm", method=RequestMethod.POST)
	public String addUser(@Valid Taxi taxi, BindingResult bindingResult, Model model){
		if (bindingResult.hasErrors()) {
			return "dregister";
		}
//		model.addAttribute(new User());
		taxiService.addTaxi(taxi);
		return "index";
	}
	
	@RequestMapping(value = "checkTaxi.do", method=RequestMethod.GET)
	@ResponseBody
	public String checkTaxi(HttpServletRequest hsr, HttpServletResponse hsr1){
		String name = hsr.getParameter("taxiname");
		Collection<Taxi> result = taxiService.SearchTaxi(name);
		if(result.isEmpty()){
			return "true";
		}
		else{
			return "false";
		}
	}
	
}
