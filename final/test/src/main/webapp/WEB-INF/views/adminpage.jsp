<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
<title>jquery音乐网站竖直下拉菜单 - 16素材网</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="<c:url value="resources/style/basic.css"/>"/>
<script type="text/javascript" src="<c:url value="resources/js/jquery-2.1.1.min.js"/>"></script>
<script type="text/javascript">
$(function(){
	$('.all>li').mouseover(function(e) {
        $(this).children().stop().slideDown(300);
    });
	$('.all>li').mouseout(function(e) {
        $(this).children().stop().slideUp(300);
    });
})
</script>
</head>

<body>

  <div id="left">
    	<div id="logo"></div>
        
        <div id="nav">
		<ul class="all">
        	<li class="li01">Hello Administrator
            	<ul class="li_01">
                	<li>Manage Transports</li>
                	<li>Manage Rechargement</li>
              </ul>
          </li>
            
            
        </ul>
    </div>
  </div>
<div align="center" style="width:1016px; margin:0 auto">
<div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
<p>适用浏览器：IE8、360、FireFox、Chrome、Safari、Opera、傲游、搜狗、世界之窗. </p>
<p>来源：<a href="http://www.16sucai.com/" target="_blank">16素材网</a></p>
</div>
</body>
</html>