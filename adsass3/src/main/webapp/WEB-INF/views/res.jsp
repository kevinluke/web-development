<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd"><%@ page import="java.awt.*"  %>


<%@ page import="org.springframework.web.multipart.MultipartFile"  %>
<%@ page import="org.apache.commons.io.FileUtils" %>
<%@ page import="org.apache.commons.io.FilenameUtils" %>

<html>
<head>
<title>Results</title>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	
<script type="text/javascript"
   src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.js"></script>

</head>
<body>

<nav class="navbar navbar-default style="background-color: #52cfeb ;" navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#navbar"
							aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="active"><a href=''>Home Page</a></li>
							<li class=""><a
								href='https://app.powerbi.com/groups/me/dashboards/65118f7b-a6ef-4b07-80e9-2f72ab44c3b4'>Real-time
									Azure Stream Analytics</a></li>
						</ul>
					</div>
				</div>
			</nav>
			
				Total
	<font size="5" color="0055ff">${requestScope.tonum }</font>
	tweets contain keyword!
	<br /> Final Anwser:
	<font size="5" color="0055ff">${requestScope.fa }</font>
	<br />

<% int i =1; %>
<c:forEach var="res" items="${requestScope.sf }">
	Day<%= i %>:<font size="5" color="0055ff"><c:out value="${res }"></c:out></font>
	<br/>
	<% i++; %>
</c:forEach>
<input type="hidden" id="d1" name="d1" value="${requestScope.sf[0] }" />

<input type="hidden" id="d2" name="d2" value="${requestScope.sf[1]}" />

<input type="hidden" id="d3" name="d3" value="${requestScope.sf[2]}" />

<input type="hidden" id="d4" name="d4" value="${requestScope.sf[3]}" />

<input type="hidden" id="d5" name="d5" value="${requestScope.sf[4]}" />

<input type="hidden" id="d6" name="d6" value="${requestScope.sf[5]}" />

<input type="hidden" id="d7" name="d7" value="${requestScope.sf[6]}" />

<canvas id="lineChart" > 

</canvas>

        <script>

            var ctx = document.getElementById("lineChart");

            ctx.height = 150;

            ctx.weight = 100; 


            var d1 = document.getElementById("d1").value;

            var d2 = document.getElementById("d2").value;

            var d3 = document.getElementById("d3").value;

            var d4 = document.getElementById("d4").value;

            var d5 = document.getElementById("d5").value;

            var d6 = document.getElementById("d6").value;

            var d7 = document.getElementById("d7").value;

            

            var myChart = new Chart(ctx, {

                type : 'line',

                data : {

                    labels : [ "Day1","Day2","Day3","Day4","Day5", "Day6","Day7"],

                    datasets : [ {
                        label: 'Sentiment By Day',
                        backgroundColor:'rgba(255, 99, 132,0.3)',
      
                        fillColor: 'rgba(255, 99, 132,0)',
                        strokeColor: 'rgba(220,180,0,1)',
                        pointColor: 'rgba(220,180,0,1)',
                        
                        data : [ d1,d2,d3,d4,d5,d6,d7],
                        
                        borderWidth : 1
                        

                    } ]
                    
/* 
                    datasets : [ {

                        label : ["Sentiment Line Bar Chart"],


                        

                         data : [ d1,d2,d3,d4,d5,d6,d7],

                        backgroundColor : [ 'rgba(75,0,145,0.8)','rgba(139,90,184,0.8)',  'rgba(185,156,212,0.8)',

                                            'rgba(220,205,233,0.8)','rgba(241,235,246,0.8)',  'rgba(245,225,225,0.8)',

                                            'rgba(250,205,205,0.8)'],

                        borderColor : [ 'rgba(75,0,145,0.8)','rgba(139,90,184,0.8)',  'rgba(185,156,212,0.8)',

                                        'rgba(220,205,233,0.8)','rgba(241,235,246,0.8)',  'rgba(255,255,255,0.8)',

                                        'rgba(250,205,205,0.8)'],

                        borderWidth : 1

                    } ]  */

                },

                

                options : {

                    scales : {

                        yAxes : [ {

                            

                            

                             ticks : {

                                beginAtZero : true

                            }

                        } ]

                    }

                }

            });

        </script> 
        
</body>
</html>