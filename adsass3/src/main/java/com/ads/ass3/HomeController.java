package com.ads.ass3;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		

		
		return "home";
		
	}
	

	
	
	@RequestMapping(value="testAzure.htm", method = RequestMethod.POST)
	public ModelAndView testAzure(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		System.out.println("*****************inside twitterController*******************");
		ModelAndView mv= new ModelAndView();
		JSONArray predictOutput = null;
		String Twitters;
		String finalAnswer = "not Vaild";
		String keyword =hsr.getParameter("testAzu");
		String restUrl="https://ussouthcentral.services.azureml.net/workspaces/94a7bdddd924402681345905483aac86/services/a80d3143cebc4d4b896b2a74a1c01958/execute?api-version=2.0&details=true";
		JSONObject user=new JSONObject();
		JSONObject inputa=new JSONObject();
		String res2 = "{\"Inputs\": {\"input1\": {\"ColumnNames\": [\"tweet_text\"],\"Values\": [";
		String test = hsr.getParameter("testAzu");
		test = test.replaceAll("[^\\w\\s]+", " ");

		res2+="[\""+test + "\"],";
		res2 = res2.substring(0,res2.length()-1);	
		res2+="]}},\"GlobalParameters\": {}}";
		Twitters = res2;
		String userInput = Twitters;
		
		System.out.println("*****************Connect to AZURE Start*******************");
        HttpPostReq httpPostReq=new HttpPostReq();
        HttpPost httpPost = httpPostReq.createConnectivity(restUrl);
        predictOutput = httpPostReq.executeReq(userInput, httpPost);
        int totalnumber = predictOutput.length();
        System.out.println("**RESULT**"+predictOutput);
        List<String> list = new ArrayList<String>();
        for (int i=0; i<predictOutput.length(); i++) {
        	System.out.println("**TEST**"+predictOutput.get(i).toString());
            list.add( predictOutput.get(i).toString().substring(6,10) );
        }
        float sum = 0;
        int pos = 0;
        int nal = 0;
        int neg = 0;
        int point1 = 0;
        int point2 = 0;
        int point3 = 0;
        int point4 = 0;
        int point5 = 0;
        int point6 = 0;
        int point7 = 0;
        int point8 = 0;
        int point9 = 0;
        int point10 = 0;
        for(String str : list){
        	if(Float.parseFloat(str)>=0.55) pos++;
        	else if(Float.parseFloat(str)>=0.45&&Float.parseFloat(str)<0.55) nal++;
        	else neg++;
        	sum+=Float.parseFloat(str);
        	if(Float.parseFloat(str)<=0.1) point1++;
        	else if(Float.parseFloat(str)<=0.2 && Float.parseFloat(str)>0.1) point2++;
        	else if(Float.parseFloat(str)<=0.3 && Float.parseFloat(str)>0.2) point3++;
        	else if(Float.parseFloat(str)<=0.4 && Float.parseFloat(str)>0.3) point4++;
        	else if(Float.parseFloat(str)<=0.5 && Float.parseFloat(str)>0.4) point5++;
        	else if(Float.parseFloat(str)<=0.6 && Float.parseFloat(str)>0.5) point6++;
        	else if(Float.parseFloat(str)<=0.7 && Float.parseFloat(str)>0.6) point7++;
        	else if(Float.parseFloat(str)<=0.8 && Float.parseFloat(str)>0.7) point8++;
        	else if(Float.parseFloat(str)<=0.9 && Float.parseFloat(str)>0.8) point9++;
        	else point10++;
        	
        }
        System.out.println(sum/(float)list.size());
        if(sum/(float)list.size()>=0.55){
        	finalAnswer = "Positive!";
        }
        else if(sum/(float)list.size()>=0.45&&sum/(float)list.size()<0.55) finalAnswer = "Neutral";
        else finalAnswer = "Negative :(";
        mv.addObject("finalAnswer", finalAnswer);
        mv.addObject("pos", pos);
        mv.addObject("nal", nal);
        mv.addObject("neg", neg);
        mv.addObject("point1", point1);
        mv.addObject("point2", point2);
        mv.addObject("point3", point3);
        mv.addObject("point4", point4);
        mv.addObject("point5", point5);
        mv.addObject("point6", point6);
        mv.addObject("point7", point7);
        mv.addObject("point8", point8);
        mv.addObject("point9", point9);
        mv.addObject("point10", point10);
        mv.addObject("totalnumber", totalnumber);
		mv.setViewName("result");
        return mv;
	
	}
	

	
	@RequestMapping(value="testdate.htm", method = RequestMethod.POST)
	public ModelAndView getDatasetByDate(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		ModelAndView mv = new ModelAndView();
		System.out.println("321");  
		try{
		cb.setDebugEnabled(true)
		.setOAuthConsumerKey("PbQjZZVYliRSWG8Rgjkj6V3Fx")
		  .setOAuthConsumerSecret("VlG3J7IqoLVs0E7pUJlb12H6zTp3nbJP0dyrQsNnGQyHusFv64")
		  .setOAuthAccessToken("4800636202-P6AMRIrlvSqtUw6wIrzbGGmPAG6zhox8sW4Sqv1")
		  .setOAuthAccessTokenSecret("TDuz8Zv9yI6bCvGehqIPvuL1F5zbeWfjHzlzl7lnvXci3");
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		
		List<String> num = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd");
		String tdate= hsr.getParameter("tdate");
		Date date1 =  formatter.parse(tdate);
		String udate= hsr.getParameter("udate");
		Date date2 =  formatter.parse(udate);

		List<List<String>> ress = new ArrayList<List<String>>();
		while(date2.after(date1)){
			List<String> res = new ArrayList<String>();
			Calendar rightNow = Calendar.getInstance();
	        rightNow.setTime(date1);
	        rightNow.add(Calendar.DAY_OF_YEAR,1);
	        Date date3=rightNow.getTime();
			Query query = new Query(hsr.getParameter("keyword"));
			query.setSince(formatter.format(date1));
			query.setUntil(formatter.format(date3));
            QueryResult result;
            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                for (Status tweet : tweets) {
                	if(!tweet.getCreatedAt().before(date1)){
                		num.add(tweet.getText());
	                	res.add(tweet.getText()+"TCA"+formatter.format(tweet.getCreatedAt()));   	
                	}	
                }
            } while ((query = result.nextQuery()) != null);
            ress.add(res);
            System.out.println(num.size()); 
            System.out.println(formatter.format(date1));
            date1 = date3;
		}
		
		List<Integer> total = new ArrayList<Integer>();
		List<String> fan = new ArrayList<String>();
		List<Float> sf = new ArrayList<Float>();
		float sumtotal = 0;
		for(List<String> list : ress){
			String res = "{\"Inputs\": {\"input1\": {\"ColumnNames\": [\"tweet_text\"],\"Values\": [";
			for (int i =0; i<list.size();i++){
				String str = list.get(i);
				str = str.replaceAll("[^\\w\\s]+", " ");

				System.out.println(str);
				res+="[\""+str + "\"],";
			}
			res = res.substring(0,res.length()-1);	
			res+="]}},\"GlobalParameters\": {}}";
			JSONArray predictOutput = null;
			String Twitters;
			String finalAnswer = "not Valid";
			String restUrl="https://ussouthcentral.services.azureml.net/workspaces/94a7bdddd924402681345905483aac86/services/a80d3143cebc4d4b896b2a74a1c01958/execute?api-version=2.0&details=true";
			JSONObject user=new JSONObject();
			JSONObject inputa=new JSONObject();
			Twitters = res;
			
			String userInput = Twitters;
			System.out.println("*****************Connect to AZURE Start*******************");
	        HttpPostReq httpPostReq=new HttpPostReq();
	        HttpPost httpPost = httpPostReq.createConnectivity(restUrl);
	        predictOutput = httpPostReq.executeReq(userInput, httpPost);
	        int totalnumber = predictOutput.length();
	        total.add(totalnumber);
	        List<String> liststr = new ArrayList<String>();
	        for (int i=0; i<predictOutput.length(); i++) {
	        	System.out.println("**TEST**"+predictOutput.get(i).toString().substring(6,10));
	        	liststr.add( predictOutput.get(i).toString().substring(6,10) );
	        }
	        float sum = 0;
	        for(String str : liststr){
	        	sum+=Float.parseFloat(str);
	        }
	        
	        sf.add(sum/(float)liststr.size());
	        if(sum/(float)liststr.size()>=0.55){
	        	finalAnswer = "Positive!";
	        }
	        else if(sum/(float)liststr.size()>=0.45&&sum/(float)liststr.size()<0.55) finalAnswer = "Neutral";
	        else finalAnswer = "Negative :(";
	        sumtotal+=sum/(float)liststr.size();
		}
		int tonum=0;
		for(int ts : total){
			tonum+=ts;
		}
		mv.addObject("sf",sf);
		mv.addObject("fan",fan);
		mv.addObject("total",total);
		mv.addObject("tdate", tdate);
		mv.addObject("udate", udate);
		mv.addObject("ress", ress);
		mv.addObject("tonum", tonum);
		int days = (int) ((formatter.parse(tdate).getTime()-formatter.parse(udate).getTime())/(1000*60*60*24));
		mv.addObject("days", days);
		String fa = "";
		float fafa = 0;
		for(float faa : sf){
			fafa+=faa;
		}
		days = Math.abs(days);
		if(fafa/(float)days>=0.55){
			fa = "Positive!";
        }
        else if(fafa/(float)days>=0.45&&fafa/(float)days<0.55) fa = "Neutral";
        else fa = "Negative :(";
		mv.addObject("fa", fa);
		mv.setViewName("res");
		}
		catch(TwitterException e){
			mv.setViewName("error");
			return mv;
			
		}
		
		return mv;
	
	}
}
