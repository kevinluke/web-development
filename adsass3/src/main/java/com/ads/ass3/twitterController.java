package com.ads.ass3;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Handles requests for the application home page.
 */
@Controller
public class twitterController {
	
	
	@RequestMapping(value = "/SearchTwitter.htm", method = RequestMethod.POST)
	public ModelAndView home(HttpServletRequest request, HttpServletResponse response,Locale locale, Model model) throws Exception {
		System.out.println("*****************inside twitterController*******************");
		ModelAndView mv= new ModelAndView();
		JSONArray predictOutput = null;
		String Twitters;
		String finalAnswer = "not Valid";
		String keyword =request.getParameter("keywordtest");
		String restUrl="https://ussouthcentral.services.azureml.net/workspaces/94a7bdddd924402681345905483aac86/services/a80d3143cebc4d4b896b2a74a1c01958/execute?api-version=2.0&details=true";
		JSONObject user=new JSONObject();
		JSONObject inputa=new JSONObject();

		Twitters = getDataset(keyword, request.getParameter("ddate"));
		if(Twitters=="error"){
			mv.setViewName("error");
			return mv;
		}
		String userInput = Twitters;
		
		System.out.println("*****************Connect to AZURE Start*******************");
        HttpPostReq httpPostReq=new HttpPostReq();
        HttpPost httpPost = httpPostReq.createConnectivity(restUrl);
        predictOutput = httpPostReq.executeReq(userInput, httpPost);
        int totalnumber = predictOutput.length();
        System.out.println("**RESULT**"+predictOutput);
        List<String> list = new ArrayList<String>();
        for (int i=0; i<predictOutput.length(); i++) {
        	System.out.println("**TEST**"+predictOutput.get(i).toString());
            list.add( predictOutput.get(i).toString().substring(6,10) );
        }
        float sum = 0;
        int pos = 0;
        int nal = 0;
        int neg = 0;
        int point1 = 0;
        int point2 = 0;
        int point3 = 0;
        int point4 = 0;
        int point5 = 0;
        int point6 = 0;
        int point7 = 0;
        int point8 = 0;
        int point9 = 0;
        int point10 = 0;
        for(String str : list){
        	if(Float.parseFloat(str)>=0.55) pos++;
        	else if(Float.parseFloat(str)>=0.45&&Float.parseFloat(str)<0.55) nal++;
        	else neg++;
        	sum+=Float.parseFloat(str);
        	if(Float.parseFloat(str)<=0.1) point1++;
        	else if(Float.parseFloat(str)<=0.2 && Float.parseFloat(str)>0.1) point2++;
        	else if(Float.parseFloat(str)<=0.3 && Float.parseFloat(str)>0.2) point3++;
        	else if(Float.parseFloat(str)<=0.4 && Float.parseFloat(str)>0.3) point4++;
        	else if(Float.parseFloat(str)<=0.5 && Float.parseFloat(str)>0.4) point5++;
        	else if(Float.parseFloat(str)<=0.6 && Float.parseFloat(str)>0.5) point6++;
        	else if(Float.parseFloat(str)<=0.7 && Float.parseFloat(str)>0.6) point7++;
        	else if(Float.parseFloat(str)<=0.8 && Float.parseFloat(str)>0.7) point8++;
        	else if(Float.parseFloat(str)<=0.9 && Float.parseFloat(str)>0.8) point9++;
        	else point10++;
        	
        }
        System.out.println(sum/(float)list.size());
        if(sum/(float)list.size()>=0.55){
        	finalAnswer = "Positive!";
        }
        else if(sum/(float)list.size()>=0.45&&sum/(float)list.size()<0.55) finalAnswer = "Neutral";
        else finalAnswer = "Negative :(";
        mv.addObject("finalAnswer", finalAnswer);
        mv.addObject("pos", pos);
        mv.addObject("nal", nal);
        mv.addObject("neg", neg);
        mv.addObject("point1", point1);
        mv.addObject("point2", point2);
        mv.addObject("point3", point3);
        mv.addObject("point4", point4);
        mv.addObject("point5", point5);
        mv.addObject("point6", point6);
        mv.addObject("point7", point7);
        mv.addObject("point8", point8);
        mv.addObject("point9", point9);
        mv.addObject("point10", point10);
        mv.addObject("totalnumber", totalnumber);
		mv.setViewName("result");
        return mv;
	
	}
	
	
	
	
	private static String getDataset(String keyword, String ddate) throws Exception {//, HttpServletRequest hsr, HttpServletResponse hsr1
		System.out.println("*****************Search Twitter Start*******************");
		System.out.println("Key Word:"+keyword);
		try{
		ConfigurationBuilder cb = new ConfigurationBuilder();
		
		cb.setDebugEnabled(true)
		.setOAuthConsumerKey("PbQjZZVYliRSWG8Rgjkj6V3Fx")
		  .setOAuthConsumerSecret("VlG3J7IqoLVs0E7pUJlb12H6zTp3nbJP0dyrQsNnGQyHusFv64")
		  .setOAuthAccessToken("4800636202-P6AMRIrlvSqtUw6wIrzbGGmPAG6zhox8sW4Sqv1")
		  .setOAuthAccessTokenSecret("TDuz8Zv9yI6bCvGehqIPvuL1F5zbeWfjHzlzl7lnvXci3");
//		  .setOAuthConsumerKey("gEFkJmIowN3jryXVlIbCLJc65")
//		  .setOAuthConsumerSecret("qElSd1n6SYP1TdrweXwVsM2RMZ48Vr4MkTsV4dXFI3McrM3ubO")
//		  .setOAuthAccessToken("709051323480723458-zu9YGV0qbZHCQ0mnuw1qntKRgfIyX27")
//		  .setOAuthAccessTokenSecret("ZBxEm6XtidQZZF1fhD1MnczRM8ErxgsF7UMTXXf1crjWE");
//		.setOAuthConsumerKey("eUL89IkTvO54eMTeNmUzXr2EK")
//		  .setOAuthConsumerSecret("7tSH8eSvufxjwtqy9RLRgClDIHOx0Bpot0fh46qoXe5zIdtdc3")
//		  .setOAuthAccessToken("2692099357-tSArR3QAD6ML1Io27URWLFpQ23NAtkkcdus8q64")
//		  .setOAuthAccessTokenSecret("psoNY7VqcKVom6hiidmKdJWNnQGAXnLau40Zd1ZTo2siI");
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		
		List<String> num = new ArrayList<String>();
		
		Query query = new Query(keyword);
		query.setUntil(ddate);
		QueryResult result;
		do {
			result = twitter.search(query);
			List<Status> tweets = result.getTweets();
			
			for (Status tweet : tweets) {
				num.add(tweet.getText());
            	}
			} 
		while ((query = result.nextQuery()) != null);
		
		
		String res = "{\"Inputs\": {\"input1\": {\"ColumnNames\": [\"tweet_text\"],\"Values\": [";
		for (int i =0; i<num.size();i++){
			String str = num.get(i);
			str = str.replaceAll("[^\\w\\s]+", " ");

			System.out.println(str);
			res+="[\""+str + "\"],";
		}
		res = res.substring(0,res.length()-1);	
		res+="]}},\"GlobalParameters\": {}}";
		

//		
//		String res2 = "{\"Inputs\": {\"input1\": {\"ColumnNames\": [\"tweet_text\"],\"Values\": [";
//		res2+="[\""+"it is so good" + "\"],";
//		res2 = res2.substring(0,res2.length()-1);	
//		res2+="]}},\"GlobalParameters\": {}}";
		return res;
		}
		catch(TwitterException e){
			return "error";
		}
	}
	
	
	
	
	

}
