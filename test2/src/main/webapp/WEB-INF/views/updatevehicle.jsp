<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Visual Admin</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
        <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>        <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/bootstrap-filestyle.min.js"/>"></script>  <!-- http://markusslima.github.io/bootstrap-filestyle/ -->
    <script type="text/javascript" src="<c:url value="resources/js/templatemo-script.js"/>"></script>  
    <link href="<c:url value='http://fonts.useso.com/css?family=Open+Sans:400,300,400italic,700'/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="resources/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/css/templatemo-style.css"/>" rel="stylesheet">
    
    <script>
                function dyn(name, list){
                	var radio = document.getElementsByName("radio");
                	var operation;
                	
                	for(var i=0; i<radio.length;i++){
                		if(radio[i].checked){
                			operation = radio[i].value;
                		}
                	}
                	var xmlhttp;
                		if (window.XMLHttpRequest){
                            xmlhttp=new XMLHttpRequest();
                        }
                        else{
                            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        
                        xmlhttp.onreadystatechange=function(){
                            if (xmlhttp.readyState==4 && xmlhttp.status==200){

                            	document.getElementById("dynamic").innerHTML=xmlhttp.responseText;       
                            }
                          }
                        xmlhttp.open("GET","dyn.htm?operation="+operation+"&vehicle="+name+"&list="+list,true);
                        xmlhttp.send();
                	
                    
                }
                </script>
        <script>
                function addStation(vehicle){
                	var index = document.getElementById("stations").selectedIndex;
                	var station = document.getElementById("stations").options[index].value;

                	var xmlhttp;
                		if (window.XMLHttpRequest){
                            xmlhttp=new XMLHttpRequest();
                        }
                        else{
                            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        
                        xmlhttp.onreadystatechange=function(){
                            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                            	if(xmlhttp.responseText == "Successful")
                            	alert("Successful! Please refresh your page to check result.");      
                            }
                          }
                        xmlhttp.open("GET","addstation.htm?station="+station+"&vehicle="+vehicle,true);
                        xmlhttp.send();
                	
                    
                }
                </script>   
                <script>
                	function formupdate(vehicle){
                		
                		var id = document.getElementById("stationid").value;
                		alert(id);
                		var xmlhttp;
                		if (window.XMLHttpRequest){
                            xmlhttp=new XMLHttpRequest();
                        }
                        else{
                            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        
                        xmlhttp.onreadystatechange=function(){
                            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                            	if(xmlhttp.responseText == "Successful")
                            	alert("Successful! Please refresh your page to check result.");      
                            }
                          }
                        xmlhttp.open("GET","deletestation.htm?stationid="+id+"&vehicle="+vehicle,true);
                        xmlhttp.send();
                	}
                </script>     
</head>
<body>
<div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>Hello! Administrator</h1>
        </header>
        <div class="profile-photo-container">
          <img src="<c:url value="resources/images/${Math.floor(Math.random()*10+1) }.jpg" />" alt="Profile Photo" class="img-responsive">
          <div class="profile-photo-overlay"></div>
        </div>
        <!-- Search box -->
        
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
          </div>
        <nav class="templatemo-left-nav">
          <ul>
<!--             <li><a href="index.html"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-bar-chart fa-fw"></i>Charts</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-database fa-fw"></i>Manage Recharges</a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker fa-fw"></i>Maps</a></li> -->
            
            <li><a href="#" class="active"><i class="fa fa-sliders fa-fw"></i>Manage Transports</a></li>

            <li><a href="index"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>
        </nav>
      </div>
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a >View Transports</a></li>
                <li><a class="active">Details</a></li>
                <li><a >Add Stations</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="templatemo-content-container">
          <div class="templatemo-content-widget white-bg">
   
          <div class="templatemo-content-widget white-bg col-1 text-center">
              <i class="fa fa-times"></i>
              <h2 class="text-uppercase">MBTA</h2>
              <h3 class="text-uppercase margin-bottom-10">Massachusetts Bay Transportation Authority</h3>
              <img src="<c:url value="resources/images/bicycle.jpg"/>" alt="Bicycle" class="img-circle img-thumbnail">
            </div>
              <div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">
                <i class="fa fa-times"></i>
                
                <div class="panel-heading templatemo-position-relative"><h2 class="text-uppercase">Stations Information</h2></div>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <td>Station ID</td>
                        <td>Station Name</td>
                      </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="station" items="${requestScope.vehicle.stations}">
                      <tr>
                        <td>${station.stationId }</td>
                        <td>${station.stationName }</td>
                      </tr>
                    </c:forEach>              
                    </tbody>
                  </table>    
                </div>                          
              </div>
             
            <form:form name="updateForm" action="updatevehicle.htm" class="templatemo-login-form" method="post" enctype="multipart/form-data">
            <div class="row form-group">
                <div class="col-lg-12 form-group">       
                <label class="control-label templatemo-block">Choose your operation</label>             
                    <div class="margin-right-15 templatemo-inline-block">
                      <input type="radio" name="radio" id="r4" value="add" onclick="javascript:dyn('${requestScope.vehicle.vehicleName}','${requestScope.stationList }')"/> 
                      <label for="r4" class="font-weight-400"><span></span>Add</label>
                    </div>
                    <div class="margin-right-15 templatemo-inline-block">
                      <input type="radio" name="radio" id="r5" value="delete" onclick="javascript:dyn('${requestScope.vehicle.vehicleName}')">
                      <label for="r5" class="font-weight-400"><span></span>Delete</label>
                    </div>
                </div>
              </div>
              <div class="row form-group">
              <div class="col-lg-12 form-group" id="dynamic">
              </div>
              </div>
            
                </form:form>

        </div>
        </div>
              <footer class="text-right">
            <p>Copyright &copy; Jianxing Lu
            | Final Project</p>
          </footer>
        </div>
	</div>
</body>
</html>