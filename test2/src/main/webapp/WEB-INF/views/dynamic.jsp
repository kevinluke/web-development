<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="col-lg-6 col-md-6 form-group">                  
                    <label for="stationid">Station ID</label>
                    <input type="text" class="form-control" id="stationid"/>                  
                </div>
                
            <div class="form-group text-right"> 
            
              <button type="button" class="templatemo-blue-button" onclick="formupdate('${requestScope.vehicle.vehicleName }')">Delete</button>
                <button type="reset" class="templatemo-white-button">Reset</button>
                </div>