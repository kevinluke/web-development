<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Visual Admin Dashboard - Preferences</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>      <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/jquery-migrate-1.2.1.min.js"/>"></script> 
        <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>        <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/bootstrap-filestyle.min.js"/>"></script>  <!-- http://markusslima.github.io/bootstrap-filestyle/ -->
    <script type="text/javascript" src="<c:url value="resources/js/templatemo-script.js"/>"></script>  
    <link href="<c:url value='http://fonts.useso.com/css?family=Open+Sans:400,300,400italic,700'/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="resources/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/css/templatemo-style.css"/>" rel="stylesheet">

	<script>
   function confirm(){
	   document.getElementById("method").value = "confirm";
	   document.forms["acceptorder"].submit();
   }
</script>

<script>
   function con(){
	   document.getElementById("method").value = "confirm";
	   document.forms["acceptorder"].submit();
   }
</script>

		<script>
                function check(){
                	var id = document.getElementById("orderid").value;
                	if(id ==0||isNaN(id)){
                		document.getElementById("orderlabel").innerHTML="Are U Crazy?? ";
                		document.getElementById("orderlabel").style.color="red";
                	}
                	else{
                		var xmlhttp;
                        if (window.XMLHttpRequest){
                            xmlhttp=new XMLHttpRequest();
                        }
                        else{
                            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange=function(){
                            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                            	alert(xmlhttp.responseText);
                            	if(xmlhttp.responseText=="true"){
                            		document.getElementById("orderlabel").innerHTML="Order ID:";
                            		document.getElementById("orderlabel").style.color="green";
                            	}
                            	else if(xmlhttp.responseText=="false"){
                            		document.getElementById("orderlabel").innerHTML="No one accept, what a pity :(";
                            		document.getElementById("orderlabel").style.color="red";
                            		var htm ="<label id='costlabel' for='orderid'>Change Your Cost</label><input name='cost' type='text' class='form-control' id='cost' onkeyup='checkcost()'/>";
                            		document.getElementById("dyn").innerHTML=htm;
                            	}
                            	else{
                            		document.getElementById("orderlabel").innerHTML="What's that??";
                            		document.getElementById("orderlabel").style.color="red";
                            	}
                            }
                          }
                        xmlhttp.open("GET","checkstatus.htm?orderid="+id,true);
                        xmlhttp.send();
                	}
                	
                }
                </script>
                <script>
                	function checkcost(){
                		var cost = document.getElementById("cost").value;
                    	if(cost ==0||isNaN(cost)){
                    		document.getElementById("costlabel").innerHTML="Are U Crazy?? ";
                    		document.getElementById("costlabel").style.color="red";
                    	}
                    	else{
                    		document.getElementById("costlabel").innerHTML="Cost";
                    		document.getElementById("costlabel").style.color="green";
                    	}
                	}
                </script>
  </head>
  <body>
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>Hi Hard-working ${requestScope.taxi.taxiname} :)</h1>
        </header>
        <div class="profile-photo-container">
          <img src="<c:url value="resources/images/${Math.floor(Math.random()*10+1) }.jpg" />" alt="Profile Photo" class="img-responsive">
          <div class="profile-photo-overlay"></div>
        </div>
        <!-- Search box -->
<%--         <form class="templatemo-search-form" role="search">
          <div class="input-group">
              <button type="submit" class="fa fa-search"></button>
              <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
          </div>
        </form> --%>
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
          </div>
        <nav class="templatemo-left-nav">
          <ul>
<!--             <li><a href="index.html"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-bar-chart fa-fw"></i>Charts</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-database fa-fw"></i>Data Visualization</a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker fa-fw"></i>Maps</a></li>
            <li><a href="manage-users.html"><i class="fa fa-users fa-fw"></i>Manage Users</a></li> -->
            <li><a ><i class="fa fa-sliders fa-fw"></i>Fast and Furious!</a></li>
            <li><a class="active"><i class="fa fa-users fa-fw"></i>Where is my MONEY</a></li>
            <li><a href="index"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>
        </nav>
      </div>
      <!-- Main content -->
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a class="active">My Income</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="templatemo-content-container">
          <div class="templatemo-content-widget white-bg">
            <h2 class="margin-bottom-10">This is my INCOME</h2>
            <p>HOLY CRAP, these are my GOLD~~</p>
            <div class="col-1">
            
              <div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">
                <i class="fa fa-times"></i>
<div class="panel panel-default no-border">
              <div class="panel-heading border-radius-10">
                <h2>Area Chart</h2>
              </div>
              <div class="panel-body">
                <div class="templatemo-flex-row flex-content-row">
                  <div class="col-1">
                    <div id="area_chart_div" class="templatemo-chart"></div>
                    <h3 class="text-center margin-bottom-5">Whole Income Information</h3>
                    <p class="text-center">this is a good way to analyze your income~~</p>                
                  </div>              
                </div>
              </div> 
            </div>                      
              </div>
            </div> 
            <form name="acceptorder" action="acceptorder.htm" class="templatemo-login-form" method="post" enctype="multipart/form-data">
              
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label id="orderlabel" for=""orderid"">Total Income</label>
                    <input name="orderid" type="text" class="form-control" id="orderid" value="${requestScope.taxi.income }"/>    
                              
     
                </div>
                <div id="dyn" class="col-lg-6 col-md-6 form-group">
                </div>
                  <input type="hidden" value="${requestScope.taxi }" name="taxi"/>  
                  <input type="hidden" value="" name="method" id="method"/> 
              </div>
              

            </form>
              
          </div>
         
        </div>
        <footer class="text-right">
            <p>Copyright &copy; Jianxing Lu
            | Final Project</p>
          </footer>
      </div>
    </div>

    <!-- JS -->
      <!-- Templatemo Script -->
      <script type="text/javascript" src="https://www.google.com/jsapi"></script> <!-- Google Chart -->
      <script>

      var gaugeChart;
      var gaugeData;
      var gaugeOptions;
      var timelineChart;
      var timelineDataTable;
      var timelineOptions;
      var areaData;
      var areaOptions;
      var areaChart;

      /* Gauage 
      --------------------------------------------------*/
      google.load("visualization", "1", {packages:["gauge"]});
      google.setOnLoadCallback(drawGauge);
      google.load("visualization", "1", {packages:["timeline"]});
      google.setOnLoadCallback(drawTimeline);
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);

      $(document).ready(function(){
        if($.browser.mozilla) {
          //refresh page on browser resize
          // http://www.sitepoint.com/jquery-refresh-page-browser-resize/
          $(window).bind('resize', function(e)
          {
            if (window.RT) clearTimeout(window.RT);
            window.RT = setTimeout(function()
            {
              this.location.reload(false); /* false to get page from cache */
            }, 200);
          });      
        } else {
          $(window).resize(function(){
            drawCharts();
          });  
        }   
      });

      function drawGauge() {

        gaugeData = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Memory', 80],
          ['CPU', 55],
          ['Network', 68]
        ]);

        gaugeOptions = {
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };

        gaugeChart = new google.visualization.Gauge(document.getElementById('gauge_div'));
        gaugeChart.draw(gaugeData, gaugeOptions);

        setInterval(function() {
          gaugeData.setValue(0, 1, 40 + Math.round(60 * Math.random()));
          gaugeChart.draw(gaugeData, gaugeOptions);
        }, 13000);
        setInterval(function() {
          gaugeData.setValue(1, 1, 40 + Math.round(60 * Math.random()));
          gaugeChart.draw(gaugeData, gaugeOptions);
        }, 5000);
        setInterval(function() {
          gaugeData.setValue(2, 1, 60 + Math.round(20 * Math.random()));
          gaugeChart.draw(gaugeData, gaugeOptions);
        }, 26000);        
      } // End function drawGauage

      /* Timeline
      --------------------------------------------------*/
      function drawTimeline() {
        var container = document.getElementById('timeline_div');
        timelineChart = new google.visualization.Timeline(container);
        timelineDataTable = new google.visualization.DataTable();
        timelineDataTable.addColumn({ type: 'string', id: 'Room' });
        timelineDataTable.addColumn({ type: 'string', id: 'Name' });
        timelineDataTable.addColumn({ type: 'date', id: 'Start' });
        timelineDataTable.addColumn({ type: 'date', id: 'End' });
        timelineDataTable.addRows([
          [ 'Magnolia Room',  'CSS Fundamentals',    new Date(0,0,0,12,0,0),  new Date(0,0,0,14,0,0) ],
          [ 'Magnolia Room',  'Intro JavaScript',    new Date(0,0,0,14,30,0), new Date(0,0,0,16,0,0) ],
          [ 'Magnolia Room',  'Advanced JavaScript', new Date(0,0,0,16,30,0), new Date(0,0,0,19,0,0) ],
          [ 'Gladiolus Room', 'Intermediate Perl',   new Date(0,0,0,12,30,0), new Date(0,0,0,14,0,0) ],
          [ 'Gladiolus Room', 'Advanced Perl',       new Date(0,0,0,14,30,0), new Date(0,0,0,16,0,0) ],
          [ 'Gladiolus Room', 'Applied Perl',        new Date(0,0,0,16,30,0), new Date(0,0,0,18,0,0) ],
          [ 'Petunia Room',   'Google Charts',       new Date(0,0,0,12,30,0), new Date(0,0,0,14,0,0) ],
          [ 'Petunia Room',   'Closure',             new Date(0,0,0,14,30,0), new Date(0,0,0,16,0,0) ],
          [ 'Petunia Room',   'App Engine',          new Date(0,0,0,16,30,0), new Date(0,0,0,18,30,0) ]]);

        timelineOptions = {
          timeline: { colorByRowLabel: true },
          backgroundColor: '#ffd'
        };

        timelineChart.draw(timelineDataTable, timelineOptions);
      } // End function drawTimeline

      /* Area Chart 
      --------------------------------------------------*/
      var orders = ${requestScope.orderList};
      function drawChart() {
    	  var i = 1;
    	  var dataArray = [['Time','Income']];
    	  for(var p in orders){
    		  dataArray.push ([i, orders[p]]);
    		  i+=1;
    	  }
    	  
    	 
    	 var data = new google.visualization.arrayToDataTable(dataArray);
		

        areaOptions = {
          title: 'Income',
          hAxis: {title: 'Time',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        areaChart = new google.visualization.AreaChart(document.getElementById('area_chart_div'));
        areaChart.draw(data, areaOptions);
      } // End function drawChart

      function drawCharts () {
          gaugeChart.draw(gaugeData, gaugeOptions);
          timelineChart.draw(timelineDataTable, timelineOptions);
          areaChart.draw(areaData, areaOptions);
      }

    </script>
  </body>
</html>
