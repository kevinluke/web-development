<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Visual Admin Dashboard - Preferences</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
        <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>        <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/bootstrap-filestyle.min.js"/>"></script>  <!-- http://markusslima.github.io/bootstrap-filestyle/ -->
    <script type="text/javascript" src="<c:url value="resources/js/templatemo-script.js"/>"></script>  
    <link href="<c:url value='http://fonts.useso.com/css?family=Open+Sans:400,300,400italic,700'/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="resources/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/css/templatemo-style.css"/>" rel="stylesheet">

	<script>
   function confirm(){
	   document.getElementById("method").value = "confirm";
	   document.forms["acceptorder"].submit();
   }
</script>

<script>
   function con(){
	   document.getElementById("method").value = "confirm";
	   document.forms["acceptorder"].submit();
   }
</script>

		<script>
                function check(){
                	var id = document.getElementById("orderid").value;
                	if(id ==0||isNaN(id)){
                		document.getElementById("orderlabel").innerHTML="Are U Crazy?? ";
                		document.getElementById("orderlabel").style.color="red";
                	}
                	else{
                		var xmlhttp;
                        if (window.XMLHttpRequest){
                            xmlhttp=new XMLHttpRequest();
                        }
                        else{
                            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange=function(){
                            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                            	alert(xmlhttp.responseText);
                            	if(xmlhttp.responseText=="true"){
                            		document.getElementById("orderlabel").innerHTML="Order ID:";
                            		document.getElementById("orderlabel").style.color="green";
                            	}
                            	else if(xmlhttp.responseText=="false"){
                            		document.getElementById("orderlabel").innerHTML="No one accept, what a pity :(";
                            		document.getElementById("orderlabel").style.color="red";
                            		var htm ="<label id='costlabel' for='orderid'>Change Your Cost</label><input name='cost' type='text' class='form-control' id='cost' onkeyup='checkcost()'/>";
                            		document.getElementById("dyn").innerHTML=htm;
                            	}
                            	else{
                            		document.getElementById("orderlabel").innerHTML="What's that??";
                            		document.getElementById("orderlabel").style.color="red";
                            	}
                            }
                          }
                        xmlhttp.open("GET","checkstatus.htm?orderid="+id,true);
                        xmlhttp.send();
                	}
                	
                }
                </script>
                <script>
                	function checkcost(){
                		var cost = document.getElementById("cost").value;
                    	if(cost ==0||isNaN(cost)){
                    		document.getElementById("costlabel").innerHTML="Are U Crazy?? ";
                    		document.getElementById("costlabel").style.color="red";
                    	}
                    	else{
                    		document.getElementById("costlabel").innerHTML="Cost";
                    		document.getElementById("costlabel").style.color="green";
                    	}
                	}
                </script>
  </head>
  <body>
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>Hi Hard-working ${requestScope.taxi.taxiname} :)</h1>
        </header>
        <div class="profile-photo-container">
          <img src="<c:url value="resources/images/${Math.floor(Math.random()*10+1) }.jpg" />" alt="Profile Photo" class="img-responsive">
          <div class="profile-photo-overlay"></div>
        </div>
        <!-- Search box -->
<%--         <form class="templatemo-search-form" role="search">
          <div class="input-group">
              <button type="submit" class="fa fa-search"></button>
              <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
          </div>
        </form> --%>
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
          </div>
        <nav class="templatemo-left-nav">
          <ul>
<!--             <li><a href="index.html"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-bar-chart fa-fw"></i>Charts</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-database fa-fw"></i>Data Visualization</a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker fa-fw"></i>Maps</a></li>
            <li><a href="manage-users.html"><i class="fa fa-users fa-fw"></i>Manage Users</a></li> -->
            <li><a class="active"><i class="fa fa-sliders fa-fw"></i>Fast and Furious!</a></li>
            <li><a href="income.htm?taxi=${requestScope.taxi }&list=${requestScope.orderList}"><i class="fa fa-users fa-fw"></i>Where is my MONEY</a></li>
            <li><a href="index"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>
        </nav>
      </div>
      <!-- Main content -->
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a >Check Order</a></li>
                <li><a class="active">My Order</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="templatemo-content-container">
          <div class="templatemo-content-widget white-bg">
            <h2 class="margin-bottom-10">Accept a ORDER</h2>
            <p> Hey, do you wanna to meet a HOOOOT GUY? Hurry Hurry</p>
            <div class="col-1">
              <div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">
                <i class="fa fa-times"></i>
                
                <div class="panel-heading templatemo-position-relative"><h2 class="text-uppercase">Your Order</h2></div>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <td>Order Id</td>
                        <td>Order To</td>
                        <td>Order By</td>
                        <td>Order Status</td>
                        <td>Payment</td>
                      </tr>
                    </thead>
                    <tbody>
	                <c:forEach var="order" items="${requestScope.orderList}">
                      <tr>
                        <td>${order.orderId }</td>
                        <td>${order.taxi.taxiid }</td>
                        <td>${order.user.userId }</td>
                        <td>${order.orderStatus }</td>
                        <td>${order.orderPayment }</td>
                      </tr>
                    </c:forEach>        
                    </tbody>
                  </table>    
                </div>                          
              </div>
            </div> 
            <form name="acceptorder" action="acceptorder.htm" class="templatemo-login-form" method="post" enctype="multipart/form-data">
              
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label id="orderlabel" for=""orderid"">Order ID</label>
                    <input name="orderid" type="text" class="form-control" id="orderid" onkeyup="check()"/>    
                               
                </div>
                <div id="dyn" class="col-lg-6 col-md-6 form-group">
                </div>
                  <input type="hidden" value="${requestScope.taxi }" name="taxi"/>  
                  <input type="hidden" value="" name="method" id="method"/> 
              </div>
              
                <div class="form-group text-right">
              <button type="button" class="templatemo-blue-button" onclick="confirm()">Complete</button>
                   </div>
            </form>
              
          </div>
         
        </div>
        <footer class="text-right">
            <p>Copyright &copy; Jianxing Lu
            | Final Project</p>
          </footer>
      </div>
    </div>

    <!-- JS -->
      <!-- Templatemo Script -->
  </body>
</html>
