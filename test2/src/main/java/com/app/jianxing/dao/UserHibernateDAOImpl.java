package com.app.jianxing.dao;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.jianxing.model.User;

@Repository	
public class UserHibernateDAOImpl implements UserDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		sessionFactory.getCurrentSession().flush();		
	}

	@Override
	public void updateUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		sessionFactory.getCurrentSession().flush();
		
	}

	@Override
	public Collection<User> listUser() {
		return sessionFactory.getCurrentSession().createQuery("from User").list();
	}

	@Override
	public User checkUser(String username, String userpwd) {
		/*User user = (User)sessionFactory.getCurrentSession().get(User.class, username);
		if(user.getUserPwd().equals(userpwd)){
			return user;
		}
		else 
			return null;*/
		Query query = sessionFactory.getCurrentSession().createQuery("from User "+"where userName =:userName and userPwd =:userPwd");
		query.setParameter("userName", username);
		query.setParameter("userPwd", userpwd);
		User user = (User) query.uniqueResult();
		return user;
	}

	@Override
	public void deleteUser(int userid) {
		User user = getUser(userid);
		if (user != null) {
			sessionFactory.getCurrentSession().delete(user);
			sessionFactory.getCurrentSession().flush();
		}
	}

	@Override
	public Collection<User> SearchUser(String name) {
		Query query = sessionFactory.getCurrentSession().createQuery("from User where userName =:userName");
		query.setParameter("userName", name);
		Collection<User> result = query.list();
		return result;
	}

	@Override
	public User getUser(int userid) {
		return (User) sessionFactory.getCurrentSession().get(User.class, userid);
	}

	@Override
	public User getUserByName(String username) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from User "+"where userName =:userName");
		query.setParameter("userName", username);
		User user = (User) query.uniqueResult();
		return user;
	}

}
