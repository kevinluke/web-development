package com.app.jianxing.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.jianxing.model.Card;
import com.app.jianxing.model.Order;

@Repository
public class CardHibernateDAOImpl implements CardDAO{

	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public void addCard(Card card) {
		sessionFactory.getCurrentSession().saveOrUpdate(card);
		sessionFactory.getCurrentSession().flush();	
		
	}

	@Override
	public void updateCard(Card card) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(card);
		sessionFactory.getCurrentSession().flush();	
		
	}

	@Override
	public Collection<Card> listCard(int cardid) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Card where cardId=:cardId");
		query.setParameter("cardId", cardid);
		Collection<Card> result = query.list();
		return result;
	}

	@Override
	public Card checkCard(String cardname, String cardpwd) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Card getCard(int cardid) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Card where cardId=:cardId");
		query.setParameter("cardId", cardid);
		return (Card)query.uniqueResult();
	}

	@Override
	public Card getCardByName(String cardname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteCard(int cardid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Object> SearchCard(String name) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Card as o left join o.user as u where u.userName=:userName");
		query.setParameter("userName", name);
		List<Object> cards = query.list();
		return cards;
	}

}
