package com.app.jianxing.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.jianxing.model.Station;
import com.app.jianxing.model.Vehicle;


@Repository	
public class VehicleHibernateDAOImpl implements VehicleDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addVehicle(Vehicle vehicle) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(vehicle);
		sessionFactory.getCurrentSession().flush();	
	}

	@Override
	public void updateVehicle(Vehicle vehicle) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(vehicle);
		sessionFactory.getCurrentSession().flush();
	}

	@Override
	public Collection<Vehicle> listVehicle() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Vehicle").list();
	}

	@Override
	public Vehicle checkVehicle(String vehiclename, String vehiclepwd) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vehicle getVehicle(String vehiclename) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Vehicle where vehicleName=:vehicleName");
		query.setParameter("vehicleName", vehiclename);
		
		return (Vehicle)query.uniqueResult();
	}

	@Override
	public void deleteVehicle(String vehiclename) {
		// TODO Auto-generated method stub
		Vehicle vehicle = getVehicle(vehiclename);
		if (vehicle != null) {
			sessionFactory.getCurrentSession().delete(vehicle);
			sessionFactory.getCurrentSession().flush();
		}
	}

	@Override
	public List<Vehicle> getAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Vehicle");
		List<Vehicle> allV = query.list();
		return allV;
	}

	@Override
	public Collection<Vehicle> SearchVehicle(String vehiclename) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Vehicle where vehicleName=:vehicleName");
		query.setParameter("vehicleName", vehiclename);
		Collection<Vehicle> result = query.list();
		return result;
	}

	

}
