package com.app.jianxing.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.jianxing.model.Station;
import com.app.jianxing.model.User;
import com.app.jianxing.model.Vehicle;
import com.app.jianxing.model.Station;

@Repository	
public class StationHibernateDAOImpl implements StationDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addStation(Station station) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(station);
		sessionFactory.getCurrentSession().flush();	
	}

	@Override
	public void updateStation(Station station) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(station);
		sessionFactory.getCurrentSession().flush();
	}

	@Override
	public Collection<Station> listStation() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Station").list();
	}

	@Override
	public Station checkStation(String stationname, String stationpwd) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Station getStation(String stationname) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Station where stationName=:stationName");
		query.setParameter("stationName", stationname);
		
		return (Station)query.uniqueResult();
	}

	@Override
	public void deleteStation(int stationid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Station> getAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Station");
		List<Station> allS = query.list();
		return allS;
	}

	@Override
	public Collection<Station> SearchStation(String stationname) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Station where stationName=:stationName");
		query.setParameter("stationName", stationname);
		Collection<Station> result = query.list();
		return result;
	}

	@Override
	public Station getStationbyID(int id) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Station where stationId=:stationId");
		query.setParameter("stationId", id);
		return (Station)query.uniqueResult();
	}



}
