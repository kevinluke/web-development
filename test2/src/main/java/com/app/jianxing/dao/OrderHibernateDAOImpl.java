package com.app.jianxing.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.jianxing.model.Order;
import com.app.jianxing.model.Station;

@Repository	
public class OrderHibernateDAOImpl implements OrderDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addOrder(Order order) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(order);
		sessionFactory.getCurrentSession().flush();	
	}

	@Override
	public void updateOrder(Order order) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(order);
		sessionFactory.getCurrentSession().flush();	
	}

	@Override
	public Collection<Order> listOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Order getOrder(String ordername) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteOrder(int orderid) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public Collection<Order> SearchOrder(int orderid) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Order where orderId=:orderId");
		query.setParameter("orderId", orderid);
		Collection<Order> result = query.list();
		return result;
	}

	@Override
	public Order getOrderbyID(int id) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Order where orderId=:orderId");
		query.setParameter("orderId", id);
		return (Order)query.uniqueResult();
	}

	@Override
	public List<Object> getAllByUser(int userid) {
		// TODO Auto-generated method stub
		
		Query query = sessionFactory.getCurrentSession().createQuery("from Order as o left join o.user as u where u.userId=:userId");
		query.setParameter("userId", userid);
		List<Object> orders = query.list();
		return orders;
	}

	@Override
	public List<Object> getAllByTaxi(int taxiid) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Order as o left join o.taxi as u where u.taxiid=:taxiid");
		query.setParameter("taxiid", taxiid);
		List<Object> orders = query.list();
		return orders;
	}

	@Override
	public List<Order> getAll() {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Order");
		List<Order> allS = query.list();
		return allS;
	}

}
