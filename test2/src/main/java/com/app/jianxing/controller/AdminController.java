package com.app.jianxing.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.app.jianxing.model.Station;
import com.app.jianxing.model.User;
import com.app.jianxing.model.Vehicle;
import com.app.jianxing.service.StationService;
import com.app.jianxing.service.VehicleService;

@Controller
public class AdminController {
	
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private StationService stationService;
	
	@RequestMapping(value="managevehicle.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(@ModelAttribute("vehicle") Vehicle vehicle, HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        ModelAndView mv = new ModelAndView();
        
        String vehiclename = hsr.getParameter("vehicles");
        String operation = hsr.getParameter("operations");
        vehicle = vehicleService.getVehicle(vehiclename);
        System.out.println(vehicle.getVehicleName());
        System.out.println(stationService.getAll().size());
        if(operation.equals("update")){
        	mv.addObject("vehicle", vehicle);
        	mv.addObject("stationList", stationService.getAll());
        	mv.setViewName("updatevehicle");
        }
        else if(operation.equals("add")){
        	mv.setViewName("addvehicle");
        }
        else{
        	vehicleService.deleteVehicle(vehicle.getVehicleName());
        	mv.setViewName("addsuccess");
        }
        return mv;
	}
	
	@RequestMapping(value="dyn.htm", method=RequestMethod.GET)
	public ModelAndView update(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		System.out.println("sdfs");
		String operation = hsr.getParameter("operation");
		String vehiclename = hsr.getParameter("vehicle");
		System.out.println(vehiclename);
		System.out.println("sdfsaaa");
		Vehicle vehicle = vehicleService.getVehicle(vehiclename);
		System.out.println(vehicle.getVehicleName());
		
		System.out.println(stationService.getAll().size());
		if(operation.equals("add")){
			mv.addObject("stationList", stationService.getAll());
			System.out.println("sdfs");
			mv.addObject("vehicle", vehicle);
			System.out.println(stationService.getAll().size());
			mv.setViewName("dynadd");
		}
		else{
			mv.addObject("vehicle", vehicle);
			mv.setViewName("dynamic");
		}
		return mv;
	}
	
	@RequestMapping(value="addstation.htm", method=RequestMethod.GET)
	public ModelAndView AddStation(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		String stationname = hsr.getParameter("station");
		String vehiclename = hsr.getParameter("vehicle");
		Vehicle vehicle = vehicleService.getVehicle(vehiclename);
		Station station = stationService.getStation(stationname);
		vehicle.getStations().add(station);
		vehicleService.updateVehicle(vehicle);
		mv.addObject("vehicle", vehicle);
    	mv.addObject("stationList", stationService.getAll());
    	mv.setViewName("updatevehicle");
    	return mv;
	}
	
	@RequestMapping(value="checkstation.htm", method=RequestMethod.GET)
	@ResponseBody
	public String CheckStation(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		String stationname = hsr.getParameter("stationname");
		Collection<Station> stations = stationService.SearchStation(stationname);
		if(stations.isEmpty()){
			return "true";
		}
		else{
			return "false";
		}
	}
	
	@RequestMapping(value="checkvehicle.htm", method=RequestMethod.GET)
	@ResponseBody
	public String Checkvehicle(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		String vehiclename = hsr.getParameter("vehiclename");
		Collection<Vehicle> stations = vehicleService.SearchVehicle(vehiclename);
		if(stations.isEmpty()){
			return "true";
		}
		else{
			return "false";
		}
	}
	
	
	@RequestMapping(value="createstation.htm", method=RequestMethod.POST)
	public ModelAndView CreateStation(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		int num = Integer.parseInt(hsr.getParameter("number"));
		
		for(int i=0; i<num; i++){
			String number = String.valueOf(i);
			String name = hsr.getParameter(number);
			Station station = new Station();
			System.out.println(name);
			station.setStationName(name);
			stationService.addStation(station);
		}
		mv.setViewName("addsuccess");
		
		return mv;
	}
	
	@RequestMapping(value="createvehicle.htm", method=RequestMethod.POST)
	public ModelAndView CreateVehicle(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		ModelAndView mv = new ModelAndView();
		int num = Integer.parseInt(hsr.getParameter("number"));
		for(int i=0; i<num; i+=3){
			String number = String.valueOf(i);
			String a = String.valueOf(i+1);
			String b = String.valueOf(i+2);
			String name = hsr.getParameter(number);
			System.out.println(name);
			Float price = Float.parseFloat(hsr.getParameter(a));
			System.out.println(price);
			String type = hsr.getParameter(b);
			System.out.println(type);
			Vehicle vehicle = new Vehicle();
			System.out.println(name);
			vehicle.setVehicleName(name);
			vehicle.setVehiclePrice(price);
			vehicle.setVehicleType(type);
			vehicleService.addVehicle(vehicle);
		}
		mv.setViewName("addsuccess");
		
		return mv;
	}
	
	
	@RequestMapping(value="deletestation.htm", method=RequestMethod.GET)
	@ResponseBody
	public String DeleteStation(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		String vehiclename = hsr.getParameter("vehicle");
		int stationid = Integer.parseInt(hsr.getParameter("stationid"));
		
		Vehicle vehicle = vehicleService.getVehicle(vehiclename);
		Station station = stationService.getStationbyID(stationid);

		vehicle.getStations().remove(station);
		vehicleService.updateVehicle(vehicle);
		
		return "Successful";
	}
}
