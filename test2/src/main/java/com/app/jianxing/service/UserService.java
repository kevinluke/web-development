package com.app.jianxing.service;

import java.util.Collection;

import com.app.jianxing.model.User;

public interface UserService {
	public void addUser(User user);
	public void updateUser(User user);
	public Collection<User> listUser();
	public User checkUser(String username, String userpwd);
	public User getUser(int userid);
	public User getUserByName(String username);
	public void deleteUser(int userid);
	public Collection<User> SearchUser(String name);
}
