package com.app.jianxing.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.app.jianxing.dao.VehicleDAO;
import com.app.jianxing.model.Vehicle;

@Service
public class VehicleServiceImpl implements VehicleService{
	@Autowired
	@Qualifier("vehicleHibernateDAOImpl")
	private VehicleDAO vehicleDAO;

	@Override
	public void addVehicle(Vehicle vehicle) {
		// TODO Auto-generated method stub
		vehicleDAO.addVehicle(vehicle);
	}

	@Override
	public void updateVehicle(Vehicle vehicle) {
		// TODO Auto-generated method stub
		vehicleDAO.updateVehicle(vehicle);
	}

	@Override
	public Collection<Vehicle> listVehicle() {
		// TODO Auto-generated method stub
		return vehicleDAO.listVehicle();
	}

	@Override
	public Vehicle checkVehicle(String vehiclename, String vehiclepwd) {
		// TODO Auto-generated method stub
		return vehicleDAO.checkVehicle(vehiclename, vehiclepwd);
	}

	@Override
	public Vehicle getVehicle(String vehiclename) {
		// TODO Auto-generated method stub
		return vehicleDAO.getVehicle(vehiclename);
	}

	@Override
	public void deleteVehicle(String vehiclename) {
		// TODO Auto-generated method stub
		vehicleDAO.deleteVehicle(vehiclename);
	}

	@Override
	public List<Vehicle> getAll() {
		return vehicleDAO.getAll();
	}

	@Override
	public Collection<Vehicle> SearchVehicle(String vehiclename) {
		// TODO Auto-generated method stub
		return vehicleDAO.SearchVehicle(vehiclename);
	}
}
