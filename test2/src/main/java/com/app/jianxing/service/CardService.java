package com.app.jianxing.service;

import java.util.Collection;
import java.util.List;

import com.app.jianxing.model.Card;

public interface CardService {
	public void addCard(Card card);
	public void updateCard(Card card);
	public Collection<Card> listCard(int cardid);
	public Card checkCard(String cardname, String cardpwd);
	public Card getCard(int cardid);
	public Card getCardByName(String cardname);
	public void deleteCard(int cardid);
	public List<Object> SearchCard(String name);
}
