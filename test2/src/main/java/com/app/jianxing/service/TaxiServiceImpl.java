package com.app.jianxing.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.jianxing.dao.TaxiDAO;
import com.app.jianxing.model.Taxi;

@Service
public class TaxiServiceImpl implements TaxiService{

	@Autowired
	@Qualifier("taxiHibernateDAOImpl")
	private TaxiDAO taxiDAO;
	@Override
	public void addTaxi(Taxi taxi) {
		taxiDAO.addTaxi(taxi);
		
	}

	@Override
	@Transactional
	public void updateTaxi(Taxi taxi) {
		taxiDAO.updateTaxi(taxi);
		
	}

	@Override
	@Transactional
	public Collection<Taxi> listTaxi() {
		return taxiDAO.listTaxi();
	}

	@Override
	@Transactional
	public Taxi checkTaxi(String taxiname, String taxipwd) {
		return taxiDAO.checkTaxi(taxiname, taxipwd);
	}

	@Override
	@Transactional
	public Taxi getTaxi(int taxiid) {
		return taxiDAO.getTaxi(taxiid);
	}

	@Override
	@Transactional
	public void deleteTaxi(int taxiid) {
		taxiDAO.deleteTaxi(taxiid);
		
	}

	@Override
	@Transactional
	public Collection<Taxi> SearchTaxi(String name) {
		return taxiDAO.SearchTaxi(name);
	}

	@Override
	public Taxi getTaxiByName(String taxiname) {
		// TODO Auto-generated method stub
		return taxiDAO.getTaxiByName(taxiname);
	}

}
