package com.app.jianxing.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.app.jianxing.dao.CardDAO;
import com.app.jianxing.model.Card;

@Service
public class CardServiceImpl implements CardService{
	
	@Autowired
	@Qualifier("cardHibernateDAOImpl")
	private CardDAO cardDAO;
	@Override
	public void addCard(Card card) {
		// TODO Auto-generated method stub
		cardDAO.addCard(card);
	}

	@Override
	public void updateCard(Card card) {
		// TODO Auto-generated method stub
		cardDAO.updateCard(card);
	}

	@Override
	public Collection<Card> listCard(int cardid) {
		// TODO Auto-generated method stub
		return cardDAO.listCard(cardid);
	}

	@Override
	public Card checkCard(String cardname, String cardpwd) {
		// TODO Auto-generated method stub
		return cardDAO.checkCard(cardname, cardpwd);
	}

	@Override
	public Card getCard(int cardid) {
		// TODO Auto-generated method stub
		return cardDAO.getCard(cardid);
	}

	@Override
	public Card getCardByName(String cardname) {
		// TODO Auto-generated method stub
		return cardDAO.getCardByName(cardname);
	}

	@Override
	public void deleteCard(int cardid) {
		// TODO Auto-generated method stub
		cardDAO.deleteCard(cardid);
	}

	@Override
	public List<Object> SearchCard(String name) {
		// TODO Auto-generated method stub
		return cardDAO.SearchCard(name);
	}

}
