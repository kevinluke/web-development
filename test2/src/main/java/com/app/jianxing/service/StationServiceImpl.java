package com.app.jianxing.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.app.jianxing.dao.StationDAO;
import com.app.jianxing.model.Station;

@Service
public class StationServiceImpl implements StationService{
	@Autowired
	@Qualifier("stationHibernateDAOImpl")
	private StationDAO stationDAO;

	@Override
	public void addStation(Station station) {
		// TODO Auto-generated method stub
		stationDAO.addStation(station);
	}

	@Override
	public void updateStation(Station station) {
		// TODO Auto-generated method stub
		stationDAO.updateStation(station);
	}

	@Override
	public Collection<Station> listStation() {
		// TODO Auto-generated method stub
		return stationDAO.listStation();
	}

	@Override
	public Station checkStation(String stationname, String stationpwd) {
		// TODO Auto-generated method stub
		return stationDAO.checkStation(stationname, stationpwd);
	}

	@Override
	public Station getStation(String stationname) {
		// TODO Auto-generated method stub
		return stationDAO.getStation(stationname);
	}

	@Override
	public void deleteStation(int stationid) {
		// TODO Auto-generated method stub
		stationDAO.deleteStation(stationid);
	}

	@Override
	public List<Station> getAll() {
		// TODO Auto-generated method stub
		return stationDAO.getAll();
	}

	@Override
	public Collection<Station> SearchStation(String stationname) {
		// TODO Auto-generated method stub
		return stationDAO.SearchStation(stationname);
	}

	@Override
	public Station getStationbyID(int id) {
		// TODO Auto-generated method stub
		return stationDAO.getStationbyID(id);
	}
}
