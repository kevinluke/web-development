<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
    <head>
        <title>Animated Form Switching with jQuery</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
		<link rel="shortcut icon" href="<c:url value="resources/favicon.ico"/> "/>
        <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/style.css"/>" />
		<script src="<c:url value="resources/js/cufon-yui.js" />"></script>
		<script src="<c:url value="resources/js/ChunkFive_400.font.js" /> "></script>
		<script type="text/javascript">
			Cufon.replace('h1',{ textShadow: '1px 1px #fff'});
			Cufon.replace('h2',{ textShadow: '1px 1px #fff'});
			Cufon.replace('h3',{ textShadow: '1px 1px #000'});
			Cufon.replace('.back');
		</script>
		<script>
                    function check(){
                        var pwd = document.getElementById("pwd").value;
                        var repwd = document.getElementById("repwd").value;
                        if(pwd == repwd){
                        	document.getElementById("reenter").innerHTML="Reenter:";
                            document.getElementById("reenter").style.color="green";
                        }
                        else{
                        	document.getElementById("reenter").innerHTML="Reenter: Not Match!";
                            document.getElementById("reenter").style.color="red";
                        }
                    }
                </script>
        <script>
                function checkunique(){
                	var username = document.getElementById("un").value;
                	var xmlhttp;
                    if (window.XMLHttpRequest){
                        xmlhttp=new XMLHttpRequest();
                    }
                    else{
                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange=function(){
                        if (xmlhttp.readyState==4 && xmlhttp.status==200){
                        	if(xmlhttp.responseText=="true"){
                        		document.getElementById("checkUser").innerHTML="Username:";
                        		document.getElementById("checkUser").style.color="green";
                        	}
                        	else{
                        		document.getElementById("checkUser").innerHTML="Username: Existed!";
                        		document.getElementById("checkUser").style.color="red";
                        	}
                            
                        }
                      }
                    xmlhttp.open("GET","checkUser.do?username="+username,true);
                    xmlhttp.send();
                }
                </script>
                <script>
                   function formregister(){
                	   var fn = document.getElementById("fn").value;
                	   var ln = document.getElementById("ln").value;
                	   var un = document.getElementById("un").value;
                	   var uc = document.getElementById("checkUser").style.color;
                	   var pc = document.getElementById("reenter").style.color;
                	   if(fn == ""||ln ==""||un==""||pc=="red"||uc=="red"){
                		   alert("Please check your input!!");
                	   }
                	   else{
                		   alert("Success! You can log in now!");
                		   document.forms["registerform"].submit();
                	   }

                   }
		</script>
<title>Insert title here</title>
</head>
<body>
		<div class="wrapper">
			<h1>Smart-Transportation Login System</h1>
			<h2>click the <span>orange links</span> to switch login role</h2>
			<div class="content">
				<div id="form_wrapper" class="form_wrapper">
				
                                   
						<form:form class="login active" action="uregister.htm" name="registerform" modelAttribute="user" method="POST" id="register" enctype="multipart/form-data">
						<h3>Register</h3>
						<div class="column">
							<div>
								<label>First Name:</label>
								<form:input path="userFname" id="fn"/>
								<font color="red"><form:errors path="userFname"/></font>
							</div>
							<div>
								<label>Last Name:</label>
								<form:input path="userLname" id="ln"/>
								<font color="red"><form:errors path="userLname"/></font>
							</div>
							<div>
								<label>Password:</label>
                                                                <form:input type="password" path="userPwd" id="pwd" name="pwd"/>
								<font color="red"><form:errors path="userPwd"/></font>
							</div>
						</div>
						<div class="column">
							<div>
								<label id="checkUser">Username:</label>
                              <form:input path="userName" id="un" type="text" onkeyup="checkunique()"/>
								<font color="red"><form:errors path="userName"/></font>
							</div>
							<div>
								<label>Type:</label>
                                           <input type="text" value="USER" disabled/>                     
							</div>
							<div>
								<label id="reenter">Reenter: </label>
                                                                
                                                                <input type="password" id="repwd" onkeyup="check()"/>
							</div>
						</div>
						<div class="bottom">
							
                            <input type="submit" value="Register" onclick="formregister()"/>
							<div class="clear"></div>
						</div>
					</form:form>

				</div>
				<div class="clear"></div>
			</div>
			<a class="back" href="http://localhost:8080/Final_Project/index.htm">back to home page</a>
		</div>
		

		<!-- The JavaScript -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript">
			$(function() {
					//the form wrapper (includes all forms)
				var $form_wrapper	= $('#form_wrapper'),
					//the current form is the one with class active
					$currentForm	= $form_wrapper.children('form.active'),
					//the change form links
					$linkform		= $form_wrapper.find('.linkform');
						
				//get width and height of each form and store them for later						
				$form_wrapper.children('form').each(function(i){
					var $theForm	= $(this);
					//solve the inline display none problem when using fadeIn fadeOut
					if(!$theForm.hasClass('active'))
						$theForm.hide();
					$theForm.data({
						width	: $theForm.width(),
						height	: $theForm.height()
					});
				});
				
				//set width and height of wrapper (same of current form)
				setWrapperWidth();
				
				/*
				clicking a link (change form event) in the form
				makes the current form hide.
				The wrapper animates its width and height to the 
				width and height of the new current form.
				After the animation, the new form is shown
				*/
				$linkform.bind('click',function(e){
					var $link	= $(this);
					var target	= $link.attr('rel');
					$currentForm.fadeOut(400,function(){
						//remove class active from current form
						$currentForm.removeClass('active');
						//new current form
						$currentForm= $form_wrapper.children('form.'+target);
						//animate the wrapper
						$form_wrapper.stop()
									 .animate({
										width	: $currentForm.data('width') + 'px',
										height	: $currentForm.data('height') + 'px'
									 },500,function(){
										//new form gets class active
										$currentForm.addClass('active');
										//show the new form
										$currentForm.fadeIn(400);
									 });
					});
					e.preventDefault();
				});
				
				function setWrapperWidth(){
					$form_wrapper.css({
						width	: $currentForm.data('width') + 'px',
						height	: $currentForm.data('height') + 'px'
					});
				}
				
				/*
				for the demo we disabled the submit buttons
				if you submit the form, you need to check the 
				which form was submited, and give the class active 
				to the form you want to show
				*/
				$form_wrapper.find('input[type="submit"]')
							 .click(function(e){
								e.preventDefault();
							 });	
			});
        </script>
    </body>
</html>