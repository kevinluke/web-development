<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Visual Admin Dashboard - Preferences</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
        <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>        <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/bootstrap-filestyle.min.js"/>"></script>  <!-- http://markusslima.github.io/bootstrap-filestyle/ -->
    <script type="text/javascript" src="<c:url value="resources/js/templatemo-script.js"/>"></script>  
    <link href="<c:url value='http://fonts.useso.com/css?family=Open+Sans:400,300,400italic,700'/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="resources/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/css/templatemo-style.css"/>" rel="stylesheet">
    <script>
    	function dyn(){
    		var num = document.getElementById("num").value*3;
    		if(num==0||isNaN(num)||num>15){
    			alert("Please enter the right number!");
    		}
    		else{
    			var htm = " <form modelAttribute='vehicle' name='addForm' action='createvehicle.htm' class='templatemo-login-form' method='post' enctype='multipart/form-data'>";
    			for(var i=0;i<num;i+=3){
    				var ii = i+15;
    				var a = i+1;
    				var b = i+2;
    				var iii = a+15;
    	    		htm+="<div class='row form-group' >";
    	    		htm+="<div class='col-lg-6 col-md-6 form-group'>";
    	    		htm+="<label id="+ii+">Vehicle's Name</label>";
    	    		htm+="<input type='text' class='form-control' name='"+i+"' id='"+i+"' onkeyup='check("+i+")'/></div>";	
    	    		
    	    		htm+="<div class='col-lg-6 col-md-6 form-group'><label id="+iii+">Price</label><input type='text' class='form-control' name='"+a+"' id='"+a+"' onkeyup='check("+i+")'></div>";
    	    		htm+="<div class='col-lg-6 col-md-6 form-group'><label class='control-label templatemo-block'>Type</label><select name='"+b+"' id='"+b+"' class='form-control'><option value='T'>T</option><option value='Bus'>Bus</option></select></div></div>";
    	    	}
    			htm+="<input type='hidden' name='number' value='"+num+"'/>";
    			htm+="<div class='form-group text-right'>";
    			htm+="<button type='submit' class='templatemo-blue-button'>Submit Change</button></div>";
    			htm+="</form>";
    			

    			document.getElementById("dynamic").innerHTML = htm;
    		}
    		
    	}
    </script>
	<script>
		function check(i){
			var a = i+1;
			var b = i+2;
			var vehiclename = document.getElementById(i).value;
			var price = document.getElementById(a).value;
			var index = document.getElementById(b).selectedIndex;
        	var type = document.getElementById(b).options[index].value;
			var ii = i+15;
			var iii = a+15;
			if(vehiclename == ""){

				document.getElementById(ii).innerHTML="Vehicle's Name can NOT be Empty!";
        		document.getElementById(ii).style.color="red";
			
        		
			}
			else{
        	var xmlhttp;
            if (window.XMLHttpRequest){
                xmlhttp=new XMLHttpRequest();
            }
            else{
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                	if(xmlhttp.responseText=="true"){
                		document.getElementById(ii).innerHTML="Vehicle's Name";
                		document.getElementById(ii).style.color="green";
                	}
                	else{
                		document.getElementById(ii).innerHTML="Vehicle's Name Existed!";
                		document.getElementById(ii).style.color="red";
                	}
                    
                }
              }
            xmlhttp.open("GET","checkvehicle.htm?vehiclename="+vehiclename,true);
            xmlhttp.send();
			}
			if(price==0||isNaN(price)){
    			document.getElementById(iii).innerHTML="Price can MUST be Float and NOT Empty!";
    		
    			document.getElementById(iii).style.color="red";
    		}
			else{
				document.getElementById(iii).innerHTML="Price";
	    		
    			document.getElementById(iii).style.color="green";
			}
		}
	</script>
    
</head>
<body>
<div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>Hello! Administrator</h1>
        </header>
        <div class="profile-photo-container">
          <img src="<c:url value="resources/images/${Math.floor(Math.random()*10+1) }.jpg" />" alt="Profile Photo" class="img-responsive">
          <div class="profile-photo-overlay"></div>
        </div>
        <!-- Search box -->
        
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
          </div>
        <nav class="templatemo-left-nav">
          <ul>
<!--             <li><a href="index.html"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-bar-chart fa-fw"></i>Charts</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-database fa-fw"></i>Manage Recharges</a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker fa-fw"></i>Maps</a></li> -->
            
            <li><a href="#" class="active"><i class="fa fa-sliders fa-fw"></i>Manage Transports</a></li>
            <li><a href="index"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>
        </nav>
      </div>
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a >View Transports</a></li>
                <li><a >Details</a></li>
                <li><a  class="active">Add Vehicles</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="templatemo-content-container">
          <div class="templatemo-content-widget white-bg">
          <h2 class="margin-bottom-10">Add Vehicles</h2>
            <p>Please enter the number of vehicles you want to add. (Maximum is 5)</p>
            <div class="row form-group" >
              
                        <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="num">Number of vehicles</label>
                    <input type="text" class="form-control" id="num" onkeyup="dyn()"/>
                </div> 
              </div>
			<div class="row form-group">
              <div class="col-lg-12 form-group" id="dynamic">
              </div>
              </div>
          </div>
          </div>
          
      <footer class="text-right">
            <p>Copyright &copy; Jianxing Lu
            | Final Project</p>
          </footer>
        </div>
	</div>
</body>
</html>