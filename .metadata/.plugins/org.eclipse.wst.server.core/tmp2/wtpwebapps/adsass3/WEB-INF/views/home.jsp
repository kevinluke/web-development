<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
<meta charset="utf-8">
    <title>Smoothie Charts: Ten Minute Tutorial</title><script async src="http://c.cnzz.com/core.php"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="skeleton/base.css">
    <link rel="stylesheet" href="skeleton/skeleton.css">
    <link rel="stylesheet" href="skeleton/layout.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="smoothie-website.css">
	  <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <script type="text/javascript" src="https://raw.githubusercontent.com/joewalnes/smoothie/master/smoothie.js"></script>
	<link href="<c:url value="recourses/js/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" media="screen">
    <link href="<c:url value="recourses/js/css/bootstrap-datetimepicker.min.css"/>" rel="stylesheet" media="screen">
	<script type="text/javascript" src="<c:url value="recourses/js/jquery/jquery-1.8.3.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="recourses/js/bootstrap/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="recourses/js/js/bootstrap-datetimepicker.js"/>"></script>
	<script type="text/javascript" src="<c:url value="recourses/js/js/bootstrap-datetimepicker.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="recourses/js/js/locales/bootstrap-datetimepicker.fr.js"/>"></script>
	<title>Home</title>
	
</head>
<body>
<h1>
	Hello world!  
</h1>
<div class="container">
    <form action="" class="form-horizontal">
        <fieldset>
            <legend>Test</legend>
            <div class="control-group">
                <label class="control-label">DateTime Picking</label>
                <div class="controls input-append date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                    <input size="16" type="text" value="" readonly>
                    <span class="add-on"><i class="icon-remove"></i></span>
					<span class="add-on"><i class="icon-th"></i></span>
                </div>
				<input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
			<div class="control-group">
                <label class="control-label">Date Picking</label>
                <div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input size="16" type="text" value="" readonly>
                    <span class="add-on"><i class="icon-remove"></i></span>
					<span class="add-on"><i class="icon-th"></i></span>
                </div>
				<input type="hidden" id="dtp_input2" value="" /><br/>
            </div>
			<div class="control-group">
                <label class="control-label">Time Picking</label>
                <div class="controls input-append date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
                    <input size="16" type="text" value="" readonly>
                    <span class="add-on"><i class="icon-remove"></i></span>
					<span class="add-on"><i class="icon-th"></i></span>
                </div>
				<input type="hidden" id="dtp_input3" value="" /><br/>
            </div>
        </fieldset>
    </form>
</div>

<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });
	$('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	$('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });
</script>


<input name="key" id="keyword" type="text"/>
<input type="button" name="button" id="button" value="Submit" onclick="confirm()"/>
<script type="text/javascript">
	function confirm()
	{
		var key = document.getElementById("keyword").value;

		var xmlhttp;
        if (window.XMLHttpRequest){
            xmlhttp=new XMLHttpRequest();
        }
        else{
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200){
            	test(xmlhttp.responseText);
            }
          }
        xmlhttp.open("GET","test.htm?keyword="+key,true);
        xmlhttp.send();
	}
	</script>
<script type="text/javascript">
	function test(res){
		alert(res);
		var reses = res.split('~!@');
		for(var i=0;i<reses.length;i++){
			alert(reses[i]);
		}
	}
	</script>
<div>
	<input id="ta" ></input>
</div>
<script>





<%-- <h2>test 1</h2>
<div class="holder"><canvas id="mycanvas3" width="400" height="100"></canvas></div>
        <script type="text/javascript">
          (function() {
            var smoothie = new SmoothieChart();
            smoothie.streamTo(document.getElementById("mycanvas3"));
          })();
        </script>
<P>  The time on the server is ${serverTime}. </P>
<h2>test 2</h2>
<div class="holder"><canvas id="mycanvas4" width="400" height="100"></canvas></div>
        <script type="text/javascript">
          (function() {
            var line1 = new TimeSeries();
            var line2 = new TimeSeries();
            setInterval(function() {
              line1.append(new Date().getTime(), Math.random());
              line2.append(new Date().getTime(), Math.random());
            }, 1000);

            var smoothie = new SmoothieChart();
            smoothie.addTimeSeries(line1);
            smoothie.addTimeSeries(line2);
            smoothie.streamTo(document.getElementById("mycanvas4"),1000);
          })();
        </script>
<h2>test 3</h2>
<div class="holder"><canvas id="mycanvas5" width="400" height="100"></canvas></div>
        <script type="text/javascript">
          (function() {
            var line1 = new TimeSeries();
            setInterval(function() {
              line1.append(new Date().getTime(), Math.random()*5);
            }, 1000);

            var smoothie = new SmoothieChart({
                grid: { strokeStyle: 'rgb(125, 0, 0)', fillStyle: 'rgb(60, 0, 0)', lineWidth: 1, millisPerLine: 250, verticalSections: 6 },
                labels: { fillStyle:'rgb(255, 255, 0)' }
            });
            smoothie.addTimeSeries(line1, { strokeStyle: 'rgb(0, 255, 0)', fillStyle: 'rgba(0, 255, 0, 0.4)', lineWidth: 3 });
            smoothie.streamTo(document.getElementById("mycanvas5"));
          })();
        </script> --%>
</body>
</html>
