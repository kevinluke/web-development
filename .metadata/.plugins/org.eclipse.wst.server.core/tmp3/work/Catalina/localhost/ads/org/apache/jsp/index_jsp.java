/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/8.0.30
 * Generated at: 2016-08-05 19:56:21 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(2);
    _jspx_dependants.put("/WEB-INF/lib/jstl-1.2.jar", Long.valueOf(1458677611643L));
    _jspx_dependants.put("jar:file:/D:/sts-bundle/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp3/wtpwebapps/ads/WEB-INF/lib/jstl-1.2.jar!/META-INF/c.tld", Long.valueOf(1153399482000L));
  }

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

final java.lang.String _jspx_method = request.getMethod();
if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
return;
}

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">\n");
      out.write("  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>\n");
      out.write("  <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\n");
      out.write("<title>Insert title here</title>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("<!-- <div class=\"container-fluid\">\n");
      out.write("\t\t<ul class=\"nav nav-tabs\">\n");
      out.write("\t\t  <li class=\"active\"><a href=\"#\">Home</a></li>\n");
      out.write("\t\t  <li><a href=\"https://public.tableau.com/views/myboston/2?:embed=y&:display_count=yes&:showTabs=y\">Visualization</a></li>\n");
      out.write("\t\t  <li><a href=\"/views/macro\">Marco-Analysis</a></li>\n");
      out.write("\t\t  <li><a href=\"#\">Micro-Analysis</a></li>\n");
      out.write("\t\t</ul>\n");
      out.write("</div> -->\n");
      out.write("<p><b></b><font size=\"5\" face=\"verdana\" color=\"green\">Welcome to the home page.</font></b></p>\n");
      out.write("<!-- <b>We Are Crazy Analyst.</b> -->\n");
      out.write("\n");
      out.write("<!-- <form action=\"addInput.htm\" method=\"post\">\n");
      out.write("  <button type=\"submit\" name=\"test\" value=\"your_value\" class=\"btn-link\">Go</button>\n");
      out.write("</form> -->\n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write("\n");
      out.write("\t<div class=\"container-fluid\">\n");
      out.write("\t\t<ul class=\"nav nav-tabs\">\n");
      out.write("\t\t  <li class=\"active\"><a href=\"#\">Home</a></li>\n");
      out.write("\t\t  <li><a href=\"https://public.tableau.com/views/myboston/2?:embed=y&:display_count=yes&:showTabs=y\">Visualization</a></li>\n");
      out.write("\t\t  <li><a href=\"#\">Marco-Analysis</a></li>\n");
      out.write("\t\t  <li><a href=\"#\">Micro-Analysis</a></li>\n");
      out.write("\t\t</ul>\n");
      out.write("\t\t<div class=\"row-fluid\">\n");
      out.write("\t\t\t<div class=\"span12\">\n");
      out.write("\t\t\t\t<fieldset>\n");
      out.write("\t\t\t\t<legend>We Are Crazy Analyst</legend>\n");
      out.write("\t\t\t\t<form class=\"form-horizontal\" method=\"post\" action='addInput.htm' name=\"inputForm\" id=\"inputForm\">\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Date</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"text\" name=\"date\" id=\"date\" title=\"Date\" value=\"\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Category</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t\t<select name=\"category_dd\" id=\"category_dd\">\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"BCYF\">BCYF</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"BPD\">BPD</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"BPL\">BPL</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"DND\">DND</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"PARKS\">PARKS</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"PROP\">PROP</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"PWD\">PWD</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"SCH\">SCH</option>\n");
      out.write("\t\t\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t\t\t\t<!-- <input type=\"text\" name=\"category\" id=\"category\" title=\"Category\" value=\"\"> -->\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Building</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t<select name=\"building_dd\" id=\"building_dd\">\n");
      out.write("\t\t\t\t\t\t\t \n");
      out.write("\t\t\t\t\t\t\t  <option value=\"AGASSIZ\">AGASSIZ</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"ArtsJan\">ArtsJan</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Blackstone Jan\">Blackstone Jan</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"BlackstoneJul\">BlackstoneJul</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"BLDG\">BLDG</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"BOSTN LATIN ACDMY\">BOSTN LATIN ACDMY</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"BOSTON LATIN\">BOSTON LATIN</option>\t\t\t\t\t\t\t  \n");
      out.write("\t\t\t\t\t\t\t  <option value=\"CAMPBELL\">CAMPBELL</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"CENTRAL KITCHEN\">CENTRAL KITCHEN</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"CHARLESTOWN HIGH\">CHARLESTOWN HIGH</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"CLEVELAND NEW\">CLEVELAND NEW</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"CLEVELAND OLD\">CLEVELAND OLD</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"COPLEY SQUARE\">COPLEY SQUARE</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"COURT ST\">COURT ST</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"CURLEY CMTY CTR\">CURLEY CMTY CTR</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"DEVER\">DEVER</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"DISTRICT 1 AREA A\">DISTRICT 1 AREA A</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"DORCHESTER HIGH\">DORCHESTER HIGH</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"DUDLEY SQ\">DUDLEY SQ</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"EEC\">EEC</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"EECEASTBOSTON\">EECEASTBOSTON</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"ENGLISH HIGH\">ENGLISH HIGH</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"HARVARD ST\">HARVARD ST</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"HEADQUARTERS\">HEADQUARTERS</option>\n");
      out.write("\t\t\t  \n");
      out.write("\t\t\t\t\t\t\t  <option value=\"HEMENWAY\">HEMENWAY</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"HYDE PARK HIGH\">HYDE PARK HIGH</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"HYDE PARK2014\">HYDE PARK2014</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"JACKSON MANN\">JACKSON MANN</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"JAMES F CONDON\">JAMES F CONDON</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"JAMES W HENNIGAN\">JAMES W HENNIGAN</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"JERIMAH BURKE\">JERIMAH BURKE</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"JOSEPH LEE\">JOSEPH LEE</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"JOSIAH QUINCY\">JOSIAH QUINCY</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"LILLA FREDRICK\">LILLA FREDRICK</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"MATTAHUNT\">MATTAHUNT</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"MATTAPAN\">MATTAPAN</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"MGMT\">MGMT</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"MGMT_1481TREMONT_TOBIN COMMUNITY CENTER\">MGMT_1481TREMONT_TOBIN COMMUNITY CENTER</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"MILDRED SCHOOL\">MILDRED SCHOOL</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"MISSION HIGH\">MISSION HIGH</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"MURPHY\">MURPHY</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"NATHAN HALE\">NATHAN HALE</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"ORCHARDGARDENS\">ORCHARDGARDENS</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"PARIS ST COMM CT\">PARIS ST COMM CT</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"RICHARD J MURPHY\">RICHARD J MURPHY</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"STATION 11 AREA C\">STATION 11 AREA C</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"STRATTON\">STRATTON</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"TROTTER\">TROTTER</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"W H OHRENBERGER\">W H OHRENBERGER</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"WEST ROXBURY\">WEST ROXBURY</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"WILLIAM H TAFT\">WILLIAM H TAFT</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"WILLIAM RUSSELL\">WILLIAM RUSSELL</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"WM H KENT\">WM H KENT</option>\n");
      out.write("\t\t\t\t\t\t\t  \n");
      out.write("\t\t\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Temperature</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"text\" name=\"temperature\" id=\"temperature\" title=\"Temperature\" value=\"\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Humidity</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"text\" name=\"humidity\" id=\"humidity\" title=\"Humidity\" value=\"\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Events</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t<select name=\"events_dd\" id=\"events_dd\">\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Fog\">Fog</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Fog-Rain\">Fog-Rain</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Fog-Rain-Snow\">Fog-Rain-Snow</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Fog-Rain-Thunderstorm\">Fog-Rain-Thunderstorm</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Fog-Snow\">Fog-Snow</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Rain\">Rain</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Rain-Snow\">Rain-Snow</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Rain-Thunderstorm\">Rain-Thunderstorm</option>\n");
      out.write("\t\t\t\t\t\t\t  <option value=\"Snow\">Snow</option>\n");
      out.write("\t\t\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t\t\t\t<!-- <input type=\"text\" name=\"events\" id=\"events\" title=\"Events\" value=\"\"> -->\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Working Day</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"text\" name=\"workingDay\" id=\"workingDay\" title=\"Working Day\" value=\"\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"control-group\">\n");
      out.write("\t\t\t\t\t\t<label class=\"control-label\">Holiday</label>\n");
      out.write("\t\t\t\t\t\t<div class=\"controls\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"text\" name=\"holiday\" id=\"holiday\" title=\"Holiday\" value=\"\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"form-actions\">\n");
      out.write("\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-success\">Submit</button>\n");
      out.write("\t\t\t\t\t\t<button type=\"button\" class=\"btn\">Cancel</button>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</form>\n");
      out.write("\t\t\t\t</fieldset>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\t\t\n");
      out.write("\n");
      out.write(" \n");
      out.write("\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
