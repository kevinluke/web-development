<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
    <head>
        <title>Animated Form Switching with jQuery</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
		<link rel="shortcut icon" href="<c:url value="resources/favicon.ico"/> "/>
        <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/style.css"/>" />
		<script src="<c:url value="resources/js/cufon-yui.js" />"></script>
		<script src="<c:url value="resources/js/ChunkFive_400.font.js" /> "></script>
		<script type="text/javascript">
			Cufon.replace('h1',{ textShadow: '1px 1px #fff'});
			Cufon.replace('h2',{ textShadow: '1px 1px #fff'});
			Cufon.replace('h3',{ textShadow: '1px 1px #000'});
			Cufon.replace('.back');
		</script>
                <script>
                    function check(){
                        var pwd = document.getElementById("pwd").value;
                        var repwd = document.getElementById("repwd").value;
                        if(pwd == repwd){
                            document.getElementById("reenter").style.color="green";
                        }
                        else{
                            document.getElementById("reenter").style.color="red";
                        }
                    }
                </script>
                <script>
                    function save(){
                        var form = document.getElementById("register");
                        var allinput = form.getElementsByTagName("input");
                        for(var i=0;i< allinput.length;i++){
                            if(allinput[i].value == ""){
                                alert("Please enter all the required information!");
                                return false;
                                
                            }
                        }
                        var pwd = document.getElementById("pwd").value;
                        var repwd = document.getElementById("repwd").value;
                        if(pwd == repwd){
                            alert("Success! Please login!");
                            return true;
                        }
                        else{
                            alert("Please check your password!");
                            return false;
                        }
                    }
                </script>
                <script>
                   function usersubmit(){
                       document.forms["userlogin"].submit();
                   }
		</script>
                <script>
                   function driversubmit(){
                       document.forms["driverlogin"].submit();
                   }
		</script>
		<script>
                   function register(){
                       document.forms["registertype"].submit();
                   }
		</script>
		   <script language="JavaScript" type="text/javascript">
     function window_onload(error) {
    	 if(error == 1){

    	       alert("Are u sure about that??");
    	 }

     }
    </script>
    </head>
    <body onload="window_onload('${requestScope.usererror}');">
    
		<div class="wrapper">
			<h1>Smart-Transportation Login System</h1>
			<h2>click the <span>orange links</span> to switch login role</h2>
			<div class="content">
				<div id="form_wrapper" class="form_wrapper">
                     <form action="login.htm" method="post" class="register" name="registertype">
                                        <h3>Register</h3>
                                        <div class="column">
							<div>
								<label>Type:</label>
								<select name="type" id="type">
                                                                    <option selected="true" label="User" value="User"></option>
                                                                    <option label="Driver" value="Driver"></option>
                                                                </select>
							</div>
                                        </div>
                                        <div class="bottom">		
                                            <input type="hidden" name="action" value="type"/>
                                            <input type="submit" value="Register" onclick="register()"/>
                                                <a href="index.html" rel="login" class="linkform">You have an account already? Log in here</a>
                                                <div class="clear"></div>
                                        </div>
                                    </form>
                                    <form:form class="login active" action="login.htm" method="post" name="userlogin">
						<h3>Login as User</h3>
						<div>
							<label>Username:</label>
							<input type="text" name="username"/>
							<span class="error">This is an error</span>
						</div>
						<div>
							<label>Password:</label>
							<input type="password" name="userpwd"/>
							<span class="error">This is an error</span>
						</div>
						<div class="bottom">
							<div class="remember"><input type="checkbox" /><span>Keep me logged in</span></div>
                                                        <input type="hidden" name="action" value="user">
                                                            <input type="submit" value="Login" onclick="usersubmit()"/>
                                                        <a href="forgot_password.html" rel="forgot_password" class="linkform">Wanna login as Driver?</a>
							<a href="register.html" rel="register" class="linkform">You don't have an account yet? Register here</a>
							<div class="clear"></div>
						</div>
					</form:form>
					<form class="forgot_password" action="login.htm" method="post" name="driverlogin">
						<h3>Login as Driver</h3>
						<div>
							<label>Username:</label>
							<input type="text" name="taxiname"/>
							<span class="error">This is an error</span>
						</div>
						<div>
							<label>Password:</label>
							<input type="password" name="taxipwd"/>
							<span class="error">This is an error</span>
						</div>
						<div class="bottom">
							<div class="remember"><input type="checkbox" /><span>Keep me logged in</span></div>
                                                        <input type="hidden" name="action" value="driver">
                                                            <input type="submit" value="Login" onclick="driversubmit()"></input>
                                                        <a href="index.html" rel="login" class="linkform">Wanna login as User?</a>
							<a href="register.html" rel="register" class="linkform">You don't have an account yet? Register here</a>
							<div class="clear"></div>
						</div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
			<a class="back" href="http://localhost:8080/security/adminpage.htm">Administrator Login</a>
		</div>
		

		<!-- The JavaScript -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript">
			$(function() {
					//the form wrapper (includes all forms)
				var $form_wrapper	= $('#form_wrapper'),
					//the current form is the one with class active
					$currentForm	= $form_wrapper.children('form.active'),
					//the change form links
					$linkform		= $form_wrapper.find('.linkform');
						
				//get width and height of each form and store them for later						
				$form_wrapper.children('form').each(function(i){
					var $theForm	= $(this);
					//solve the inline display none problem when using fadeIn fadeOut
					if(!$theForm.hasClass('active'))
						$theForm.hide();
					$theForm.data({
						width	: $theForm.width(),
						height	: $theForm.height()
					});
				});
				
				//set width and height of wrapper (same of current form)
				setWrapperWidth();
				
				/*
				clicking a link (change form event) in the form
				makes the current form hide.
				The wrapper animates its width and height to the 
				width and height of the new current form.
				After the animation, the new form is shown
				*/
				$linkform.bind('click',function(e){
					var $link	= $(this);
					var target	= $link.attr('rel');
					$currentForm.fadeOut(400,function(){
						//remove class active from current form
						$currentForm.removeClass('active');
						//new current form
						$currentForm= $form_wrapper.children('form.'+target);
						//animate the wrapper
						$form_wrapper.stop()
									 .animate({
										width	: $currentForm.data('width') + 'px',
										height	: $currentForm.data('height') + 'px'
									 },500,function(){
										//new form gets class active
										$currentForm.addClass('active');
										//show the new form
										$currentForm.fadeIn(400);
									 });
					});
					e.preventDefault();
				});
				
				function setWrapperWidth(){
					$form_wrapper.css({
						width	: $currentForm.data('width') + 'px',
						height	: $currentForm.data('height') + 'px'
					});
				}
				
				/*
				for the demo we disabled the submit buttons
				if you submit the form, you need to check the 
				which form was submited, and give the class active 
				to the form you want to show
				*/
				$form_wrapper.find('input[type="submit"]')
							 .click(function(e){
								e.preventDefault();
							 });	
			});
        </script>
    </body>
</html>