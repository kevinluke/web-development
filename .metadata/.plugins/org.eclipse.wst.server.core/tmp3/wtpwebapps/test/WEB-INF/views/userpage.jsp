<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Visual Admin Dashboard - Preferences</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
        <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>        <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/bootstrap-filestyle.min.js"/>"></script>  <!-- http://markusslima.github.io/bootstrap-filestyle/ -->
    <script type="text/javascript" src="<c:url value="resources/js/templatemo-script.js"/>"></script>  
    <link href="<c:url value='http://fonts.useso.com/css?family=Open+Sans:400,300,400italic,700'/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="resources/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/css/templatemo-style.css"/>" rel="stylesheet">

	<script>
                function search(){
                	var from = document.getElementById("from").value;
                	var to = document.getElementById("to").value;
                	var radio = document.getElementsByName("radio");
                	var way;
                	for(var i=0; i<radio.length;i++){
                		if(radio[i].checked){
                			way = radio[i].value;
                		}
                	}
                	if(from == to){
                		alert("Are you sure you wannna do this? XD");
                	}
                	else{
                		if(way == "Taxi"){
                    		document.forms["getPath"].submit();
                    	}
                    	
                    	else{
                    		var xmlhttp;
                            if (window.XMLHttpRequest){
                                xmlhttp=new XMLHttpRequest();
                            }
                            else{
                                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            xmlhttp.onreadystatechange=function(){
                                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                                	var str = xmlhttp.responseText;
                                	alert(str);
                                	var strs = str.split(":");
                                	if(strs[0] == "1"){
                                		var one = strs[1].split(" ");
                                		if(isNaN(one[0])){
                                			var color = one[0].toLowerCase();
                                			var htm = "<div style='color:"+color+"' class='col-lg-6 col-md-6 form-group'><label for='inputUsername'>First Line</label><input type='text' class='form-control' id='inputUsername' value='"+strs[1]+"'></div>";
                                			document.getElementById("dynamic").innerHTML=htm;
                                		}
                                		else{
                                			var htm = "<div class='col-lg-6 col-md-6 form-group'><label for='inputUsername'>First Line</label><input type='text' class='form-control' id='inputUsername' value='"+strs[1]+"'></div>";
                                			htm +="";
                                			document.getElementById("dynamic").innerHTML=htm;
                                		}	
                                		
                                		var hhh = "<div class='col-lg-6 col-md-6 form-group'><label for='price'>Total Price</label><input name='price' type='text' class='form-control' id='price' value='"+Number(strs[2])+"'></div>";
                                		hhh+="<div class='col-lg-6 col-md-6 form-group'><label class='control-label templatemo-block'>Payment</label><select name='pay' class='form-control' id='pay'><c:forEach var='card' items='${requestScope.user.cards }'><option value='${card.cardId } to ${card.cardExpire }'>${card.cardId } to ${card.cardExpire }</option></c:forEach><option value='${requestScope.user.userBalance}'>${requestScope.user.userBalance}</option></select></div>";
                                		hhh+="<input type='hidden' value='${requestScope.user.userName}' name='username'>";
                                		hhh+="<div class='form-group text-right'><button type='button' class='templatemo-blue-button' onclick='checkout()'>Check out</button></div> ";
                                		document.getElementById("payment").innerHTML=hhh;
                                	}
                                	else if(strs[0] == "2"){
                                		var one = strs[1].split(" ");
                                		var two = strs[4].split(" ");
                                			var color = one[0].toLowerCase();
                                			var color2 = two[0].toLowerCase();
                                			var ht = "<div style='color:"+color+"' class='col-lg-6 col-md-6 form-group'><label for='inputUsername'>First Line</label><input type='text' class='form-control' id='inputUsername' value='"+strs[1]+"'></div><div class='col-lg-6 col-md-6 form-group'><label for='change'>Change Line</label><input type='text' class='form-control' id='change' value='"+strs[3]+"'></div><div style='color:"+color2+"' class='col-lg-6 col-md-6 form-group'><label for='second'>Second Line</label><input type='text' class='form-control' id='second' value='"+strs[4]+"'></div>";
                                			document.getElementById("dynamic").innerHTML=ht;     
                                			
                                			var total = Number(strs[2])+Number(strs[5]);
                                			var hhh = "<div class='col-lg-6 col-md-6 form-group'><label for='price'>Total Price</label><input name='price' type='text' class='form-control' id='price' value='"+total+"'></div>";
                                    		hhh+="<div class='col-lg-6 col-md-6 form-group'><label class='control-label templatemo-block'>Payment</label><select name='pay' class='form-control' id='pay'><c:forEach var='card' items='${requestScope.user.cards }'><option value='${card.cardId } to ${card.cardExpire }'>${card.cardId } to ${card.cardExpire }</option></c:forEach><option value='${requestScope.user.userBalance}'>${requestScope.user.userBalance}</option></select></div>";
                                    		hhh+="<input type='hidden' value='${requestScope.user.userName}' name='username'>";
                                    		hhh+="<div class='form-group text-right'><button type='button' class='templatemo-blue-button' onclick='checkout()'>Check out</button></div> ";
                                    		
                                    		document.getElementById("payment").innerHTML=hhh;
                                	}
                                	else if(strs[0] == "3"){
                                		var one = strs[1].split(" ");
                                		var two = strs[4].split(" ");
                                		var three = strs[7].split(" ");
                                		var color = one[0].toLowerCase();
                            			var color2 = two[0].toLowerCase();
                            			var color3 = three[0].toLowerCase();
                            			var ht = "<div style='color:"+color+"' class='col-lg-6 col-md-6 form-group'><label for='inputUsername'>First Line</label><input type='text' class='form-control' id='inputUsername' value='"+strs[1]+"'></div><div class='col-lg-6 col-md-6 form-group'><label for='change'>Change Line</label><input type='text' class='form-control' id='change' value='"+strs[3]+"'></div><div style='color:"+color2+"' class='col-lg-6 col-md-6 form-group'><label for='second'>Second Line</label><input type='text' class='form-control' id='second' value='"+strs[4]+"'></div><div class='col-lg-6 col-md-6 form-group'><label for='change2'>Change Line</label><input type='text' class='form-control' id='change2' value='"+strs[6]+"'></div><div style='color:"+color3+"' class='col-lg-6 col-md-6 form-group'><label for='third'>Third Line</label><input type='text' class='form-control' id='third' value='"+strs[7]+"'></div>";
                            			document.getElementById("dynamic").innerHTML=ht;  
                            			
                            			var total = Number(strs[2])+Number(strs[5])+Number(strs[8])
                            			var hhh = "<div class='col-lg-6 col-md-6 form-group'><label for='price'>Total Price</label><input name='price' type='text' class='form-control' id='price' value='"+total+"'></div>";
                                		hhh+="<div class='col-lg-6 col-md-6 form-group'><label class='control-label templatemo-block'>Payment</label><select name='pay' class='form-control' id='pay'><c:forEach var='card' items='${requestScope.user.cards }'><option value='${card.cardId } to ${card.cardExpire }'>${card.cardId } to ${card.cardExpire }</option></c:forEach><option value='${requestScope.user.userBalance}'>${requestScope.user.userBalance}</option></select></div>";
                                		hhh+="<input type='hidden' value='${requestScope.user.userName}' name='username'>";
                                		hhh+="<div class='form-group text-right'><button type='button' class='templatemo-blue-button' onclick='checkout()'>Check out</button></div> ";
                                		
                                		document.getElementById("payment").innerHTML=hhh;
                                	}
                                	else{
                                		alert("There is no suitable exchanging plan, please check the taxi option!");
                                	}
                                }
                              }
                            xmlhttp.open("GET","searchPath.htm?from="+from+"&to="+to,true);
                            xmlhttp.send();
                    	}
                	}
                	
                	
                }
                </script>

<script>
function checkout(){
	document.forms["checkoutForm"].submit();
}
</script>
  </head>
  <body>
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>Hi Smart ${requestScope.user.userFname} :)</h1>
        </header>
        <div class="profile-photo-container">
          <img src="<c:url value="resources/images/${Math.floor(Math.random()*10+1) }.jpg" />" alt="Profile Photo" class="img-responsive">
          <div class="profile-photo-overlay"></div>
        </div>
        <!-- Search box -->
<%--         <form class="templatemo-search-form" role="search">
          <div class="input-group">
              <button type="submit" class="fa fa-search"></button>
              <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
          </div>
        </form> --%>
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
          </div>
        <nav class="templatemo-left-nav">
          <ul>
<!--             <li><a href="index.html"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-bar-chart fa-fw"></i>Charts</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-database fa-fw"></i>Data Visualization</a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker fa-fw"></i>Maps</a></li>
            <li><a href="manage-users.html"><i class="fa fa-users fa-fw"></i>Manage Users</a></li> -->
            <li><a href="#" class="active"><i class="fa fa-sliders fa-fw"></i>Let's Go!!</a></li>
            <li><a href="havearest.htm?user=${requestScope.user }"><i class="fa fa-users fa-fw"></i>Have a Rest</a></li>
            <li><a href="index"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>
        </nav>
      </div>
      <!-- Main content -->
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a class="active">User panel</a></li>
                <li><a>Order Status</a></li>
                <li><a>Check Out</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="templatemo-content-container">
          <div class="templatemo-content-widget white-bg">
            <h2 class="margin-bottom-10">Begin a trip</h2>
            <p>Please select your starting point and destination.</p>
            <form name="getPath" action="getPath.htm" class="templatemo-login-form" method="post" enctype="multipart/form-data">
              <!-- <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="inputFirstName">First Name</label>
                    <input type="text" class="form-control" id="inputFirstName" placeholder="John">                  
                </div>
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="inputLastName">Last Name</label>
                    <input type="text" class="form-control" id="inputLastName" placeholder="Smith">                  
                </div> 
              </div>
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="inputUsername">Username</label>
                    <input type="text" class="form-control" id="inputUsername" placeholder="Admin">                  
                </div>
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="inputEmail">Email</label>
                    <input type="email" class="form-control" id="inputEmail" placeholder="admin@company.com">                  
                </div> 
              </div>
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="inputCurrentPassword">Current Password</label>
                    <input type="password" class="form-control highlight" id="inputCurrentPassword" placeholder="*********************">                  
                </div>                
              </div>
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="inputNewPassword">New Password</label>
                    <input type="password" class="form-control" id="inputNewPassword">
                </div>
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label for="inputConfirmNewPassword">Confirm New Password</label>
                    <input type="password" class="form-control" id="inputConfirmNewPassword">
                </div> 
              </div>
              <div class="row form-group">
                <div class="col-lg-12 has-success form-group">                  
                    <label class="control-label" for="inputWithSuccess">Input with success</label>
                    <input type="text" class="form-control" id="inputWithSuccess">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-lg-12 has-warning form-group">                  
                    <label class="control-label" for="inputWithWarning">Input with warning</label>
                    <input type="text" class="form-control" id="inputWithWarning">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-lg-12 has-error form-group">                  
                    <label class="control-label" for="inputWithError">Input with error</label>
                    <input type="text" class="form-control" id="inputWithError">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-lg-12 form-group">                   
                    <label class="control-label" for="inputNote">Note</label>
                    <textarea class="form-control" id="inputNote" rows="3"></textarea>
                </div>
              </div> -->
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group"> 
                  <label class="control-label templatemo-block">From</label>                 
                  <select name="from" class="form-control" id="from">
                  <c:forEach var="station" items="${requestScope.stationList }">
                    <option value="${station.stationName }">${station.stationName }</option>
                    </c:forEach>                   
                  </select>
                </div>
                <div class="col-lg-6 col-md-6 form-group">                  
                    <label class="control-label templatemo-block">To</label>                 
                  <select name="to" class="form-control" id="to">
                    <c:forEach var="station" items="${requestScope.stationList }">
                    <option value="${station.stationName }">${station.stationName }</option>
                    </c:forEach>                    
                  </select>
                </div> 
              </div>
              <div class="row form-group">
                <div class="col-lg-12 form-group">                   
                    <div class="margin-right-15 templatemo-inline-block">
                      <input type="radio" name="radio" id="r4" value="T" checked>
                      <label for="r4" class="font-weight-400"><span></span>By T</label>
                    </div>
                    <div class="margin-right-15 templatemo-inline-block">
                      <input type="radio" name="radio" id="r5" value="Bus">
                      <label for="r5" class="font-weight-400"><span></span>By Bus</label>
                    </div>
                    <div class="margin-right-15 templatemo-inline-block">
                      <input type="radio" name="radio" id="r6" value="Taxi">
                      <label for="r6" class="font-weight-400"><span></span>By Taxi</label>
                    </div>
                </div>
                <div class="row form-group">
              <div class="col-lg-12 form-group" id="dynamic">
              </div>
              </div>
              <input type="hidden" value="${requestScope.user }" name="user"/> 
                <div class="form-group text-right">
                <button type="button" class="templatemo-blue-button" onclick="search()">Search</button>
                <button type="button" class="templatemo-white-button" onclick="location='http://localhost:8080/security/getstatus.htm?user='+'${requestScope.user}'">Order Status</button>
              </div>   
              
              </form>
              <form name="checkoutForm" action="checkout.htm" class="templatemo-login-form" method="post" enctype="multipart/form-data">
            <div class="row form-group">
              <div class="col-lg-12 form-group" id="payment">
              
              </div>
              </div>
            </form>
              <%-- <form action="index.html" class="templatemo-login-form" method="post" enctype="multipart/form-data">
              <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group"> 
                  <label class="control-label templatemo-block">Multiple Selection Control</label>                 
                  <select multiple class="templatemo-multi-select form-control" style="overflow-y: scroll;">
                    <option value="">Charts</option>
                    <option value="">Graphs</option>
                    <option value="">Icons</option>
                    <option value="">Repsonsive</option>  
                    <option value="">HTML5</option>
                    <option value="">CSS3</option>
                    <option value="">jQuery</option>                    
                  </select>
                </div>
                <div class="col-lg-6 col-md-6 form-group">
                  <div>
                    <label class="control-label templatemo-block">Email Option</label> 
                    <div class="templatemo-block margin-bottom-5">
                      <input type="radio" name="emailOptions" id="r1" value="html" checked>
                      <label for="r1" class="font-weight-400"><span></span>HTML Format</label>
                    </div>
                    <div class="templatemo-block margin-bottom-5">
                      <input type="radio" name="emailOptions" id="r2" value="plain">
                      <label for="r2" class="font-weight-400"><span></span>Plain Text</label>
                    </div>
                    <div class="templatemo-block margin-bottom-5">
                      <input type="radio" name="emailOptions" id="r3" value="rich">
                      <label for="r3" class="font-weight-400"><span></span>Rich Text</label>
                    </div>                    
                  </div>                  
                </div> 
              </div>
              <div class="row form-group">
                <div class="col-lg-12 form-group">                   
                    <div class="margin-right-15 templatemo-inline-block">
                      <input type="checkbox" name="server" id="c3" value="" checked>
                      <label for="c3" class="font-weight-400"><span></span>Server Status</label>
                    </div>
                    <div class="margin-right-15 templatemo-inline-block">                      
                      <input type="checkbox" name="member" id="c4" value="">
                      <label for="c4" class="font-weight-400"><span></span>Member Status</label>
                    </div>
                    <div class="margin-right-15 templatemo-inline-block">
                      <input type="checkbox" name="expired" id="c5" value="">
                      <label for="c5" class="font-weight-400"><span></span>Expired Members</label>                      
                    </div>
                </div>
              </div>
              <div class="row form-group">
                <div class="col-lg-12">
                  <label class="control-label templatemo-block">File Input</label> 
                  <!-- <input type="file" name="fileToUpload" id="fileToUpload" class="margin-bottom-10"> -->
                  <input type="file" name="fileToUpload" id="fileToUpload" class="filestyle" data-buttonName="btn-primary" data-buttonBefore="true" data-icon="false">
                  <p>Maximum upload size is 5 MB.</p>                  
                </div>
              </div>
              <div class="form-group text-right">
                <button type="submit" class="templatemo-blue-button">Update</button>
                <button type="reset" class="templatemo-white-button">Reset</button>
              </div>                           
            </form> --%>
          </div>
         
        </div>
        <footer class="text-right">
            <p>Copyright &copy; Jianxing Lu
            | Final Project</p>
          </footer>
      </div>
    </div>

    <!-- JS -->
      <!-- Templatemo Script -->
  </body>
</html>
