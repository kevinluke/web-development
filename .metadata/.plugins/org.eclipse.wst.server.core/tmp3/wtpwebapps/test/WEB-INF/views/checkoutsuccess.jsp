<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Visual Admin Dashboard - Preferences</title>
    <meta name="description" content="">
    <meta name="author" content="templatemo">
        <script type="text/javascript" src="<c:url value="resources/js/jquery-1.11.2.min.js"/>"></script>        <!-- jQuery -->
    <script type="text/javascript" src="<c:url value="resources/js/bootstrap-filestyle.min.js"/>"></script>  <!-- http://markusslima.github.io/bootstrap-filestyle/ -->
    <script type="text/javascript" src="<c:url value="resources/js/templatemo-script.js"/>"></script>  
    <link href="<c:url value='http://fonts.useso.com/css?family=Open+Sans:400,300,400italic,700'/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="resources/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/css/templatemo-style.css"/>" rel="stylesheet">

	
  </head>
  <body>
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>Hi Smart ${requestScope.user.userFname} :)</h1>
        </header>
        <div class="profile-photo-container">
          <img src="<c:url value="resources/images/${Math.floor(Math.random()*10+1) }.jpg" />" alt="Profile Photo" class="img-responsive">
          <div class="profile-photo-overlay"></div>
        </div>
        <!-- Search box -->
<%--         <form class="templatemo-search-form" role="search">
          <div class="input-group">
              <button type="submit" class="fa fa-search"></button>
              <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
          </div>
        </form> --%>
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
          </div>
        <nav class="templatemo-left-nav">
          <ul>
<!--             <li><a href="index.html"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-bar-chart fa-fw"></i>Charts</a></li>
            <li><a href="data-visualization.html"><i class="fa fa-database fa-fw"></i>Data Visualization</a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker fa-fw"></i>Maps</a></li>
            <li><a href="manage-users.html"><i class="fa fa-users fa-fw"></i>Manage Users</a></li> -->
            <li><a href="#" class="active"><i class="fa fa-sliders fa-fw"></i>Let's Go!!</a></li>
            <li><a href="manage-users.html"><i class="fa fa-users fa-fw"></i>Have a Rest</a></li>
            <li><a href="index"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>
        </nav>
      </div>
      <!-- Main content -->
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a >User panel</a></li>
                <li><a>Add Card</a></li>
                <li><a class="active">Check Out</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="templatemo-content-container">
          <div class="templatemo-content-widget white-bg">
            <h2 class="margin-bottom-10">Begin a trip</h2>
            <p>Please select your starting point and destination.</p>
            <form name="getPath" action="getPath.htm" class="templatemo-login-form" method="post" enctype="multipart/form-data">
              
              <div class="row form-group">
                
            </form>
              
          </div>
         
        </div>
        <footer class="text-right">
            <p>Copyright &copy; Jianxing Lu
            | Final Project</p>
          </footer>
      </div>
    </div>

    <!-- JS -->
      <!-- Templatemo Script -->
  </body>
</html>
