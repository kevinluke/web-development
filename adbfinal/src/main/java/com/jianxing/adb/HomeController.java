package com.jianxing.adb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import com.jianxing.adb.MRs.Avg;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value="mapreduce.htm", method=RequestMethod.GET)
	@ResponseBody
	public String MapReduce(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception{
		Avg avg = new Avg();
		return avg.run();
	}
	
	@RequestMapping(value="functions.htm", method=RequestMethod.GET)
	@ResponseBody
	public String CheckStation(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		
		String func = hsr.getParameter("func");
		System.out.println(func);
		if(func.equals("sc")){
			System.out.println("sc");
			File file = new File("C:\\Users\\lu\\Desktop\\2\\part-r-00000");  
	//        FileSystem fs = FileSystem.get(URI.create(uri), conf);  
			BufferedReader br = new BufferedReader(new FileReader(file));  
	        String temp = null;  
	        StringBuffer sb = new StringBuffer();  
	        sb.append("sc;");
	        temp = br.readLine();  
	        while (temp != null) {  
	        	String[] s = temp.split("	");
	        	String[] str = s[0].split(";");
	        	String name = str[0], lat = str[1], lng = str[2];
	        	name = name.substring(1, name.length()-1);
	        	lat = lat.substring(1, lat.length()-1);
	        	lng = lng.substring(1, lng.length()-1);
	            sb.append(name+","+lat+","+lng+","+s[1]+";");  
	            temp = br.readLine();  
	        }  
	        return sb.toString();  
        }
		else if(func.equals("desc")){
			System.out.println("desc");
			Map<String, String> map = getTable("LastK");
			File file = new File("C:\\Users\\lu\\Desktop\\1\\part-r-00000");  
	//        FileSystem fs = FileSystem.get(URI.create(uri), conf);  
			BufferedReader br = new BufferedReader(new FileReader(file));  
	        String temp = null;  
	        StringBuffer sb = new StringBuffer();  
	        sb.append("desc;");
	        temp = br.readLine();  
	        int count = 0;
	        while (temp != null) {  
	        	if(count <4){
	        		count++;
	        		temp = br.readLine(); 
	        		continue;
	        	}
	        	
	        	String[] s = temp.split("	");
	        	String[] str = s[0].split(";");
	        	String name = str[0], lat = str[1], lng = str[2];
	        	name = name.substring(1, name.length()-1);
	        	lat = lat.substring(1, lat.length()-1);
	        	lng = lng.substring(1, lng.length()-1);
	            sb.append(name+","+lat+","+lng+","+s[1]+";");  
	            System.out.println(sb.toString());
	            temp = br.readLine();  
	        }  
	        return sb.toString();  
		}
		else{
			System.out.println("avg");
	        StringBuffer sb = new StringBuffer();  
	        sb.append("avg;");
			JSONParser parser = new JSONParser();
			String count = hsr.getParameter("count");
			String lat = hsr.getParameter("lat");
			float latf = Float.parseFloat(lat);
			String lavg = "";
			File file = new File("C:\\Users\\lu\\Desktop\\1\\part-r-00001");  
			BufferedReader br = new BufferedReader(new FileReader(file));  
			String str ="";
			Map<String, String> map = new HashMap<String, String>();
			str = br.readLine();
			while(str!=null){
				String[] ss = str.split("	");
				map.put(ss[0], ss[1]);
				str = br.readLine();
			}
			if(latf<=10 && latf>-10)
				lavg = map.get("N10-S30");
			else if(latf>10 && latf<=30)
				lavg = map.get("N10-N30");
			else if(latf>30 && latf<=60)
				lavg = map.get("N30-N60");
			else if(latf>60 && latf<=90)
				lavg = map.get("N60-N90");
			else if(latf>-30 && latf<=-10)
				lavg = map.get("S10-S30");
			else if(latf>-60 && latf<=-30)
				lavg = map.get("S30-S60");
			else 
				lavg = "No Data";
        	
			String lng = hsr.getParameter("lng");
			lng = lng.trim();
			String temp = HttpRequest("http://api.openweathermap.org/data/2.5/find?lat="+lat+"&lon="+lng+"&cnt="+count+"&APPID=420b33ea2a495b21a35d6970308cecb5");
	    	try {
		        	Object obj = parser.parse(temp);
		        	JSONObject jsonObject = (JSONObject) obj;

		        	JSONArray jsonObject1 = (JSONArray) jsonObject.get("list");
		        	Iterator<JSONObject> iterator = jsonObject1.iterator();
		        	float sum =0;
		        	while(iterator.hasNext()){
		        		JSONObject jo = (JSONObject)iterator.next();
		        		JSONObject jsonObject2 = (JSONObject)jo.get("main");
		        		String tem =  String.valueOf(jsonObject2.get("temp"));
		        		sum+=Float.parseFloat(tem);
		        	}
		        	sb.append(sum/Integer.parseInt(count));   	

	    	} catch (ParseException e) {
	    		e.printStackTrace();
	    	}
	    	sb.append("     (Average Temp at that Lat is "+lavg+")");
	        return sb.toString();  
		}
		
		
	}
	@RequestMapping(value="test.htm", method=RequestMethod.GET)
	@ResponseBody
	public int sdf(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
		System.out.println("sdfsdf");
		InputStream is = new FileInputStream("C:\\Users\\lu\\Desktop\\weather_14.json");  
		Reader rd = new InputStreamReader(is, "utf-8"); 
		JSONObject json = (JSONObject) new JSONParser().parse(rd);
		JSONParser jsonParser = new JSONParser();
		JSONArray lang= (JSONArray) json.get("city");
		return lang.size();
	}
	
	
	
	
	public static String HttpRequest(String requestUrl) {
        StringBuffer sb = new StringBuffer();
        InputStream ips = getInputStream(requestUrl);
        InputStreamReader isreader = null;
        try {
            isreader = new InputStreamReader(ips, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(isreader);
        String temp = null;
        try {
            while ((temp = bufferedReader.readLine()) != null) {
                sb.append(temp);
            }
            bufferedReader.close();
            isreader.close();
            ips.close();
            ips = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
    private static InputStream getInputStream(String requestUrl) {
        URL url = null;
        HttpURLConnection conn = null;
        InputStream in = null;
        try {
            url = new URL(requestUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.connect();
 
            in = conn.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return in;
    }
    
    private static Map<String, String> getTable(String tname) throws IOException, Exception{
		// Instantiating Configuration class
	      Configuration config = HBaseConfiguration.create();
	      //Creating Table Object
	      HTable table = new HTable(config, Bytes.toBytes(tname));
	      try {
	       //Scanning all the data
	       Scan scan = new Scan();
	       ResultScanner rss = table.getScanner(scan);
	       Map<String, String> map = new HashMap<String, String>();
	       for(Result r:rss){
		       
		       
		       for(KeyValue kv:r.raw()){
		    	   map.put(new String(r.getRow()),  new String(kv.getValue(),"utf-8"));
		       }
	       }
	       rss.close();
	       return map;
	      } catch (IOException e) {
	    	  e.printStackTrace();
	      }  
	      return null;
	   }

}
