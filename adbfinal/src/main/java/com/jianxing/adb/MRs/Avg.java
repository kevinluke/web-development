package com.jianxing.adb.MRs;


import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


public class Avg {
	public Avg(){
			
		}
	public class UserMapper extends Mapper<Object, Text, Text, FloatWritable>{
        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        	String[] lines = value.toString().split(",");
        	
        	String name = lines[0], lat = lines[1],lng = lines[2], tem = lines[3];
        	Pattern pattern = Pattern.compile("(.)*[\\d]+(.)*");
            if(!lat.equals("")&&!lng.equals("")&&!tem.equals("")&&pattern.matcher(lat).matches()){
//	            name = name.substring(1, name.length()-1);
	            lat = lat.substring(1, lat.length()-1);
	            lng = lng.substring(1, lng.length()-1);
	            tem = tem.substring(1, tem.length()-1);
	            
	            float latf = Float.parseFloat(lat);
	            if(latf <= 10 && latf>-10){
	            	context.write(new Text("N10-S10"), new FloatWritable(Float.parseFloat(tem)));
	            }
	            else if(latf >10 && latf<=30)
	            	context.write(new Text("N10-N30"), new FloatWritable(Float.parseFloat(tem)));
	            else if(latf >30 && latf<=60)
	            	context.write(new Text("N30-N60"), new FloatWritable(Float.parseFloat(tem)));
	            else if(latf >60 && latf<=90)
	            	context.write(new Text("N60-N90"), new FloatWritable(Float.parseFloat(tem)));
	            else if(latf >-30 && latf<=-10)
	            	context.write(new Text("S10-S30"), new FloatWritable(Float.parseFloat(tem)));
	            else if(latf >-60 && latf<=-30)
	            	context.write(new Text("S30-S60"), new FloatWritable(Float.parseFloat(tem)));
	            else
	            	context.write(new Text("S60-S90"), new FloatWritable(Float.parseFloat(tem)));
            }

          
        }

   	
    }

	public class AvgHbaseReducer extends TableReducer<Text, FloatWritable, ImmutableBytesWritable> {
	    public void reduce(Text key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {
	    	float sum = 0;
	    	float count =0;
			for(FloatWritable value : values){
				sum+=value.get();
				count++;
			}
	        Put put = new Put(key.getBytes());//instantiate of put
	        //rows is content, description is avg, value is average
	        put.add(Bytes.toBytes("content"), Bytes.toBytes("avg"), Bytes.toBytes(String.valueOf(sum/count)));
	        context.write(new ImmutableBytesWritable(key.getBytes()), put);// output is <key, value> pair
	    }
}

	public String run() throws Exception {
		String tablename = "Avg";
	    Configuration conf = HBaseConfiguration.create();
	
	    HBaseAdmin admin = new HBaseAdmin(conf);
	    if(admin.tableExists(tablename)){
	        System.out.println("table exists!recreating.......");
	        admin.disableTable(tablename);
	        admin.deleteTable(tablename);
	    }
	    HTableDescriptor htd = new HTableDescriptor(tablename);
	    HColumnDescriptor tcd = new HColumnDescriptor("content");
	    htd.addFamily(tcd);//Create rows
	    admin.createTable(htd);//Create table
	    String[] args = new String[]{"kl"};
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 1) {
	      System.err.println("Usage: AvgCountHbase <in>");
	      System.exit(2);
	    }
	    Job job = new Job(conf, "AvgHbase");
	    job.setJarByClass(Avg.class);
	    //Using AvgHbaseReducer to finish reduce tasks
	    job.setMapperClass(UserMapper.class);
	    TableMapReduceUtil.initTableReducerJob(tablename, AvgHbaseReducer.class, job);
	    //Set the input of datasets
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    //Set input type of output, key is Text
	    job.setOutputKeyClass(Text.class);
	    //value is FloatWritable
	    job.setOutputValueClass(FloatWritable.class);
	    //Call job.waitForCompletion function, and then quit
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	    return "Successful for Avg";
	}

}
