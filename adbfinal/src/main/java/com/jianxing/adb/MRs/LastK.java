package com.jianxing.adb.MRs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class LastK {
	public LastK(){
		
	}
	public void run() throws Exception {
		String tablename = "LastK";
	    Configuration conf = HBaseConfiguration.create();
	
	    HBaseAdmin admin = new HBaseAdmin(conf);
	    if(admin.tableExists(tablename)){
	        System.out.println("table exists!recreating.......");
	        admin.disableTable(tablename);
	        admin.deleteTable(tablename);
	    }
	    HTableDescriptor htd = new HTableDescriptor(tablename);
	    HColumnDescriptor tcd = new HColumnDescriptor("content");
	    htd.addFamily(tcd);//Create rows
	    admin.createTable(htd);//Create table
	    String[] args = new String[]{"kl"};
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 1) {
	      System.err.println("Usage: LastKCountHbase <in>");
	      System.exit(2);
	    }
	    Job job = new Job(conf, "LastKHbase");
	    job.setJarByClass(LastK.class);
	    //Using LastKHbaseReducer to finish reduce tasks
	    job.setMapperClass(UserMapper.class);
	    TableMapReduceUtil.initTableReducerJob(tablename, LastKReducer.class, job);
	    //Set the input of datasets
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    //Set input type of output, key is Text
	    job.setOutputKeyClass(Text.class);
	    //value is FloatWritable
	    job.setOutputValueClass(FloatWritable.class);
	    //Call job.waitForCompletion function, and then quit
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	
    }
	
	 public class UserMapper extends Mapper<Object, Text, Text, FloatWritable> {
		 	@Override
	        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
	        	String[] lines = value.toString().split(",");
	        	
	            String name = lines[0], lat = lines[1],lng = lines[2], tem = lines[3];
	            if(!lat.equals("")&&!lng.equals("")&&!tem.equals("")){
//		            name = name.substring(1, name.length()-1);
//		            lat = lat.substring(1, lat.length()-1);
//		            lng = lng.substring(1, lng.length()-1);
		            tem = tem.substring(1, tem.length()-1);
		            String str = name+";"+lat+";"+lng;
		            context.write(new Text(str), new FloatWritable(Float.parseFloat(tem)));
	            }

	          
	        }
 
	   	
	    }
	 
	 public class LastKReducer extends TableReducer<Text, FloatWritable, ImmutableBytesWritable> {
		 private TreeMap<String, Float> tm = new TreeMap<String, Float>();
		 public void reduce(Text key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {
			 for(FloatWritable value : values){
				 tm.put(key.toString(), value.get());
			 }
	         
		 }
		 @Override
		 protected void cleanup(Context context) throws IOException, InterruptedException{
	     	List<Entry<String, Float>> list = new ArrayList<Map.Entry<String,Float>>(tm.entrySet());
	         //implementing sort by comparator
	         Collections.sort(list,new Comparator<Map.Entry<String,Float>>() {
	             //order by increasing
	             public int compare(Entry<String, Float> o1,
	                     Entry<String, Float> o2) {
	                 return  o1.getValue().compareTo(o2.getValue());
	             }
	         });
	         int count = 0;
	         for(Entry<String, Float> mapping:list){ 
	         	if(count == 10)
	         		break;
	         	String key = mapping.getKey();
	         	Put put = new Put(key.getBytes());//instantiate of put
	            //rows is content, description is LastK, value is Last
	            put.add(Bytes.toBytes("content"), Bytes.toBytes("Last"+(count+1)), Bytes.toBytes(String.valueOf(mapping.getValue())));
	            context.write(new ImmutableBytesWritable(key.getBytes()), put);// output is <key, value> pair
	
	         	count++;
	         } 
		 }    
	 }
}
