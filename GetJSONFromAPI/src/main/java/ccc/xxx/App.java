package ccc.xxx;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	String temp = HttpRequest("http://api.openweathermap.org/data/2.5/find?lat=55.5&lon=37.5&cnt=10&APPID=420b33ea2a495b21a35d6970308cecb5");
        System.out.println( HttpRequest("http://api.openweathermap.org/data/2.5/find?lat=55.5&lon=37.5&cnt=10&APPID=420b33ea2a495b21a35d6970308cecb5"));
        JSONParser parser = new JSONParser();

    	try {
//    		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\lu\\Desktop\\daily_14.json"));  
//    		String temp = null;
//    		temp = br.readLine();  
//	        while (temp != null) {  
	        	Object obj = parser.parse(temp);
	        	JSONObject jsonObject = (JSONObject) obj;

	        	JSONArray jsonObject1 = (JSONArray) jsonObject.get("list");
	        	Iterator<JSONObject> iterator = jsonObject1.iterator();
	        	float sum =0;
	        	while(iterator.hasNext()){
	        		JSONObject jo = (JSONObject)iterator.next();
	        		JSONObject jsonObject2 = (JSONObject)jo.get("main");
	        		String tem =  String.valueOf(jsonObject2.get("temp"));
	        		sum+=Float.parseFloat(tem);
	        	}
	        	System.out.println(sum/10);
//	    		JSONObject jsonObject2 = (JSONObject)jsonObject1.get("main");
//	    		String tem =  String.valueOf(jsonObject2.get("temp"));
//	    		Double lng = (Double) jsonObject2.get("lon");
//	    		JSONArray msg = (JSONArray) jsonObject.get("data");
//	    		Iterator<JSONObject> iterator = msg.iterator();
//	    		Double s = (Double)((JSONObject)iterator.next().get("temp")).get("day");
//	    		System.out.println(tem);
	            // 读取的每一行内容后面加上一个空格用于拆分成语句    
//	            temp = br.readLine();  
//	        }  
    		

//    		long age = (Long) jsonObject.get("age");
//    		System.out.println(age);

    		// loop array
//    		JSONArray msg = (JSONArray) jsonObject.get("id");
//    		Iterator<String> iterator = msg.iterator();
//    		while (iterator.hasNext()) {
//    			System.out.println(iterator.next());
//    		}

//    	} catch (FileNotFoundException e) {
//    		e.printStackTrace();
//    	} catch (IOException e) {
//    		e.printStackTrace();
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}
	
    }
    public static String HttpRequest(String requestUrl) {
        StringBuffer sb = new StringBuffer();
        InputStream ips = getInputStream(requestUrl);
        InputStreamReader isreader = null;
        try {
            isreader = new InputStreamReader(ips, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(isreader);
        String temp = null;
        try {
            while ((temp = bufferedReader.readLine()) != null) {
                sb.append(temp);
            }
            bufferedReader.close();
            isreader.close();
            ips.close();
            ips = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
    private static InputStream getInputStream(String requestUrl) {
        URL url = null;
        HttpURLConnection conn = null;
        InputStream in = null;
        try {
            url = new URL(requestUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.connect();
 
            in = conn.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return in;
    }
}
