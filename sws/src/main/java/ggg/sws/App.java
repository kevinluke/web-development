package ggg.sws;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	JSONParser parser = new JSONParser();
    	File file = new File("e:\\write.csv");  
        Writer writer = new FileWriter(file);  
        CSVWriter csvWriter = new CSVWriter(writer, ',');  
        
    	try {
    		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\lu\\Desktop\\daily_14.json"));  
    		String temp = null;
    		temp = br.readLine();  
	        while (temp != null) {  
	        	Object obj = parser.parse(temp);
	        	JSONObject jsonObject = (JSONObject) obj;

	    		JSONObject jsonObject1 = (JSONObject) jsonObject.get("city");
	    		JSONObject jsonObject2 = (JSONObject)jsonObject1.get("coord");
	    		String name = String.valueOf(jsonObject1.get("name"));
	    		String lat = String.valueOf(jsonObject2.get("lat"));
	    		String lng = String.valueOf(jsonObject2.get("lon"));
	    		JSONArray msg = (JSONArray) jsonObject.get("data");
	    		Iterator<JSONObject> iterator = msg.iterator();
	    		String s = String.valueOf(((JSONObject)iterator.next().get("temp")).get("day"));
//	    		System.out.println(name+";"+lat+";"+lng+";"+s);
	            // 读取的每一行内容后面加上一个空格用于拆分成语句    
	            temp = br.readLine();  
	            String[] strs = {name, lat , lng, s};  
	            csvWriter.writeNext(strs);  
	            
	        }  
	        
	        csvWriter.close();  
	        System.out.println("Successful");
//    		long age = (Long) jsonObject.get("age");
//    		System.out.println(age);

    		// loop array
//    		JSONArray msg = (JSONArray) jsonObject.get("id");
//    		Iterator<String> iterator = msg.iterator();
//    		while (iterator.hasNext()) {
//    			System.out.println(iterator.next());
//    		}

    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}
	}
}
